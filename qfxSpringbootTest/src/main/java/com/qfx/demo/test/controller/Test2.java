package com.qfx.demo.test.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test2")
public class Test2 {

	@RequestMapping("/hello")
	public String hello(String name, Integer age) {
		return "你好啊，" + name + ",你今年" + age +"岁了吧";
	}
}
