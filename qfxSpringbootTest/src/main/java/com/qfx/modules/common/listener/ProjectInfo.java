package com.qfx.modules.common.listener;

import com.qfx.modules.common.util.ToolIP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <h5>描述:显示系统基本信息</h5>
 *  
 */
@Component
public class ProjectInfo implements ApplicationListener<ApplicationReadyEvent> {
	
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	
	private final static List<String> URL_LIST = new ArrayList<>();
	
	@Value("${server.port}")
	String port;
	@Value("${server.servlet.context-path}")
	String projectName;
	@Value("${spring.profiles.active}")
	String projectActive;

	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		// 输出项目的所有接口
		printControllerMethod();
		
		// 神兽的祝福
		blessingFromDivineBeast();

		// 输出项目访问信息
		printProjectInfo();
	}
	
	/**
	 * 功能：获取项目的所有接口信息
	 */
	public static List<String> getUrlList() {
		return URL_LIST;
	}

	// ---------------------------------------- private method ----------------------------------------
	/**
	 * 功能：输出项目的所有接口
	 */
	private void printControllerMethod() {
		Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
		if (!handlerMethods.isEmpty()) {
			log.info("======================================== 项目接口信息 ========================================");
			for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : handlerMethods.entrySet()) {
				String controllerName = getSimpleClassName(entry.getValue().getBeanType().getName());
				String endpoint = entry.getKey().toString();

				URL_LIST.add(endpoint.substring(endpoint.indexOf("[") + 1, endpoint.indexOf("]")));
				log.info("接口: {}，控制器: {}", endpoint, controllerName);
			}
			log.info("======================================== 项目接口信息 ========================================\r\n");
		}
	}

	/**
	 * 功能：获取类名的简写
	 *
	 * @param fullName 全类名
	 *
	 * @return String
	 */
	private String getSimpleClassName(String fullName) {
		int lastDotIndex = fullName.lastIndexOf('.');
		return lastDotIndex > 0 ? fullName.substring(lastDotIndex + 1) : fullName;
	}

	/**
	 * 功能：输出项目访问信息
	 */
	private void printProjectInfo() {
		String localPort = ToolIP.getPort();
		if (null == localPort) {
			localPort = port;
		}
		String projectUrl;
		if ("prod".equals(projectActive)) {
			projectUrl =  " http://" + ToolIP.getPublicIpAddr() + ":" + localPort + projectName;
		} else {
			projectUrl = " http://" + ToolIP.getLocalIpAddr() + ":" + localPort + projectName;
		}

		System.out.println("当前项目运行的是[" + projectActive + "]环境,访问地址为:" + projectUrl);
	}
	
	/**
	 * 功能：神兽的祝福
	 * 详细描述：祝福服务永不宕机、永无Bug
	 */
	private void blessingFromDivineBeast() {
	    System.out.println("                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
	    System.out.println("                    //                 ┌─┐       ┌─┐                        //");
	    System.out.println("                    //              ┌──┘ ┴───────┘ ┴──┐                     //");
	    System.out.println("                    //              │                 │                     //");
	    System.out.println("                    //              │       ───       │                     //");
	    System.out.println("                    //              │  ─┬┘       └┬─  │                     //");
	    System.out.println("                    //              │                 │                     //");
	    System.out.println("                    //              │       ─┴─       │                     //");
	    System.out.println("                    //              │                 │                     //");
	    System.out.println("                    //              └───┐         ┌───┘                     //");
	    System.out.println("                    //                  │         │                         //");
	    System.out.println("                    //                  │         │                         //");
	    System.out.println("                    //                  │         │                         //");
	    System.out.println("                    //                  │         └──────────────┐          //");
	    System.out.println("                    //                  │                        │          //");
	    System.out.println("                    //                  │                        ├─┐        //");
	    System.out.println("                    //                  │                        ┌─┘        //");
	    System.out.println("                    //                  │                        │          //");
	    System.out.println("                    //                  └─┐  ┐  ┌───────┬──┐  ┌──┘          //");
	    System.out.println("                    //                    │ ─┤ ─┤       │ ─┤ ─┤             //");
	    System.out.println("                    //                    └──┴──┘       └──┴──┘             //");
	    System.out.println("                    //             神兽保佑                    永不宕机                    永无BUG       //");
	    System.out.println("                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
	    System.out.println();
	}
}
