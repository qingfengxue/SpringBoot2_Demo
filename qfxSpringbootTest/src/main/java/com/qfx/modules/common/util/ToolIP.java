package com.qfx.modules.common.util;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson2.JSONObject;

/**
 * <h5>描述:获取各类IP信息</h5>
 *  
 */
public class ToolIP {
	
	private static Logger LOG = LoggerFactory.getLogger(ToolIP.class);
	
	public static void main(String[] args) {
		System.out.println("局域网IP地址为:" + getLocalIpAddr());
		System.out.println("公网IP地址为:" + getPublicIpAddr());
	}
	
	/**
	 * <h5>功能:获取本地IP</h5>
	 * 	如果没有获取到本地IP则返回0.0.0.0
	 * 
	 * @return 本地IP地址
	 */
	public static String getLocalIpAddr() {
		String localIpAddr;
		try {
			localIpAddr = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			localIpAddr = "0.0.0.0";
		}
		return localIpAddr;
	}
	
	/**
	 * <h5>功能:获取本地公网Ip地址</h5>
	 * 	如果没有获取到本地公网IP则返回0.0.0.0
	 * @return 
	 */
	public static String getPublicIpAddr(){
		String cmd = "curl httpbin.org/ip";
		String result = ToolOSCmd.execCmd(cmd);
		String ipAddr = "0.0.0.0";
		if (!result.equals("")) {
			try {
				JSONObject resultJson = JSONObject.parseObject(result);
				String[] split = resultJson.getString("origin").split(",");
				if (split.length == 2 && split[0].trim().equals(split[1].trim())) {
					ipAddr = split[0];
				} else {
					ipAddr = split[0];
				}
			} catch (Exception e) {
				LOG.error("执行[{}]命令返回信息[{}]", cmd, result);
			}
		}
		
		return ipAddr;
	}
	
	 /** 
     * 获取用户真实IP地址，不使用request.getRemoteAddr()的原因是有可能用户使用了代理软件方式避免真实IP地址, 
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值 
     *  
     * @return ip
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for"); 
        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {  
            // 多次反向代理后会有多个ip值，第一个ip才是真实ip
            if( ip.indexOf(",")!=-1 ){
                ip = ip.split(",")[0];
            }
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("X-Real-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        } 
        return ip;  
    }
    
	/**
	 * <h5>功能:获取外部tomcat启动的端口号,使用springboot内部tomcat的时候返回null</h5>
	 * 
	 * @return 
	 */
	public static String getPort() {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		Set<ObjectName> objs = null;
		try {
			objs = mbs.queryNames(new ObjectName("*:type=Connector,*"),
					Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
		} catch (MalformedObjectNameException e) {
			e.printStackTrace();
		}
		String port = null;
		for (Iterator<ObjectName> i = objs.iterator(); i.hasNext();) {
			ObjectName obj = i.next();
			obj.getCanonicalName();
			port = obj.getKeyProperty("port");
		}
		return port;
	}
}
