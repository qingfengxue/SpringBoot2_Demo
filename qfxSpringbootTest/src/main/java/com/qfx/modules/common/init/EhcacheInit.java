package com.qfx.modules.common.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)	// 如果有多个类实现了ApplicationRunner接口,可以使用此注解指定执行顺序
public class EhcacheInit implements ApplicationRunner {
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("==================================== 系统数据缓存初始化开始 ====================================");
		log.info("系统信息数据初始化中...");
		log.info("==================================== 系统数据缓存初始化结束 ====================================\r\n");
	}
}
