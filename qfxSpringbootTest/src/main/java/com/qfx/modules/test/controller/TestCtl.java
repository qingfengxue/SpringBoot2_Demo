package com.qfx.modules.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.qfx.module.test.service.TestStarterService;
import com.qfx.modules.test.service.TestService;

@RestController
@RequestMapping("test")
public class TestCtl {
	@Value("${time.count}")
	String count;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	TestService testService;
	
	@Autowired
	TestStarterService testStarterService;
	
	
	/**
	 * <h5>描述:使用restTemplate进行接口调用</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("list")
	public String list() {
		// getForObject第一个参数是请求地址,第二个参数是返回数据类型
		return restTemplate.getForObject("http://localhost:8081/qfxSpringbootMybatisPlusDemo/sysTest/list", String.class);
	}
	
	/**
	 * <h5>描述:使用restTemplate进行接口调用</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("test")
	public void test() {
		try {
			testService.test();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * <h5>描述:返回数据信息</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("hello")
	@ResponseBody
	public String hello() {
		System.out.println("count=" + count);
		return "你好";
	}
	
	
	/**
	 * <h5>描述:调用starter方法,并返回数据</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("starter")
	@ResponseBody
	public String starter() {
		return testStarterService.getPropertiesInfo();
	}
}
