package com.qfx.modules.test.bean;

import java.io.IOException;
import java.io.OutputStream;

/**
 * TeeOutputStreamExt类是一个自定义的输出流类，可以将数据同时写入主输出流和辅助输出流。
 */
public class TeeOutputStreamExt extends OutputStream {

	/**
	 * 主输出流
	 */
	private OutputStream primaryStream;
	
    /**
     * 辅助输出流
     */
    private OutputStream secondaryStream;

    /**
     * 构造函数，创建一个TeeOutputStreamExt对象。
     * @param primaryStream 主输出流
     * @param secondaryStreams 辅助输出流数组
     * @throws IllegalArgumentException 如果未提供辅助输出流则抛出异常
     */
    public TeeOutputStreamExt(OutputStream primaryStream, OutputStream... secondaryStreams) {
        this.primaryStream = primaryStream;
        if (secondaryStreams.length == 0) {
            throw new IllegalArgumentException("请至少提供一个输出流");
        }
        
        // 创建一个CompositeOutputStream对象来同时写入多个辅助输出流
        this.secondaryStream = new CompositeOutputStream(secondaryStreams);
    }

    /**
     * 将指定字节写入主输出流和辅助输出流。
     * @param b 要写入的字节
     * @throws IOException 如果发生I/O错误
     */
    @Override
    public void write(int b) throws IOException {
    	// 将数据写入主输出流
        primaryStream.write(b);
        // 将数据写入辅助输出流
        secondaryStream.write(b);
    }

    /**
     * 将指定字节数组的一部分写入主输出流和辅助输出流。
     * @param b 要写入的字节数组
     * @param off 数组中的起始偏移量
     * @param len 要写入的字节数
     * @throws IOException 如果发生I/O错误
     */
    @Override
    public void write(byte[] b, int off, int len) throws IOException {
    	// 将字节数组的一部分写入主输出流
        primaryStream.write(b, off, len);
        // 将字节数组的一部分写入辅助输出流
        secondaryStream.write(b, off, len);
    }

    /**
     * 刷新主输出流和辅助输出流。
     * @throws IOException 如果发生I/O错误
     */
    @Override
    public void flush() throws IOException {
    	// 刷新主输出流
        primaryStream.flush();
        // 刷新辅助输出流
        secondaryStream.flush();
    }

    /**
     * 关闭辅助输出流，不关闭主输出流。
     * @throws IOException 如果发生I/O错误
     */
    @Override
    public void close() throws IOException {
    	// 不关闭主输出流，由调用方负责关闭
    	// primaryStream.close();
    	
    	// 关闭辅助输出流
        secondaryStream.close();
    }
}
