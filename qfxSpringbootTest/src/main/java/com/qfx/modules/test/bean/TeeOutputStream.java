package com.qfx.modules.test.bean;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 输出流的复制工具，它可以将数据同时写入两个不同的输出流中。
 */
public class TeeOutputStream extends OutputStream {
	private OutputStream mainStream; // 第一个输出流
	private OutputStream outputStream2; // 第二个输出流

	/**
	 * 构造函数，用于创建一个TeeOutputStream对象。
	 *
	 * @param mainStream 第一个输出流
	 * @param outputStream2 第二个输出流
	 */
	public TeeOutputStream(OutputStream mainStream, OutputStream outputStream2) {
		this.mainStream = mainStream;
		this.outputStream2 = outputStream2;
	}

	/**
	 * 将指定的字节写入输出流中。
	 *
	 * @param b 要写入的字节
	 * @throws IOException 如果发生输入/输出异常
	 */
	@Override
	public void write(int b) throws IOException {
		mainStream.write(b);
		outputStream2.write(b);
	}

	/**
	 * 刷新输出流，确保所有缓冲的数据被写入目标流中。
	 *
	 * @throws IOException 如果发生输入/输出异常
	 */
	@Override
	public void flush() throws IOException {
		mainStream.flush();
		outputStream2.flush();
	}

	/**
	 * 关闭输出流，释放任何系统资源。
	 *
	 * @throws IOException 如果发生输入/输出异常
	 */
	@Override
	public void close() throws IOException {
		// mainStream.close();  // 务必不要关闭,否则后续正常的标准输出流都无法生效
		outputStream2.close();
	}
}
