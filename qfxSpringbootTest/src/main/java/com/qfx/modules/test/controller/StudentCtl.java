package com.qfx.modules.test.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson2.JSONObject;
import com.qfx.modules.test.entity.Student;

@RestController
@RequestMapping("/student")
public class StudentCtl {

	@RequestMapping("/info")
	public Student info(Student student) {
		System.out.println(JSONObject.toJSONString(student));
		return student;
	}
}
