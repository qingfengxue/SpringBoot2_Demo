package com.qfx.modules.test.service;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.qfx.modules.test.bean.TeeOutputStream;
import com.qfx.modules.test.bean.TeeOutputStreamExt;
import com.qfx.modules.test.bean.TeePrintStream;

/**
 * 获取控制台输出信息
 * @author user
 *
 */
public class ConsoleOutputCaptureDemo {
	public static void main(String[] args) {
//		ConsoleOutput();
//		ConsoleOutputToFile();
		captureConsoleOutput();
		System.out.println("正常输出...");
	}

	/**
	 * 控制台输出的同时，获取控制台输出信息放在最后统一处理
	 */
	public static void ConsoleOutput() {
		// 保存原来的标准输出流
		PrintStream originalOut = new PrintStream(System.out);
		String output = "";
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				TeePrintStream teePrintStream = new TeePrintStream(System.out, baos)) {
			System.setOut(teePrintStream);

			for (int i = 1; i <= 5; i++) {
				System.out.println(i + ": 这条消息会同时在控制台中显示，并在字节数组输出流中存在");
			}
			// 获取捕获到的控制台输出
			output = baos.toString();
		} catch (Exception e) {
			e.getMessage();
		}

		// 恢复原始的标准输出流
		System.setOut(originalOut);

		System.out.println();
		System.out.println("-------------获取抓取到的控制台信息---------------");
		System.out.println(output);
	}

	/**
	 * 控制台输出信息的同时输出信息到指定文件中
	 */
	public static void ConsoleOutputToFile() {
		// 保存原来的标准输出流
		PrintStream originalOut = new PrintStream(System.out);
		try (FileOutputStream fileOutputStream = new FileOutputStream("f:/output.txt");
				TeeOutputStream teeOutputStream = new TeeOutputStream(System.out, fileOutputStream);
				PrintStream printStream = new PrintStream(teeOutputStream, true, "UTF-8")) {
			// 将标准输出流重定向到打印流
			System.setOut(printStream);

			for (int i = 1; i <= 10; i++) {
				System.out.println(i + ": 这条消息会同时在控制台和文件中显示");
				Thread.sleep(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 恢复原始的标准输出流
		System.setOut(originalOut);
	}

	/**
	 * 控制台输出的同时输出信息到指定文件中,并获取控制台输出信息在最后统一处理
	 */
	public static void captureConsoleOutput() {
		// 保存原来的标准输出流
		PrintStream originalOut = new PrintStream(System.out);
		String output = "";
		try (
				// 创建字节数组输出流，用于捕获控制台输出信息
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				// 创建文件输出流，将控制台输出信息写入文件
				FileOutputStream fileOutputStream = new FileOutputStream("f:/output.txt");
				// 创建一个合成输出流，同时将输出信息分别写入标准输出流、字节数组输出流和文件输出流
				TeeOutputStreamExt teeOutputStream = new TeeOutputStreamExt(originalOut, baos, fileOutputStream);
				// 创建一个新的PrintStream，将其设置为当前的标准输出流，使得控制台输出信息被重定向到合成输出流中
				PrintStream printStream = new PrintStream(teeOutputStream)
			) {
			// 设置为新的输出方式开始生效
			System.setOut(printStream);

			for (int i = 1; i <= 5; i++) {
				System.out.println(i + ": 这条消息会同时在控制台中显示，并在字节数组输出流中存在");
				Thread.sleep(1000);
			}
			// 获取捕获到的控制台输出
			output = baos.toString();
		} catch (Exception e) {
			e.getMessage();
		}

		// 恢复原始的标准输出流
		System.setOut(originalOut);

		System.out.println();
		System.out.println("-------------获取抓取到的控制台信息---------------");
		System.out.println(output);
	}
}
