package com.qfx.modules.test.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Student {
	private String name;
	
	private int age;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8") //配置文件中已经设置全局格式和时区，此处就不需要此注解了
	private Date birthday;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
