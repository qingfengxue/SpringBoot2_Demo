package com.qfx.modules.test.bean;

import java.io.IOException;
import java.io.OutputStream;

/**
 * CompositeOutputStream类是一个自定义的输出流类，它将数据同时写入多个输出流中。
 */
public class CompositeOutputStream extends OutputStream {
	
	/**
	 *  存储多个输出流的数组
	 */
	private OutputStream[] outputStreams;

	/**
	 * 构造函数，接收多个输出流作为参数
	 *
	 * @param outputStreams 多个输出流
	 */
    public CompositeOutputStream(OutputStream... outputStreams) {
        this.outputStreams = outputStreams;
    }

    /**
     * 将指定字节写入每个输出流中
     *
     * @param b 要写入的字节
     * @throws IOException 如果发生 I/O 错误
     */
    @Override
    public void write(int b) throws IOException {
        for (OutputStream outputStream : outputStreams) {
            outputStream.write(b);
        }
    }

    /**
     * 将指定范围内的字节数组写入每个输出流中
     *
     * @param b   要写入的字节数组
     * @param off 起始偏移量
     * @param len 要写入的字节数
     * @throws IOException 如果发生 I/O 错误
     */
    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        for (OutputStream outputStream : outputStreams) {
            outputStream.write(b, off, len);
        }
    }

    /**
     * 刷新每个输出流
     *
     * @throws IOException 如果发生 I/O 错误
     */
    @Override
    public void flush() throws IOException {
        for (OutputStream outputStream : outputStreams) {
            outputStream.flush();
        }
    }

    /**
     * 关闭每个输出流
     *
     * @throws IOException 如果发生 I/O 错误
     */
    @Override
    public void close() throws IOException {
        for (OutputStream outputStream : outputStreams) {
            outputStream.close();
        }
    }
}
