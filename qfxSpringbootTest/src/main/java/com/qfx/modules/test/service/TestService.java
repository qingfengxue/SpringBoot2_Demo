package com.qfx.modules.test.service;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {
	
	@Autowired
	HttpServletRequest request;
	
	public static Set<String> set = new HashSet<>();
	
	public void test() throws InterruptedException {        
        // …………………………通过某种方式获得了request对象………………………………

        // 判断线程安全
        String value = request.getParameter("key");        
        if (set.contains(value)) {
            System.out.println(value + "\t重复出现，request并发不安全！");
        } else {
            System.out.println(value);
            set.add(value);
        }        // 模拟程序执行了一段时间
        Thread.sleep(1000);
    }
}
