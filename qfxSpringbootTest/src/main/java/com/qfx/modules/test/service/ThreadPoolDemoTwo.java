package com.qfx.modules.test.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.springframework.util.StopWatch;

import com.alibaba.fastjson2.JSONObject;

/**
 * 线程池示例
 */
public class ThreadPoolDemoTwo {

	public static void main(String[] args) {
		test01();
		System.out.println("----------------------------");
		test02();
		System.out.println("----------------------------");
		test03();
	}

	// 线程池执行方案
	public static void test01() {
		System.out.println("线程池执行方案");

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		String str = threadPollTest();
		System.out.println(str);

		stopWatch.stop();
		long[] de = formatDurationByMillis(stopWatch.getLastTaskTimeMillis());
		System.out.println("用时:" + de[0] + "天" + de[1] + "小时" + de[2] + "分" + de[3] + "秒" + de[4] + "毫秒");
	}

	// 普通执行方案,不使用线程,顺序执行
	public static void test02() {
		System.out.println("普通执行方案,不使用线程,顺序执行");

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		Map<String, Object> totalResultMap = getTotalResult();
		List<Map<String, Object>> userList = getUserList();
		Map<String, Object> resultMap = getResult();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", totalResultMap);
		map.put("rows", userList);
		map.put("result", resultMap);

		String str = JSONObject.toJSONString(map);
		System.out.println(str);

		stopWatch.stop();
		long[] de = formatDurationByMillis(stopWatch.getLastTaskTimeMillis());
		System.out.println("用时:" + de[0] + "天" + de[1] + "小时" + de[2] + "分" + de[3] + "秒" + de[4] + "毫秒");
	}

	// 线程池执行方案-循环执行
	public static void test03() {
		System.out.println("线程池执行方案-循环执行");

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		List<List<Map<String, Object>>> resultList = threadPollTest02();
		System.out.println(JSONObject.toJSONString(resultList));

		stopWatch.stop();
		long[] de = formatDurationByMillis(stopWatch.getLastTaskTimeMillis());
		System.out.println("用时:" + de[0] + "天" + de[1] + "小时" + de[2] + "分" + de[3] + "秒" + de[4] + "毫秒");
	}

	// ------------------- private method -------------------
	// 调用多个方法,获取结果顺序执行,不会因为先执行完毕时间不一样而顺序打乱
	private static String threadPollTest() {
		Map<String, Object> map = new HashMap<String, Object>();

		// 创建一个固定大小的线程池
		ExecutorService service = Executors.newFixedThreadPool(3);

		Future<Map<String, Object>> submit = service.submit(() -> getTotalResult());
		Future<List<Map<String, Object>>> submit2 = service.submit(() -> getUserList());
		Future<Map<String, Object>> submit3 = service.submit(() -> getResult());

		try {
			Map<String, Object> totalResultMap = submit.get();
			List<Map<String, Object>> userList = submit2.get();
			Map<String, Object> resultMap = submit3.get();

			map.put("total", totalResultMap);
			map.put("rows", userList);
			map.put("result", resultMap);
		} catch (InterruptedException | ExecutionException e) {
			// 如果发生异常则中断正在运行的任务
			submit.cancel(true);
			submit2.cancel(true);
			submit3.cancel(true);

			e.printStackTrace();
		} finally {
			// 关闭线程池
			service.shutdown();
		}
		return JSONObject.toJSONString(map);
	}

	// 调用相同方法循环处理
	private static List<List<Map<String, Object>>> threadPollTest02() {
		List<List<Map<String, Object>>> resultList = new ArrayList<List<Map<String, Object>>>();

		// 创建一个固定大小的线程池,可控制线程最大并发数，超出的线程会在队列中等待,代码如下
		ExecutorService service = Executors.newFixedThreadPool(3);

		List<Future<List<Map<String, Object>>>> list = new ArrayList<Future<List<Map<String, Object>>>>();
		for (int i = 1; i <= 10; i++) {
			// 方法调用方式一：有参调用
			// 用于Lambda表达式的变量不可变,如以下写法会编译错误,因为getUserList(i)中的i是可变的
			// service.submit(()-> getUserList(i));
			// 编译错误信息:Local variable i defined in an enclosing scope must be
			// final or effectively final
			// 但可以使用以下方法来完成参数传递
			int k = i;
			Future<List<Map<String, Object>>> submit = service.submit(() -> getUserList(k));

			// 方法调用方式二：无参调用
			// Future<List<Map<String, Object>>> submit = service.submit(()->
			// getUserList());

			list.add(submit);
		}

		try {
			for (Future<List<Map<String, Object>>> future : list) {
				List<Map<String, Object>> userList = future.get();
				resultList.add(userList);
			}
		} catch (InterruptedException | ExecutionException e) {
			// 如果发生异常则中断正在运行的任务
			for (Future<List<Map<String, Object>>> future : list) {
				future.cancel(true);
			}

			e.printStackTrace();
		} finally {
			// 关闭线程池
			service.shutdown();
		}
		return resultList;
	}

	// 总记录数
	private static Map<String, Object> getTotalResult() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("totalResult", 8699);
		try {
			Thread.sleep(2000);
			System.err.println("线程[" + Thread.currentThread().getName() + "]执行方法getTotalResult()完毕,本方法执行需要时间2秒");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return map;
	}

	// 记录集合(无参)
	private static List<Map<String, Object>> getUserList() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Map<String, Object> map = null;
		for (int i = 1; i <= 18; i++) {
			map = new HashMap<String, Object>();
			map.put("id", UUID.randomUUID());
			map.put("age", "王琳琳" + i);
			list.add(map);
		}
		try {
			Thread.sleep(5000);
			System.err.println("线程[" + Thread.currentThread().getName() + "]执行方法getUserList()完毕,本方法执行需要时间5秒");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return list;
	}

	// 记录集合2(有参)
	private static List<Map<String, Object>> getUserList(int threadNum) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Map<String, Object> map = null;
		for (int i = 1; i <= 18; i++) {
			map = new HashMap<String, Object>();
			map.put("id", UUID.randomUUID());
			map.put("age", "王琳琳" + i);
			list.add(map);
		}
		try {
			Thread.sleep(5000);
			System.err.println("第" + threadNum + "个方法使用线程[" + Thread.currentThread().getName() + "]执行完毕,本方法执行需要时间5秒");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return list;
	}

	// 请求结果
	private static Map<String, Object> getResult() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", true);
		try {
			Thread.sleep(3000);
			System.err.println("线程[" + Thread.currentThread().getName() + "]执行方法getResult()完毕,本方法执行需要时间3秒");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * <h5>功能:根据毫秒数来格式化持续时间</h5>
	 * 
	 * @param millis 毫秒数
	 * @return long[] 返回值为：{天, 时, 分, 秒, 毫秒}
	 */
	private static long[] formatDurationByMillis(long millis) {
		long day = TimeUnit.MILLISECONDS.toDays(millis);
		long hour = TimeUnit.MILLISECONDS.toHours(millis) % 24;
		long minute = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
		long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;
		millis = millis % 1000;

		long[] times = { day, hour, minute, seconds, millis };
		return times;
	}
}
