1. 这是一个springboot2整合Redis的示例,Springboot的版本是2.2.1.RELEASE

2. 整合步骤：
	2.1 pom.xml添加以下配置：
		<!-- 5.引入redis依赖 -->
		<dependency>
	        <groupId>org.springframework.boot</groupId>
	        <artifactId>spring-boot-starter-data-redis</artifactId>
	    </dependency>
	    
	2.2 application.properties添加以下配置：
		# ----------------Redis配置---------------
		## Redis数据库索引(默认为 0)
		spring.redis.database=2
		## Redis服务器地址
		spring.redis.host=127.0.0.1
		## Redis服务器连接端口
		spring.redis.port=6379
		## Redis 密码
		spring.redis.password=
		## 连接池最大连接数（使用负值表示没有限制）
		spring.redis.jedis.pool.max-active=2000
		## 连接池最大阻塞等待时间（使用负值表示没有限制）
		spring.redis.jedis.pool.max-wait=30000
		## 连接池中的最大空闲连接
		spring.redis.jedis.pool.max-idle=500
		## 连接池中的最小空闲连接
		spring.redis.jedis.pool.min-idle=10
		## 连接超时时间（毫秒）
		spring.redis.timeout=3000
	
	2.3 添加redis配置文件
		com.qfx.modules.common.config.RedisConfig.java
	
	2.4 创建一个redis工具类
		com.qfx.modules.common.util.ToolRedis.java
		
	2.5 编写一个测试的Controller
		com.qfx.modules.demo.controller.TestCtl.java

3. 测试
	地址：
		http://127.0.0.1/qfxSpringbootRedisDemo/test/set?key=aa&value=11
		http://127.0.0.1/qfxSpringbootRedisDemo/test/get?key=aa

	