package com.qfx.modules.demo.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * <h5>描述:Redis的key过期事件监听</h5>
 *  
 */
@Component
public class RedisExpiredKeyListener extends KeyExpirationEventMessageListener {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public RedisExpiredKeyListener(RedisMessageListenerContainer listenerContainer) {
		super(listenerContainer);
	}

	/**
	 * 接收Redis的key过期事件通知
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		String expiredKey = message.toString(); // 获取过期的key
		log.info("指定的 Redis key [{}] 过期", message.toString());
		
        if (expiredKey.contains("aa")) { // 判断是否是想要监听的过期key
        	log.info("指定的 Redis key [{}] 过期", message.toString());
            // TODO 业务逻辑
        }
	}
}
