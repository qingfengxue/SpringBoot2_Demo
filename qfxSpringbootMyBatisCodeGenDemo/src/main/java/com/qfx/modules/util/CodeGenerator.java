package com.qfx.modules.util;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 功能：代码生成
 */
public class CodeGenerator {
	// 数据库地址
    private static final String DB_URL = "jdbc:mysql://82.156.116.249:3306/task_test";
    // 数据库用户名
    private static final String DB_USER_NAME = "root";
    // 数据库密码
    private static final String DB_PASSWOED = "F6zx3qcY4yhl9t";
    // 作者
    private static final String AUTHOR = "users";
    // 指定输出目录
    // private static final String DIR_ROOT = System.getProperty("user.dir")+"/src/main/java";
    private static final String DIR_ROOT = "d:/codeInfo/";
    // 父包名
    private static final String PKG_NAME = "com.qfx.module.business";
    // 需要忽略的表前缀,多个前缀使用逗号分隔
    private static final String[] TABLE_PREFIX_LIST = {
            "lh_"
    };
    // 需要生成的表名,多个使用表名逗号分隔,不指定则默认生成全部表
    private static final String[] TABLE_NAME_LIST = {
            "task_info"
    };

    public static void main(String[] args) {
        // 执行代码生成
        autoCodeGenerator(Arrays.asList(TABLE_NAME_LIST), Arrays.asList(TABLE_PREFIX_LIST));
    }

    /**
     * 功能：自动生成代码
     *
     * @param tableNameList 要生成的表名
     * @param tablePrefixList 要忽略的表前缀
     */
    public static void autoCodeGenerator(List<String> tableNameList, List<String> tablePrefixList) {
        System.out.println("\r\n======================================== 代码生成 ========================================");
        // 输出要生成的表名，如果不指定则默认生成全部表
        System.out.println(tableNameList.size() > 0 ? "要生成的表: " + tableNameList : "未指定要生成的表名,将默认生成全部表");

        System.out.println("开始生成代码,请耐心等待...");
        // 执行代码生成
        codeGenerator(tableNameList, tablePrefixList);

        System.out.println("生成代码成功,请到指定目录查看: " + DIR_ROOT);
        System.out.println("======================================== 代码生成 ========================================\r\n");
    }

    // ---------------------------------------- private method ----------------------------------------
    /**
     * 功能：代码生成
     *
     * @param tableNameList 要生成的表名
     * @param tablePrefixList 要忽略的表前缀
     */
    private static void codeGenerator(List<String> tableNameList, List<String> tablePrefixList) {
        FastAutoGenerator.create(DB_URL, DB_USER_NAME, DB_PASSWOED)
                //全局配置(GlobalConfig)
                .globalConfig(builder -> {
                    builder.outputDir(DIR_ROOT) // 指定输出目录
//                            .disableOpenDir()// 禁止打开输出目录,默认值:true
                            .author(AUTHOR) // 作者
//                            .enableSwagger() // 启用Swagger
                            .dateType(DateType.ONLY_DATE) // 日期类型
                            .commentDate("yyyy-MM-dd") // 日期格式
                            .build();
                })

                //包配置(PackageConfig)
                .packageConfig(builder -> {
                    builder.parent(PKG_NAME) // 父包名
                            .entity("entity") // 实体类包名
                            .service("service") // Service接口包名
                            .serviceImpl("service.impl") // Service实现类包名
                            .controller("controller") // Controller包名
                            .mapper("mapper") // Mapper接口包名
                            .xml("mapper.xml") // Mapper XML文件包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, DIR_ROOT + "/mapper")) // XML文件路径
                            .build();
                })

                //策略配置
                .strategyConfig(builder -> {
                    builder.addTablePrefix(tablePrefixList) // 需要忽略的表前缀
                            .addInclude(tableNameList) // 需要生成的表名

                            .entityBuilder()
                            .enableLombok() // 开启Lombok
                            .enableFileOverride() // 覆盖已生成文件
//                            .idType(IdType.NONE) // 主键类型
                            .disableSerialVersionUID() // 不生成SerialVersionUID
                            .javaTemplate("/templates/entity.java") // 指定实体类模板路径

                            .mapperBuilder()
                            .enableFileOverride() // 覆盖已生成文件
                            .formatMapperFileName("%sMapper") // 格式化 Mapper 文件名称
                            .mapperTemplate("/templates/mapper.java") // 指定mapper模板路径
                            .mapperXmlTemplate("/templates/mapper.xml") // 指定mapper.xml模板路径

                            .serviceBuilder()
                            .enableFileOverride() // 不覆盖已生成文件
                            .formatServiceFileName("%sSer") // 格式化Service文件名
                            .formatServiceImplFileName("%sSerImp") // 格式化Service实现类文件名
                            .serviceTemplate("/templates/service.java") // 指定service模板路径
                            .serviceImplTemplate("/templates/serviceImpl.java") // 指定service实现类模板路径
                            
                            .controllerBuilder()
                            .enableRestStyle() // 开启生成@RestController控制器
                            .enableFileOverride() // 覆盖已生成文件
                            .formatFileName("%sCtl") // 格式化Controller的名称
                            .template("/templates/controller.java") // 指定controller模板路径
                            .build();

                }).execute();
    }
}
