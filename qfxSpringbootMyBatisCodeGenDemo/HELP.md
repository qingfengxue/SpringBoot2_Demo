# Springboot3 + MyBatis-plus 代码生成示例
## 1.项目介绍
    1.1 这是一个使用mybatis-plus-generator插件生成代码的示例
    1.2 SpringBoot版本采用3.2.4,与SpringCloudAlibaba版本2023.0.1.2所对应的版本一致
    1.3 jdk版本采用21
    1.4 运行环境:idea

## 2.项目依赖
### 2.1 pom.xml
    <!-- 1.引入SpringBoot核心包,整合SpringMVC Web组件 -->
    <!-- 实现原理：MAVEN依赖继承关系,相当于把第三方常用MAVEN依赖信息,在parent项目中已经封装好了 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!-- 2.引入MySQL连接支持 -->
    <dependency>
        <groupId>com.mysql</groupId>
        <artifactId>mysql-connector-j</artifactId>
        <scope>runtime</scope>
    </dependency>

    <!-- ↓↓↓↓↓↓↓↓↓↓ MyBatis-plus相关支持 ↓↓↓↓↓↓↓↓↓↓ -->
    <!-- 3.引入SpringBoot3.x对应的MyBatis-plus连接支持 -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-spring-boot3-starter</artifactId>
        <version>3.5.9</version>
    </dependency>

    <!-- 4.引入MyBatis-plus代码生成器 -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-generator</artifactId>
        <version>3.5.9</version>
    </dependency>

    <!-- 5.引入velocity模板引擎,2.4.1版本会报错误信息,建议使用2.3或其他升级版本 -->
    <dependency>
        <groupId>org.apache.velocity</groupId>
        <artifactId>velocity-engine-core</artifactId>
        <version>2.3</version>
    </dependency>
    <!-- ↑↑↑↑↑↑↑↑↑↑ MyBatis-plus相关支持 ↑↑↑↑↑↑↑↑↑↑ -->

## 3.使用说明
    1.打开 [com.qfx.modules.util.CodeGenerator] 文件
    2.设置 [作者、指定输出目录、父包名、需要忽略的表前缀和需要生成的表名]信息
    3.执行 [main 方法]
    4.复制生成代码到对应项目下

## 4.其他
### SpringBoot采用2.x版本时,对应的MyBatis-plus连接支持也需要修改
    <!-- 3.引入SpringBoot2.x对应的MyBatis-plus连接支持 -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>3.5.9</version>
    </dependency>

## 5.官方文档
    https://baomidou.com/reference/new-code-generator-configuration/
    