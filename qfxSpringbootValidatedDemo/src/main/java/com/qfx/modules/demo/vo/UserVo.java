package com.qfx.modules.demo.vo;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.qfx.modules.common.util.ToolInterface;

public class UserVo {
	@Min(groups = {ToolInterface.update.class}, value = 1, message = "ID不能小于1")
	private int id;
	
	@NotBlank(groups = {ToolInterface.update.class, ToolInterface.insert.class}, message = "用户名不能为空")
	private String name;
	
	@NotBlank(groups = {ToolInterface.update.class, ToolInterface.insert.class}, message = "密码不能为空")
	@Size(groups = {ToolInterface.update.class, ToolInterface.insert.class}, min = 6, max = 12, message = "密码长度不能小于6,大于12")
	private String password;
	
	@Min(value = 1, message = "年龄必须大于1岁")
	@Max(value = 200, message = "年龄必须小于200岁")
	private int age;
	
	private String remark;
	
	// 添加上@Valid表示对bookList中的元素进行校验
	@Valid
	@NotEmpty(groups = {ToolInterface.select.class}, message = "书籍不能空,且至少1本")
	private List<BookVo> bookList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<BookVo> getBookList() {
		return bookList;
	}

	public void setBookList(List<BookVo> bookList) {
		this.bookList = bookList;
	}
}
