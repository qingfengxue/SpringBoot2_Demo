package com.qfx.modules.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.qfx.modules.common.util.ToolInterface;
import com.qfx.modules.common.vo.MessageBean;
import com.qfx.modules.demo.vo.UserVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * <h5>描述:实体类参数与普通参数校验示例</h5>
 *  采用全局异常处理的方式来进行参数校验
 */
@Validated
@RestController
@RequestMapping("testExt")
public class TestExtCtl {
	
	/**
	 * <h5>功能:只验证UserVo中未设置group或groups的属性</h5>
	 * 
	 * @param userVo
	 * @return 
	 */
	@RequestMapping("validatedOne")
	public String validatedOne(@Validated() UserVo userVo) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:只验证UserVo中group或groups为"insert.class"的属性</h5>
	 * 
	 * @param userVo
	 * @return 
	 */
	@RequestMapping("validatedTwo")
	public String validatedTwo(@Validated(ToolInterface.insert.class) UserVo userVo) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:只验证UserVo中group或groups为"update.class"的属性</h5>
	 * 
	 * @param userVo
	 * @return 
	 */
	@RequestMapping("validatedThree")
	public String validatedThree(@Validated(ToolInterface.update.class) UserVo userVo) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:只验证UserVo中同时满足多个group或groups的属性(这样用的少)</h5>
	 * all.class中同时包含了insert.class和update.class,即标注了insert.class或update.class任何一个的属性都会被验证
	 * @param userVo
	 * @return 
	 */
	@RequestMapping("validatedFour")
	public String validatedFour(@Validated(ToolInterface.all.class) UserVo userVo) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:直接参数校验,如果不生效需要从Controller加上@Validated注解</h5>
	 * 验证框架里面大部分都不需要我们显示设置message，每个注解框架都给了一个默认提示语，大多数提示还都比较友好
	 * 不建议对原始类型数据如int进行参数校验(支持不好,会报异常),建议绑定实体参数校验,如上面几个方法的校验方式
	 * @param name
	 * @param age
	 * @return 
	 */
	@RequestMapping("validatedFive")
	public String validatedFive(@NotBlank(message = "姓名不能为空") String name, @Min(value = 10, message = "年龄必须大于10岁") Integer age) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:@RequestBody和@Validated组合时参数校验</h5>
	 * 比如使用post进行json传传递时,需要使用到此种校验方式
	 * 
	 * @param userVo
	 * @return
	 */
	@PostMapping("validatedSix")
	public String validatedSix(@RequestBody @Validated(ToolInterface.all.class) UserVo userVo) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:@RequestBody和@Validated组合时参数校验</h5>
	 * 比如使用post进行json传传递时,需要使用到此种校验方式
	 * 
	 * @param userVo
	 * @return
	 */
	@PostMapping("validatedSeven")
	public String validatedSeven(@RequestBody @Validated(ToolInterface.select.class) UserVo userVo) {
		MessageBean messageBean = new MessageBean("哈哈哈,通过验证了");
		
		return JSONObject.toJSONString(messageBean);
	}
}
