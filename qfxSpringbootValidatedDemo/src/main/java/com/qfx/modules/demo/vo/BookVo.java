package com.qfx.modules.demo.vo;

import java.math.BigDecimal;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.qfx.modules.common.util.ToolInterface;

public class BookVo {
	
	@NotBlank(groups = {ToolInterface.select.class}, message = "书名不能为空")
	private String bookName;
	
	@Valid
	@NotNull(groups = {ToolInterface.select.class}, message = "价格不能为空")
	@DecimalMin(groups = {ToolInterface.select.class}, message = "价格不能少于0.01元", value = "0.01")
	@DecimalMax(groups = {ToolInterface.select.class}, message = "价格不能大于9.99元", value = "9.99")
	private BigDecimal price;

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
