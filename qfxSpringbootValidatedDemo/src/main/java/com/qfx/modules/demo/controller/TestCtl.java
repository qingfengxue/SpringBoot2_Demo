package com.qfx.modules.demo.controller;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.qfx.modules.common.util.ToolInterface;
import com.qfx.modules.common.util.ToolValidated;
import com.qfx.modules.common.vo.MessageBean;
import com.qfx.modules.demo.vo.UserVo;

/**
 * <h5>描述:实体类参数校验示例</h5>
 *  本类示例仅适用于实体类参数校验,方法参数校验请参照TestExtCtl.java中的validatedFive方法
 */
@RestController
@RequestMapping("test")
public class TestCtl {
	
	/**
	 * <h5>功能:只验证未UserVo中设置group的属性</h5>
	 * 	只验证不需要分组的参数,这里只验证userVo.age
	 * @param userVo
	 * @param bindingResult
	 * @return 
	 */
	@RequestMapping("validatedOne")
	public String validatedOne(@Validated() UserVo userVo, BindingResult bindingResult) {
		// 验证参数信息是否有效
		MessageBean messageBean = ToolValidated.myValidate(bindingResult);
		if (ToolValidated.SUCCESS == messageBean.getCode()) {
			messageBean.setMessage("哈哈哈,通过验证了");
		}
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:只验证UserVo中group为"ToolValidated.insert.class"的属性</h5>
	 * 	验证组为"insert.class"的参数,这里验证userVo.name、验证userVo.password
	 * @param userVo
	 * @param bindingResult
	 * @return 
	 */
	@RequestMapping("validatedTwo")
	public String validatedTwo(@Validated(ToolInterface.insert.class) UserVo userVo, BindingResult bindingResult) {
		// 验证参数信息是否有效
		MessageBean messageBean = ToolValidated.myValidate(bindingResult);
		if (ToolValidated.SUCCESS == messageBean.getCode()) {
			messageBean.setMessage("哈哈哈,通过验证了");
		}
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:只验证UserVo中group为"ToolValidated.update.class"的属性</h5>
	 * 	验证组为"update.class"的参数,这里验证userVo.id、userVo.name、验证userVo.password
	 * @param userVo
	 * @param bindingResult
	 * @return 
	 */
	@RequestMapping("validatedThree")
	public String validatedThree(@Validated(ToolInterface.update.class) UserVo userVo, BindingResult bindingResult) {
		// 验证参数信息是否有效
		MessageBean messageBean = ToolValidated.myValidate(bindingResult);
		if (ToolValidated.SUCCESS == messageBean.getCode()) {
			messageBean.setMessage("哈哈哈,通过验证了");
		}
		
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:只验证UserVo中同时满足多个group的属性</h5>
	 * 	验证组为"all.class"的参数,all接口包含insert.class和 update.class,
	 * 	因此需要验证同时属于insert.class和 update.class的参数,
	 * 	这里只验证userVo.name、验证userVo.password
	 * @param userVo
	 * @param bindingResult
	 * @return 
	 */
	@RequestMapping("validatedFour")
	public String validatedFour(@Validated(ToolInterface.all.class) UserVo userVo, BindingResult bindingResult) {
		// 验证参数信息是否有效
		MessageBean messageBean = ToolValidated.myValidate(bindingResult);
		if (ToolValidated.SUCCESS == messageBean.getCode()) {
			messageBean.setMessage("哈哈哈,通过验证了");
		}
		
		return JSONObject.toJSONString(messageBean);
	}
}
