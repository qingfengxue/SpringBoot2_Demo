package com.qfx.modules.common.handler;

import com.qfx.modules.common.util.ToolValidated;
import com.qfx.modules.common.vo.MessageBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * <h5>描述:全局异常处理类</h5>
 * 优先执行此异常处理类
 * 不返回页面，仅返回数据，适用于纯后台模式
 */
//@RestControllerAdvice
public class MyExceptionHandlerTwo {
	private static final Logger LOG = LoggerFactory.getLogger(MyExceptionHandlerTwo.class);

	/**
	 * <h5>功能:JSON转换异常</h5>
	 * @param request 请求对象
	 * @param e 异常对象
	 *
	 * @return 错误信息
	 */
	@ExceptionHandler(value = HttpMessageNotReadableException.class)
	public MessageBean httpMessageNotReadableExceptionExceptionHandler(HttpServletRequest request, HttpMessageNotReadableException e){
		logError(request, e);

		return new MessageBean(ToolValidated.PARAM_INVALID, "参数无效,JSON数据不合法[" + e.getMessage() + "]：" + e.getMessage());
	}

	/**
	 * 处理所有运行时异常
	 *
	 * @param request 当前的HTTP请求对象
	 * @param e 发生的异常
	 * @return 返回一个友好的错误信息
	 */
	@ExceptionHandler(value = {RuntimeException.class})
	public MessageBean handleCommonExceptions(HttpServletRequest request, Exception e) {
		logError(request, e);

		return new MessageBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "系统内部错误，请联系管理员,错误信息：" + e.getMessage());
	}

//	/**
//	 * 处理所有请求类型错误的异常
//	 *
//	 * @param request 当前的HTTP请求对象
//	 * @param e 发生的异常
//	 * @return 返回一个友好的错误信息
//	 */
//	@ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
//	public MessageBean handleHttpRequestMethodNotSupportedException(HttpServletRequest request, Exception e) {
//		logError(request, e);
//
//		return new MessageBean(HttpStatus.METHOD_NOT_ALLOWED.value(), "请求类型错误：" + e.getMessage());
//	}

	// -------------------- private method --------------------
	/**
	 * <h5>功能:获取从request中传递过来的参数信息</h5>
	 *
	 * @return Map<String, Object>
	 */
	private Map<String, Object> getMaps(HttpServletRequest request){
		Map<String, Object> paramMap = new HashMap<>();
		Enumeration<String> enume = request.getParameterNames();
		while (enume.hasMoreElements()) {
			String key = enume.nextElement();
			String[] values = request.getParameterValues(key);
			paramMap.put(key, values.length == 1 ? request.getParameter(key).trim() : values);
		}

		return paramMap;
	}

	/**
	 * <h5>功能: 获取从request中传递过来的header信息</h5>
	 *
	 * @return Map<String, Object>
	 */
	private Map<String, Object> getHeaders(HttpServletRequest request) {
		Map<String, Object> headerMap = new HashMap<>();
		//获取请求头的所有name值
		Enumeration<?> er = request.getHeaderNames();
		String headerName;
		while(er.hasMoreElements()){
			headerName = er.nextElement().toString();
			headerMap.put(headerName, request.getHeader(headerName));
		}

		return headerMap;
	}

	/**
	 * 记录异常日志。
	 *
	 * @param request 当前的HTTP请求对象
	 * @param e 发生的异常
	 */
	private void logError(HttpServletRequest request, Exception e) {
		Map<String, Object> paramsMap = getMaps(request);
		Map<String, Object> headersMap = getHeaders(request);

		String requestUri = request.getRequestURI();

		LOG.error("请求[{}]发生[{}]异常\r\n参数[{}]\r\nheader[{}]", requestUri, e.getMessage(), paramsMap, headersMap, e);
	}
}
