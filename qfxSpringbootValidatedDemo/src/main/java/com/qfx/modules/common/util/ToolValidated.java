package com.qfx.modules.common.util;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.alibaba.fastjson.JSONObject;
import com.qfx.modules.common.vo.MessageBean;

public class ToolValidated {
	private static final Logger LOG = LoggerFactory.getLogger(ToolValidated.class);
	
	// 实际使用建议将编码信息放置在一个单独的文件中统一管理
	/**
	 * 操作成功
	 */
	public static int SUCCESS = 200;
	/**
	 * 参数无效
	 */
	public static int PARAM_INVALID = 1001;
	
	// =================== Spring validated (建议使用) =================== 	
	/**
	 * <h5>功能:验证参数信息是否有效</h5>
	 * 
	 * @param bindingResult
	 * @return 
	 */
	public static MessageBean myValidate(BindingResult bindingResult) {
		MessageBean messageBean = new MessageBean();
        if(bindingResult.hasErrors()) {
        	// 设置验证结果状态码
        	messageBean.setCode(PARAM_INVALID);
        	// 获取错误字段信息集合
        	List<FieldError> fieldErrorList = bindingResult.getFieldErrors();

        	// 使用TreeSet是为了让输出的内容有序输出(默认验证的顺序是随机的)
        	Set<String> errorInfoSet = new TreeSet<String>();
            for (FieldError fieldError : fieldErrorList) {
            	// 遍历错误字段信息
            	errorInfoSet.add(fieldError.getDefaultMessage());
            	LOG.debug("[{}.{}]{}", fieldError.getObjectName() , fieldError.getField(), fieldError.getDefaultMessage());
            }
			String errorInfo = String.join(",", errorInfoSet);

			messageBean.setMessage(errorInfo);
        }
		
		return messageBean;
	}
	
	// =================== 自定义验证方法 =================== 
	
	/**
	 * <h5>功能:验证参数是否完整,不为NULL或空</h5>
	 * 
	 * @param obj
	 * @return 
	 */
	public static boolean validateParams(Object... obj) {
		boolean flag = true;
		for (int i = 0; i < obj.length; i++) {
			if (null == obj[i] || "".equals(obj[i])) {
				LOG.debug("参数信息[{}],第{}个参数不能为NULL或空", JSONObject.toJSONString(obj), i+1);
				flag = false;
				break;
			}
		}
		
		return flag;
	}
	
	/**
	 * <h5>功能:验证参数是否完整,不为NULL或空</h5>
	 * 
	 * @param obj
	 * @return 
	 */
	public static MessageBean validateParamsExt(Object... obj) {
		MessageBean messageBean = new MessageBean();
		for (int i = 0; i < obj.length; i++) {
			if (null == obj[i] || "".equals(obj[i])) {
				LOG.debug("参数信息[{}],第{}个参数不能为NULL或空", JSONObject.toJSONString(obj), i+1);
				messageBean.setCode(PARAM_INVALID);
				messageBean.setMessage("参数信息不完整");
				break;
			}
		}
		return messageBean;
	}
	
	/**
	 * <h5>功能:验证参数是否完整,不为NULL或空</h5>
	 *
	 * @param obj
	 * @return
	 */
	public static boolean isAllParamsNull(Object... obj) {
		boolean ret  = true;
		for (int i = 0; i < obj.length; i++) {
			if (null == obj[i] || "".equals(obj[i])) {
			}else {
				ret = false;
				break;
			}
		}
		return ret;
	}
	
	public static void main(String[] args) {
		validateParamsExt(1,"2","",null);
	}
}
