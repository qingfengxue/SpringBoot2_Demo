package com.qfx.modules.common.util;

import javax.validation.GroupSequence;

public class ToolInterface {
	
	// 新增使用(配合spring的@Validated功能分组使用)
	public interface select{}
	
	// 新增使用(配合spring的@Validated功能分组使用)
	public interface insert{}
	
	// 更新使用(配合spring的@Validated功能分组使用)
	public interface update{}
	
	// 属性必须有这两个分组的才验证(配合spring的@Validated功能分组使用)
	@GroupSequence({insert.class, update.class})
	public interface all{};
}
