1.Springboot的版本是2.2.1.RELEASE

2.此示例是Springboot2使用validator进行参数校验的示例,使用了两种方式
	com.qfx.modules.demo.controller.TestCtl.java 是"实体类参数校验示例",采用人工调用的方式校验
	com.qfx.modules.demo.controller.TestExtCtl.java 是"实体类参数与普通参数校验示例",采用全局异常处理的方式自动拦截校验

3.添加验证支持
	Springboot2.3以下的版本已经引用了spring-boot-starter-web,就不要需要引用spring-boot-starter-validation了,
	但Springboot2.3以后的版本则需要单独引用,本例就不再引用,下面是Springboot2.3以后的版本引用方式:
	
	3.1 pom.xml
		<!-- 引入validation支持 -->
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-validation</artifactId>
		</dependency>
	
4.测试地址
	人工调用校验:
		http://127.0.0.1/qfxSpringbootValidatedDemo/test/validatedOne
		http://127.0.0.1/qfxSpringbootValidatedDemo/test/validatedTwo
		http://127.0.0.1/qfxSpringbootValidatedDemo/test/validatedThree
		http://127.0.0.1/qfxSpringbootValidatedDemo/test/validatedFour
	自动拦截校验:
		请求方式:get
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedOne
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedTwo
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedThree
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedFour
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedFive
		
		请求方式:post,json串{}
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedSix
			{
				"name": 0,
				"password": 877777,
				"id": 2
			}
			
			http://127.0.0.1/qfxSpringbootValidatedDemo/testExt/validatedSeven
			{
				"bookList": [
			        {
			            "bookName": 1,
			            "price": 0.01,
			        }
				]
			}