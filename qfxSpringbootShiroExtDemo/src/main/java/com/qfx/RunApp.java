package com.qfx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@MapperScan("com.qfx.api.dao")	// 指定Mapper需要扫描的包,这样就不用每个Mapper上都添加@Mapper注解了
public class RunApp  extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RunApp.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RunApp.class);
	}
}
