package com.qfx.common.util;

import java.util.Map;

/**
 * <h5>描述:Map数据转换</h5>
 *  
 * @author zhangpj	2019年11月5日
 */
public class ToolMapValueCast {

	/**
	 * <h5>功能:获取String[]类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息或转换失败返回null
	 */
	public String[] getStringArryParam(Map<String, Object> paramMap, String key){
		Object ojb = paramMap.get(key);
		return null == ojb ? null: (String[])ojb;
	}
	
	/**
	 * <h5>功能:获取String类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回null
	 */
	public String getStringParam(Map<String, Object> paramMap, String key){
		Object ojb = paramMap.get(key);
		return null == ojb ? null: String.valueOf(ojb);
	}
	
	/**
	 * <h5>功能:获取int类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0
	 */
	public int getIntParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0 : Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}
	
	/**
	 * <h5>功能:获取double类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0.0
	 */
	public double getDoublePara(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0.0d : Double.parseDouble(value);
		} catch (Exception e) {
			return 0.0d;
		}
	}
	
	/**
	 * <h5>功能:获取float类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0.0
	 */
	public float getFloatParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0f : Float.parseFloat(value);
		} catch (Exception e) {
			return 0.0f;
		}
	}
	
	/**
	 * <h5>功能:获取long类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0
	 */
	public long getLongParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0L : Long.parseLong(value);
		} catch (Exception e) {
			return 0L;
		}
	}
	
	/**
	 * <h5>功能:获取boolean类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息或转换失败返回false
	 */
	public boolean getBooleanParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		return null == value ? false : Boolean.parseBoolean(value);
	}
}
