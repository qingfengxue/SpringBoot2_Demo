package com.qfx.common.util;

public final class ToolResultCode {
	// --------------- 登录 ---------------
	/**
	 * 登录成功
	 */
	public static final String LOGIN_SUCCESS = "0001";
	/**
	 * 登录失败
	 */
	public static final String LOGIN_FAIL = "0002";
	/**
	 * 注册成功
	 */
	public static final String USER_REG_SUCCESS = "0003";
	/**
	 * 登录失败
	 */
	public static final String USER_REG_FAIL = "0004";
	/**
	 * 用户不存在
	 */
	public static final String INFO_05 = "0005";
	/**
	 * 用户密码出错
	 */
	public static final String INFO_06 = "0006";
	/**
	 * 账户锁定
	 */
	public static final String INFO_07 = "0007";
	/**
	 * 帐号已被禁用
	 */
	public static final String INFO_08 = "0008";
	/**
	 * 其他错误
	 */
	public static final String INFO_9999 = "9999";
	
	// --------------- 权限登录 ---------------
	public static final String INFO_401 = "401";
	public static final String INFO_403 = "403";
	public static final String INFO_404 = "404";
	public static final String INFO_500 = "500";
}
