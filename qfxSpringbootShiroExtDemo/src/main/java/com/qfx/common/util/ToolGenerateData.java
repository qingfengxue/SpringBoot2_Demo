package com.qfx.common.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.UUID;

public class ToolGenerateData {
	
	static Random r=new Random();
	static DecimalFormat floatFormat = new DecimalFormat("0.00");
	static String dateFormat="yyyy-MM-dd";
	static String timeFormat="HH:mm:ss";
	static String longDateFormat="yyyyMMDDHHmmssSSS";
	static DateFormat df = new SimpleDateFormat(dateFormat+" "+timeFormat);
	static DateFormat dfl=new SimpleDateFormat(longDateFormat);
	static String letterLow="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static String letterUpper=letterLow.toUpperCase();
	static String letterLowExt="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	public static int getIntData(int inD){
		return r.nextInt(inD);
	}
	
	/**
	 * <h5>功能:获取指定范围内的随机整数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月23日
	 * @param minD 最小范围
	 * @param maxD 最大范围
	 * @return 大于等于最小范围,小于最大范围
	 */
	public static int getIntDataExt(int minD, int maxD){
		int num = getIntData(maxD);
		if (num < minD) {
			num = getIntDataExt(minD, maxD);
		}
		
		return num;
	}
	
	/**
	 * <h5>功能:获取指定长度的随机整数字符串</h5>
	 * 
	 * @author zhangpj	@date 2018年11月23日
	 * @param inMax
	 * @param length
	 * @return 
	 */
	public static int getIntData(int inMax,int length){
		String c="";
		int x=0;
		while(c.length()<length){
			x=getIntData(inMax);
			c+=""+x+"";
		}
		return new Integer(c);
	}
	
	/**
	 * <h5>功能:</h5>生成指定长度的随机整数字符串
	 * @param strLength 指定生成的长度
	 * @return 
	 *
	 * @author zhangpj	@date 2016年9月11日
	 */
	public static String getStrData(int strLength){
		StringBuffer sbf = new StringBuffer();
		while(sbf.length() < strLength){
			sbf.append(getIntData(9));
		}
		return sbf.toString();
	}
	
	public static float getFloatData(int inD){
		return new Float(floatFormat.format(r.nextFloat()*inD));
	}
	
	public static char getCharLow(){
		return letterLow.charAt(getIntData(26));
	}
	
	public static char getChineseChar0(){
		return (char)(0x4e00 + r.nextInt(0x9fa5 - 0x4e00 + 1));
	}
	public static char getChineseChar1() throws Exception {
		byte[] b = new byte[2];
		b[0] = (new Integer((176 + Math.abs(r.nextInt(39))))).byteValue();
		b[1] = (new Integer(161 + Math.abs(r.nextInt(93)))).byteValue();
		return new String(b, "GB2312").charAt(0);
	}
	
	public static String getChineseString0(int inLength){
		String s="";
		for(int i=0;i<inLength;i++) s+=getChineseChar0();
		return s;		
	}
	
	public static String getChineseString1(int inLength) throws Exception {
		String s="";
		for(int i=0;i<inLength;i++) s+=getChineseChar1();
		return s;		
	}
	
	public static char getCharUpper(){
		return letterUpper.charAt(getIntData(26));
	}
	
	/**
	 * @功能描述：	随机生成指定长度的字母
	 *
	 * @param inLentgh
	 * @return 
	 *
	 * @作者：zhangpj		@创建时间：2018年2月3日
	 */
	public static String getStringRandom(int inLentgh){
		StringBuilder sb=new StringBuilder("");
		for(int i=0;i<inLentgh;i++) sb.append(letterLow.charAt(getIntData(52)));
		return sb.toString();
	}
	
	/**
	 * <h5>功能:生成指定位数的大小写和数字组合字符串</h5>
	 * 
	 * @author zhangpj	@date 2018年11月22日
	 * @param inLentgh
	 * @return 
	 */
	public static String getStringRandomExt(int inLentgh){
		StringBuilder sb=new StringBuilder("");
		for(int i=0;i<inLentgh;i++) sb.append(letterLowExt.charAt(getIntData(62)));
		return sb.toString();
	}
	
	/**
	 * <h5>功能:生成指定位数的必定有一个数字的大小写和数字组合字符串</h5>
	 * 
	 * @author zhangpj	@date 2018年11月22日
	 * @param inLentgh 正整数
	 * @return 
	 */
	public static String getStringRandomExt2(int inLentgh){
		String str =ToolGenerateData.getStringRandomExt(inLentgh);
		int number = getFirstInt(str);
		if (number == -1) {
			str = getStringRandomExt2(inLentgh);
		} 
		return str;
	}
	
	/**
	 * @功能描述：	随机生成指定长度的小写字母
	 *
	 * @param inLentgh
	 * @return 
	 *
	 * @作者：zhangpj		@创建时间：2018年2月3日
	 */
	public static String getStringRandomLow(int inLentgh){
		return getStringRandom(inLentgh).toLowerCase();
	}
	
	/**
	 * @功能描述：	随机生成指定长度的大写字母
	 *
	 * @param inLentgh
	 * @return 
	 *
	 * @作者：zhangpj		@创建时间：2018年2月3日
	 */
	public static String getStringRandomUpper(int inLentgh){
		return getStringRandom(inLentgh).toUpperCase();
	}
	
	public static String getSystemDateTime()  {
		return df.format(Calendar.getInstance().getTime());
	}
	
	public static String getLongDate(){
		return dfl.format(Calendar.getInstance().getTime());
	}
	
	public static String getUUID(){
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	/**
	 * <h5>功能:获取字符串中第一个数字</h5>
	 * 
	 * @author zhangpj	@date 2018年11月23日
	 * @param str
	 * @return 
	 */
	public static int getFirstInt(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= '0' && c <= '9') {
				return c - '0';
			}
		}
		return -1;
	}
	
	public static void main(String[] args) throws Exception {
//		System.out.println(ToolGenerateData.getIntData(26));
//		System.out.println(ToolGenerateData.getIntData(9,6));
//		System.out.println(ToolGenerateData.getFloatData(10));
//		System.out.println(ToolGenerateData.getCharLow());
//		System.out.println(ToolGenerateData.getChineseChar0());
//		System.out.println(ToolGenerateData.getChineseString0(15));
//		System.out.println(ToolGenerateData.getChineseChar1());
//		System.out.println(ToolGenerateData.getChineseString1(15));
//		System.out.println(ToolGenerateData.getCharUpper());
//		System.out.println(ToolGenerateData.getStringRandom(5));
//		System.out.println(ToolGenerateData.getStringRandomLow(5));
//		System.out.println(ToolGenerateData.getStringRandomUpper(5));
		System.out.println(ToolGenerateData.getSystemDateTime());
		System.out.println(ToolGenerateData.getLongDate());
//		System.out.println(ToolGenerateData.getUUID());
		for (int i = 0; i < 10; i++) {
////			System.out.println(ToolGenerateData.getStringRandom(6));
//			System.out.println(ToolGenerateData.getStringRandomExt2(6));
			System.out.println(ToolGenerateData.getIntData(10));
//			System.out.println(getIntDataExt(1,10));
		}
	}

}
