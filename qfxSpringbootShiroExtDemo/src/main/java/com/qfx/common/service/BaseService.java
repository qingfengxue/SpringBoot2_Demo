package com.qfx.common.service;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.qfx.common.util.Page;

public class BaseService {
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public static String CURRENT_PAGE_NAME="page";
	public static String PAGE_SIZE_NAME="limit";
	
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	public HttpServletResponse response;

	/**
	 * @功能描述：	获取从request中传递过来的参数信息
	 *
	 * @作者：zhangpj		@创建时间：2017年8月15日
	 * @param request
	 */
	public Map<String, Object> getMaps(){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> enume = request.getParameterNames();
		while (enume.hasMoreElements()) {
			String key = (String) enume.nextElement();
			String[] values = request.getParameterValues(key);
			paramMap.put(key, values.length == 1 ? request.getParameter(key).trim() : values);
		}

		return paramMap;
	}
	
	/**
	 * @功能描述：	获取分页信息
	 *
	 * @param map
	 * @return 
	 *
	 * @作者：zhangpj		@创建时间：2018年3月23日
	 */
	public Page getPage(Map<String, Object> map){
		int pageNum=1;
		int pageSize=10;
		if(map.containsKey(CURRENT_PAGE_NAME) && !"".equals(map.get(CURRENT_PAGE_NAME))){
			pageNum=Integer.parseInt((String)map.get(CURRENT_PAGE_NAME));
		}
		if(map.containsKey(PAGE_SIZE_NAME) && !"".equals(map.get(PAGE_SIZE_NAME))){
			pageSize=Integer.parseInt((String)map.get(PAGE_SIZE_NAME));
		}
		Page page=new Page(pageNum,pageSize);
		return page;
	}
	
	/**
	 * <h5>功能:获取String[]类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息或转换失败返回null
	 */
	public String[] getStringArryParam(Map<String, Object> paramMap, String key){
		Object ojb = paramMap.get(key);
		return null == ojb ? null: (String[])ojb;
	}
	
	/**
	 * <h5>功能:获取String类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回null
	 */
	public String getStringParam(Map<String, Object> paramMap, String key){
		Object ojb = paramMap.get(key);
		return null == ojb ? null: String.valueOf(ojb);
	}
	
	/**
	 * <h5>功能:获取int类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0
	 */
	public int getIntParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0 : Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}
	
	/**
	 * <h5>功能:获取double类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0.0
	 */
	public double getDoublePara(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0.0d : Double.parseDouble(value);
		} catch (Exception e) {
			return 0.0d;
		}
	}
	
	/**
	 * <h5>功能:获取float类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0.0
	 */
	public float getFloatParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0f : Float.parseFloat(value);
		} catch (Exception e) {
			return 0.0f;
		}
	}
	
	/**
	 * <h5>功能:获取long类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息返回0
	 */
	public long getLongParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0L : Long.parseLong(value);
		} catch (Exception e) {
			return 0L;
		}
	}
	
	/**
	 * <h5>功能:获取boolean类型参数</h5>
	 * 
	 * @author zhangpj	@date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key 参数名
	 * @return 没有参数信息或转换失败返回false
	 */
	public boolean getBooleanParam(Map<String, Object> paramMap, String key){
		String value = getStringParam(paramMap, key);
		return null == value ? false : Boolean.parseBoolean(value);
	}
}
