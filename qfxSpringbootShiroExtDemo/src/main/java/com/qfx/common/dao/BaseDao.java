package com.qfx.common.dao;

import java.util.List;
import java.util.Map;

public interface BaseDao<T> {
	// 根据条件获取全部信息(必须实现)
	List<T> selectAll(Map<String, Object> paramMap);
	// 根据ID获取信息
	T selectByPrimaryKey(String t_id);
	// 新增信息
	int insert(T t);
	// 批量新增信息
    int insert(List<Map<String, Object>> list);
    // 根据ID更新信息
    int updateByPrimaryKeySelective(T t);
    // 根据ID删除信息
    int deleteByPrimaryKey(String t_id);
    // 根据ID批量删除
    int deleteByPrimaryKey(List<String> list);
    // 根据其他条件批量删除
    int delete(List<String> list);
}
