package com.qfx.common.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import com.qfx.api.entity.SysUser;

/**
 * @功能描述：	拦截控制,用于替换shiro默认的roles拦截规则,改"并且(and)"为"或者 (or)"
 *
 * @作者：zhangpj		@创建时间：2017年5月27日
 */
public class CustomRolesAuthorizationFilter extends AuthorizationFilter{

	private final Logger logger = LogManager.getLogger(getClass()); 
	
	/**
	 * Overriding
	 * @功能描述：	设置同一个URL配置多个角色为"或者"的关系,默认为"并且",
	 * 			如:/user/** = Role["admin,user"],默认必须满足"admin","user"条件,
	 * 			      改为"或者"之后只需要满足一个条件即可(Ini.Section中有此url,会走此方法)
	 *
	 * @作者：zhangpj		@创建时间：2017年5月27日
	 * @param request
	 * @param response
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	@Override
	protected boolean isAccessAllowed(ServletRequest request,ServletResponse response, Object obj) throws Exception {
		Subject subject = getSubject(request, response);
		// 验证是否登录
		if (null == subject.getPrincipals()) {
			return false;
		}
		// 获取请求地址
        HttpServletRequest hsq = (HttpServletRequest) request;
        String requestUrl = hsq.getServletPath();
        // 获取用户信息,这里返回的对象类型与登录验证时
        // new SimpleAuthenticationInfo(user, pwd, this.getName())中的第一个参数的类型需要保持一致
        SysUser user = (SysUser)subject.getPrincipals().getPrimaryPrincipal();
        
        System.out.println("--------1.开启用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤--------");
        
        // 获取角色信息
        String[] rolesArray = (String[]) obj;
        if (rolesArray == null || rolesArray.length == 0) { //没有角色限制，有权限访问
        	System.out.println("--------3.用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤结束--------");
        	logger.info("用户["+user.getUserName()+"]访问["+requestUrl+"]无角色限制,权限验证通过!");
            return true;
        }    
        for (int i = 0; i < rolesArray.length; i++) {    
            if (subject.hasRole(rolesArray[i])) { //若当前用户是rolesArray中的任何一个，则有权限访问  
            	System.out.println("--------3.用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤结束--------");
            	logger.info("用户["+user.getUserName()+"]访问["+requestUrl+"]权限验证通过!");
                return true;    
            }    
        }
        System.out.println("--------3.用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤结束--------");
        logger.info("用户["+user.getUserName()+"]访问["+requestUrl+"]权限验证失败,禁止访问!");
		return false;
	}
}
