package com.qfx.common.vo;

/**
 * @功能描述： JSON模型 用户后台向前台返回的JSON对象
 */
public class MessageBean {
	private boolean result = true;	//是否成功
	private String message = "";	//提示信息
	private Object obj = null;		//其他信息
	private String code = "200";	//返回编码

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
