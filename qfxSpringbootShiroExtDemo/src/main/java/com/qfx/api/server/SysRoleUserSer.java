package com.qfx.api.server;

import java.util.List;

import com.qfx.api.vo.SysRoleUserVo;

public interface SysRoleUserSer {
	/**
	 * <h5>功能:获取用户的角色信息</h5>
	 * 
	 * @return 
	 */
	List<SysRoleUserVo> getSysRoleByUser(String userId);
}
