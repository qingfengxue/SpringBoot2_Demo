package com.qfx.api.server.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qfx.api.dao.SysRoleUserDao;
import com.qfx.api.server.SysRoleUserSer;
import com.qfx.api.vo.SysRoleUserVo;
import com.qfx.common.service.BaseService;

@Service
public class SysRoleUserSerImpl extends BaseService implements SysRoleUserSer {
	@Autowired
	SysRoleUserDao sysRoleUserDao;

	/**
	 * 获取用户的角色信息
	 */
	@Override
	public List<SysRoleUserVo> getSysRoleByUser(String userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		
		return sysRoleUserDao.selectAll(null);
	}

}
