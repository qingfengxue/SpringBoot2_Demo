package com.qfx.api.server;

import java.util.List;

import com.qfx.api.entity.SysUser;
import com.qfx.common.vo.MessageBean;

public interface SysUserSer {
	/**
	 * <h5>功能:获取用户信息集合</h5>
	 * 
	 * @return List<SysUser>
	 */
	List<SysUser> getList();
	
	/**
	 * <h5>功能:获取用户信息</h5>
	 * 
	 * @param userName
	 * @return SysUser
	 */
	SysUser getSysUserByUserName(String userName);
	
	/**
	 * <h5>功能:新增APP用户信息</h5>
	 * 
	 * @param sysUser
	 * @return SysUser
	 */
	MessageBean insertSysUser(SysUser sysUser);
}
