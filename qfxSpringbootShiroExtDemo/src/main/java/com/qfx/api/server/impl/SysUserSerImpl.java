package com.qfx.api.server.impl;

import java.util.List;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qfx.api.dao.SysUserDao;
import com.qfx.api.entity.SysUser;
import com.qfx.api.server.SysUserSer;
import com.qfx.common.service.BaseService;
import com.qfx.common.util.ToolCrypt;
import com.qfx.common.util.ToolResultCode;
import com.qfx.common.vo.MessageBean;

@Service
public class SysUserSerImpl extends BaseService implements SysUserSer {
	
	@Autowired
	private SysUserDao sysUserDao;

	/**
	 * 获取用户信息集合
	 */
	@Override
	public List<SysUser> getList() {
		return sysUserDao.selectAll(null);
	}

	/**
	 * 根据用户名或手机号码获取用户信息
	 */
	@Override
	public SysUser getSysUserByUserName(String userName) {
		// 根据用户名获取用户信息
		return sysUserDao.selectByUserName(userName);
	}

	/**
	 * 新增APP用户信息
	 */
	@Override
	public MessageBean insertSysUser(SysUser sysUser) {
		MessageBean messageBean = new MessageBean();
		try {
			// 1.生成加密盐值和加密后的密码
			sysUser = setUserInfo(sysUser);
			// 2.保存用户信息到数据库中
			sysUserDao.insert(sysUser);
			log.info("用户["+sysUser.getUserName()+"]注册成功");
			messageBean.setMessage("注册成功,请登录!");
			messageBean.setCode(ToolResultCode.USER_REG_SUCCESS);
		} catch (Exception e) {
			log.error("用户["+sysUser.getUserName()+"]注册失败");
			messageBean.setMessage("注册失败,请重新注册");
			messageBean.setObj(sysUser);
			messageBean.setCode(ToolResultCode.USER_REG_FAIL);
		}
		messageBean.setObj(sysUser);
		return messageBean;
	}
	
	// ==================== private method ====================
	
	/**
	 * @功能描述：	生成加密盐值和加密后的密码
	 *
	 * @param sysUser
	 */
	private SysUser setUserInfo(SysUser sysUser){
		//设置加密方式,需要与shiro配置文件中加密方式匹配：SHA-256、SHA-384、SHA-512、MD5
		String algorithmName = "MD5";
		// 注册用户名,如果用户名不存在,默认使用手机号码作为用户名
		String userName = (null == sysUser.getUserName()) ? sysUser.getPhone() : sysUser.getUserName();
		// 注册密码,如果密码不存在,默认使用手机号码作为密码
		String password = (null == sysUser.getPassword()) ? sysUser.getPhone() : sysUser.getPassword();
		// 生成加密盐值:随机数
		String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
		int hashIterations = 3; //设置加密迭代次数,需要与shiro配置文件中加密迭代次数匹配
		
		// 生成密码时使用"用户名+盐值"来生成加密数据
		SimpleHash hash = new SimpleHash(algorithmName, password,userName + salt, hashIterations);
		String encodedPassword = hash.toHex();
		
		// 生成暗码
		String cipher = ToolCrypt.encrypt(password);
		
		sysUser.setUserName(userName);
		sysUser.setPassword(encodedPassword);
		sysUser.setSalt(salt);
		sysUser.setState(1);	// 状态(0:禁用 1:正常 2:锁定)
		sysUser.setSex(2);		// 性别(0:女 1:男 2:未知)
		sysUser.setCipher(cipher);	// 暗码
		sysUser.setRegisterSource(1);	// 注册来源(0:系统 1:App)
		
		return sysUser;
	}
}
