package com.qfx.api.server;

import com.qfx.api.entity.SysUser;
import com.qfx.common.vo.MessageBean;

public interface LoginSer {
	/**
	 * <h5>功能:shiro登录验证</h5>
	 * 如果没有注册则注册返回，已经注册则登录验证
	 * @param sysUser
	 * @return 
	 */
	public MessageBean login(SysUser sysUser);
}
