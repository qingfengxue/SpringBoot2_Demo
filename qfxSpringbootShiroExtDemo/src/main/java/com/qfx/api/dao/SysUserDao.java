package com.qfx.api.dao;

import com.qfx.api.entity.SysUser;
import com.qfx.common.dao.BaseDao;

public interface SysUserDao extends BaseDao<SysUser> {
	/**
	 * <h5>功能:根据用户名获取用户信息</h5>
	 * 
	 * @return SysUser
	 */
	SysUser selectByUserName(String userName);
}