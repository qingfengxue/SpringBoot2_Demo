package com.qfx.api.dao;

import com.qfx.api.vo.SysRoleUserVo;
import com.qfx.common.dao.BaseDao;

public interface SysRoleUserDao extends BaseDao<SysRoleUserVo>{
	
}