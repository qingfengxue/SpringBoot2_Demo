package com.qfx.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.qfx.api.entity.SysUser;
import com.qfx.api.server.SysUserSer;
import com.qfx.common.vo.MessageBean;

@RestController
@RequestMapping("sysUser")
public class SysUserCtl {
	
	@Autowired
	SysUserSer sysUserSer;
	
	@GetMapping("list")
	public String list() {
		List<SysUser> sysUserList = sysUserSer.getList();
		
		MessageBean messageBean = new MessageBean();
		messageBean.setObj(sysUserList);
		
		return JSONObject.toJSONString(messageBean);
	}
}
