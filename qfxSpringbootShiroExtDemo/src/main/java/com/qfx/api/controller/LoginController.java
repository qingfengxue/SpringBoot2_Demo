package com.qfx.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.qfx.api.entity.SysUser;
import com.qfx.api.server.LoginSer;
import com.qfx.api.server.SysUserSer;
import com.qfx.common.util.ToolResultCode;
import com.qfx.common.vo.MessageBean;

@RestController
@RequestMapping("login")
public class LoginController {
	
	@Autowired
	LoginSer loginSer;
	@Autowired
	SysUserSer sysUserSer;
	
	@RequestMapping("register")
	public String register(SysUser sysUser){
		System.out.println("111111");
		MessageBean messageBean = sysUserSer.insertSysUser(sysUser);
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:用户登录</h5>
	 * 
	 * @param sysUser
	 * @return String
	 */
	@RequestMapping("login")
	public String login(SysUser sysUser) {
		MessageBean messageBean;
		// 1.使用手机号作为用户名
		sysUser.setUserName(sysUser.getPhone());
		// 2.获取用户信息(使用手机号作为用户名)
		SysUser user = sysUserSer.getSysUserByUserName(sysUser.getUserName());
		if (null == user) {
			// 如果用户不存在则注册当前用户
			messageBean = sysUserSer.insertSysUser(sysUser);
		} else {
			// shiro登录验证,通过返回true,失败返回false
			messageBean = loginSer.login(sysUser);
		}
		
		return JSONObject.toJSONString(messageBean);
	}
	
	@RequestMapping("401")
	public String errror401() {
		MessageBean messageBean = new MessageBean();
		messageBean.setCode(ToolResultCode.INFO_401);
		messageBean.setMessage("您还没有登录,请登录后再进行操作~!");
		return JSONObject.toJSONString(messageBean);
	}
	
	@RequestMapping("403")
	public String errror403() {
		MessageBean messageBean = new MessageBean();
		messageBean.setCode(ToolResultCode.INFO_403);
		messageBean.setMessage("请求权限不足~!");
		return JSONObject.toJSONString(messageBean);
	}
	
	@RequestMapping("404")
	public String errror404() {
		MessageBean messageBean = new MessageBean();
		messageBean.setCode(ToolResultCode.INFO_404);
		messageBean.setMessage("请求接口不存在~!");
		return JSONObject.toJSONString(messageBean);
	}
	
	@RequestMapping("500")
	public String errror500() {
		MessageBean messageBean = new MessageBean();
		messageBean.setCode(ToolResultCode.INFO_404);
		messageBean.setMessage("请求发生错误~!");
		return JSONObject.toJSONString(messageBean);
	}

}
