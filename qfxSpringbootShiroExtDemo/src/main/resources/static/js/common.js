// 获取项目路径
function getProjectName() {
	// 获取路径
	var pathName = window.document.location.pathname;
	// 截取，得到项目名称
	var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
	
	return projectName;
}

// 跳转到新的路径
function toUrl(url) {
	window.location.href = getProjectName() + url;
}