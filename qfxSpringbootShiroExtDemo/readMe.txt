1.springboot的版本是2.2.1.RELEASE
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增shiro支持
	1.pom.xml
		<!-- 9.开启shiro依赖 -->
		<dependency>
		    <groupId>org.apache.shiro</groupId>
		    <artifactId>shiro-spring-boot-starter</artifactId>
		    <version>1.4.0</version>
		</dependency>
		
	2.shiro相关类
		com.qfx.demo.shiro.UserRealm.java  -- 自定义realm
		com.qfx.demo.shiro.CustomRolesAuthorizationFilter.java -- 自定义拦截方式
		com.qfx.demo.shiro.ShiroConfig.java -- shiro核心配置文件,取代之前的xml配置文件
4.基础数据
	连接数据库(mysql8)
	mysql8的数据库驱动采用com.mysql.cj.jdbc.Driver
	spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
		
5.启动:
	1.直接运行RunApp.java中的main方法即可
	2.复制target目录中的qfxSpringbootShiroExtDemo.war到tomcat中,启动tomcat即可

6.测试地址
	默认首页：http://127.0.0.1/qfxSpringbootShiroExtDemo/login/login?phone=13280009366&password=13280009366
	