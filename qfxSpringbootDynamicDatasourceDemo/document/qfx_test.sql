/*
 Navicat Premium Data Transfer

 Source Server         : 本地_MySql8_3307
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : 127.0.0.1:3307
 Source Schema         : qfx_test

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 20/10/2022 19:00:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL COMMENT '用户编号',
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `sex` int NULL DEFAULT NULL COMMENT '性别 0女 1男 2未知',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (10001, '陈风', 1, 16, NULL);
INSERT INTO `sys_user` VALUES (10002, '林玲', 0, 17, NULL);
INSERT INTO `sys_user` VALUES (10003, '陈婷', 2, 16, NULL);

SET FOREIGN_KEY_CHECKS = 1;
