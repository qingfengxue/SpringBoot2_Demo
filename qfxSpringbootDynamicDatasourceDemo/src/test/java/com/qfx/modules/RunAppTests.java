package com.qfx.modules;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONWriter;
import com.qfx.modules.test.service.TestSer;

@SpringBootTest
class RunAppTests {
	
	@Autowired
	TestSer testSer;
	
	@Test
	void contextLoads() {
		Map<String, Object> map = new HashMap<>();
		map.put("aa", "a");
		map.put("bb", 22);
		map.put("cc", null);
		map.put("dd", 100.00d);
		
		System.out.println(JSONObject.toJSONString(map));
		System.out.println(JSONObject.toJSONString(map, JSONWriter.Feature.WriteMapNullValue));
	}
	
//	@Test
	void serviceTest() {
		List<Object> userAndStudentInfo = testSer.getUserAndStudentInfo();
		for (Object object : userAndStudentInfo) {				
			System.out.println(JSONObject.toJSONString(object));
			System.out.println(JSONObject.toJSONString(object, JSONWriter.Feature.WriteMapNullValue));
		}
	}

}
