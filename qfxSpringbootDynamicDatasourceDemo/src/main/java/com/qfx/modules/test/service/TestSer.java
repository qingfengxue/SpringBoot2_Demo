package com.qfx.modules.test.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import com.qfx.modules.student.dao.StudentDao;
import com.qfx.modules.student.entity.Student;
import com.qfx.modules.system.dao.SysUserDao;
import com.qfx.modules.system.entity.SysUser;

@Service
public class TestSer {
	
	@Autowired
	SysUserDao sysUserDao;
	
	@Autowired
	StudentDao studentDao;
	
	public List<Object> getUserAndStudentInfo() {
		List<SysUser> userList = sysUserDao.selectAll();
		List<Student> studentList = studentDao.selectAll();
		
		List<Object> list = new ArrayList<>();
		list.add(userList);
		list.add(studentList);
		
		return list;
	}
	
	@DSTransactional
	public Map<String, Integer> addUserAndStudent() {
		return saveUserAndStudent();
	}

	// ------------------- private method -------------------
	private Map<String, Integer> saveUserAndStudent() {
		SysUser sysUser = new SysUser();
		sysUser.setName("测试姓名");
		sysUser.setSex(1);
		
		int insertSysUserCount = sysUserDao.insert(sysUser);
		
//		int a=10,b=0;
//		double c = a/b;
		
		Student student = new Student();
		BeanUtils.copyProperties(sysUser, student);
		int	insertStudentCount = studentDao.insert(student);
		
		Map<String, Integer> map = new HashMap<>();
		map.put("insertSysUserCount", insertSysUserCount);
		map.put("insertStudentCount", insertStudentCount);
		return map;
	}
}
