package com.qfx.modules.test.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qfx.modules.test.service.TestSer;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestCtl {

	@Autowired
	TestSer testSer;
	
	@RequestMapping("userAndStudent")
	public List<Object> getUserAndStudentInfo() {
		return testSer.getUserAndStudentInfo();
	}
	
	@RequestMapping("addUserAndStudent")
	public Map<String, Integer> addUserAndStudent() {
		return testSer.addUserAndStudent();
	}
}
