package com.qfx.modules.student.dao;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.qfx.modules.student.entity.Student;

/**
 * @DS可以注解在方法上或类上,如果同时存在则采用就近原则:方法上注解 优先于 类上注解
 */
@DS("db2")
public interface StudentDao {

	@DS("db2")
	Student selectByPK(Long id);
    
    List<Student> selectAll();
    
    int insert(Student student);

}