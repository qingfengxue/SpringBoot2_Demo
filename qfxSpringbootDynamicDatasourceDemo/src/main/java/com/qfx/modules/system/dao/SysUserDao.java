package com.qfx.modules.system.dao;

import java.util.List;

import com.qfx.modules.system.entity.SysUser;

public interface SysUserDao {

    SysUser selectByPK(Long id);
    
    List<SysUser> selectAll();
    
    int insert(SysUser sysUser);
}