1.springboot的版本是2.5.3
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增多数据源支持
	3.1 pom.xml
		<!-- 5.引入多数据源支持 -->
		<dependency>
			<groupId>com.baomidou</groupId>
			<artifactId>dynamic-datasource-spring-boot-starter</artifactId>
			 <version>3.5.2</version> 
		</dependency>

	3.2 配置文件多数据源配置
		# ----------------数据库连接基本配置---------------
		## 连接池类型,这里我们采用hikari连接池
		spring.datasource.type=com.zaxxer.hikari.HikariDataSource
		## 设置默认的数据源或者数据源组,默认值即为master
		spring.datasource.dynamic.primary=master
		## 严格匹配数据源,默认false. true未匹配到指定数据源时抛异常,false使用默认数据源
		spring.datasource.dynamic.strict=false
		
		# ----------------多数据源信息配置---------------
		## 默认数据源("master"为指定的数据源名称,对应spring.datasource.dynamic.primary的值)
		spring.datasource.dynamic.datasource.master.driver-class-name=com.mysql.cj.jdbc.Driver
		spring.datasource.dynamic.datasource.master.url=jdbc:mysql://127.0.0.1:3307/qfx_test?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8
		spring.datasource.dynamic.datasource.master.username=root
		spring.datasource.dynamic.datasource.master.password=666666
		
		## 读库数据源(可多个,"db2"为指定的数据源名称,自定义即可)
		spring.datasource.dynamic.datasource.db2.driver-class-name=com.mysql.cj.jdbc.Driver
		spring.datasource.dynamic.datasource.db2.url=jdbc:mysql://127.0.0.1:3307/qfx_test2?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8
		spring.datasource.dynamic.datasource.db2.username=root
		spring.datasource.dynamic.datasource.db2.password=666666

4.使用方法
	4.1 约定
		(1)配置文件所有以下划线"_"分割的数据源,下划线"_"之前的为组名称,同组名称的数据源会放在一个组下
		(2)切换数据源可以是组名,也可以是具体数据源名称,组名则切换时采用负载均衡算法切换
		(3)默认的数据源名称为"master",可以通过"spring.datasource.dynamic.primary"修改
		(4)方法上的注解优先于类上注解

	4.2 多数据源配置方案	
		# 一主多从           		# 多主多从           		纯粹多库(记得设置primary)          混合配置
		spring:           		spring:                 spring:                        spring:
		  datasource:     		  datasource:             datasource:                    datasource:
		    dynamic:      		    dynamic:                dynamic:                       dynamic:
		      datasource: 		      datasource:             datasource:                    datasource:
		        master: 		        master_1:               mysql:                         master:
		        slave_1: 		        master_2:               oracle:                        slave_1:
		        slave_2: 		        slave_1:                sqlserver:                     slave_2:
		        slave_3:  		        slave_2:                postgresql:                    oracle_1:
		        slave_4:  		        slave_3:                h2:                            oracle_2:
		        
	4.3 使用@DS切换数据源
		@DS可以注解在方法上或类上,如果同时存在则采用就近原则:方法上注解 优先于 类上注解
		
		注解 					结果
		没有@DS 				默认数据源
		@DS("dsName") 		dsName可以为组名也可以为具体某个库的名称
		
		示例：
			@DS("slave")
			public interface StudentDao {
				@DS("slave_1")
				Student selectByPK(Long id);
				
			    List<Student> selectAll();
			}
  	4.4 事物控制
  		多数据源建议使用@DSTransactional,示例:
	  		@DSTransactional
			public Map<String, Integer> addUserAndStudent() {
				return saveUserAndStudent();
			}
  		
  	4.5 官网地址
  		https://gitee.com/baomidou/dynamic-datasource-spring-boot-starter     
5.测试地址
	http://localhost/test/userAndStudent
	