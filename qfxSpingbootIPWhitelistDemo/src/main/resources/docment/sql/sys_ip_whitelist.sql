-- ----------------------------
-- Table structure for sys_ip_whitelist
-- ----------------------------
DROP TABLE IF EXISTS `sys_ip_whitelist`;
CREATE TABLE `sys_ip_whitelist`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'IP地址',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '对IP地址的描述',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '表示该IP地址是否可用 0-不可用，1-可用',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_ip`(`ip_address`) USING BTREE,
  INDEX `ip_addr_index`(`ip_address`) USING BTREE
) COMMENT = 'IP白名单表';

SET FOREIGN_KEY_CHECKS = 1;
