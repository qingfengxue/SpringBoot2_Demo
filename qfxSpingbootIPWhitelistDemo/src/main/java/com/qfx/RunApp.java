package com.qfx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.qfx.modules.*.dao")	// 指定Mapper需要扫描的包,这样就不用每个Mapper上都添加@Mapper注解了
public class RunApp {

	public static void main(String[] args) {
		SpringApplication.run(RunApp.class, args);
		System.out.println("IP白名单拦截示例启动成功!");
	}

}
