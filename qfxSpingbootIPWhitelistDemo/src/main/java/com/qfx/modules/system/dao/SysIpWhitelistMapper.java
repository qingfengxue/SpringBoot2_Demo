package com.qfx.modules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qfx.modules.system.entity.SysIpWhitelist;

public interface SysIpWhitelistMapper extends BaseMapper<SysIpWhitelist>{

}