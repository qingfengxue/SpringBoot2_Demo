package com.qfx.modules.system.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qfx.common.util.ToolRedis;
import com.qfx.modules.system.dao.SysIpWhitelistMapper;
import com.qfx.modules.system.entity.SysIpWhitelist;

@Service
public class SysIpWhitelistSer {
	// 默认有效期30天
    private static long TTL_EXP = 60 * 1 * 1;
    
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    @Autowired
    private ToolRedis toolRedis;
	
	@Autowired
	SysIpWhitelistMapper sysIpWhitelistMapper;
	
	public boolean selectSysIpWhitelistByPk(String ipAddress) {
		QueryWrapper<SysIpWhitelist> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("ip_address", ipAddress);
		queryWrapper.eq("status", 1);
		Integer resultCount = sysIpWhitelistMapper.selectCount(queryWrapper);

		boolean flag = false;
		if (!ObjectUtils.isEmpty(resultCount) && resultCount.equals(1)) {
			flag = true;
		}
		return flag;
	}
	
	/**
     * <h5>描述:验证IP地址是否合法,通过redisTemplate操作redis<h5>
     * 
     * @param ipAddress 客户IP地址
     *
     * @return 是否合法 true-合法 false-不合法
     */
    @SuppressWarnings("unchecked")
	public boolean validetaByIpAddress(String ipAddress){
        boolean flag = false;
        try {
            String key = "sysIpWhitelists";
            List<Object> rangeList = redisTemplate.opsForList().range(key, 0, -1);
            if (ObjectUtils.isEmpty(rangeList)) {
                flag = true;
            } else {
                List<String> ipList = (List<String>) rangeList.get(0);
                if (ipList.contains(ipAddress)) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    
    /**
     * <h5>描述:验证IP地址是否合法<h5>
     * 
     * @param ipAddress 客户IP地址
     *
     * @return 是否合法 true-合法 false-不合法
     */
    @SuppressWarnings("unchecked")
	public boolean validetaByIpAddressExt(String ipAddress){
    	boolean flag = false;
    	try {
    		String key = "sysIpWhitelists";
    		List<Object> rangeList = toolRedis.lGet(key, 0, -1);
    		if (ObjectUtils.isEmpty(rangeList)) {
    			flag = true;
    		} else {
    			List<String> ipList = (List<String>) rangeList.get(0);
    			if (ipList.contains(ipAddress)) {
    				flag = true;
    			}
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return flag;
    }
    
    // ------------------- private method -------------------
    /**
     * <h5>描述:初始化IP白名单信息到缓存<h5>
     */
    @PostConstruct
    private void initToCacheExt(){
    	QueryWrapper<SysIpWhitelist> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("status", 1);
    	List<SysIpWhitelist> sysIpWhitelists = sysIpWhitelistMapper.selectList(queryWrapper);
    	try {
    		List<String> iplist = new ArrayList<>();
    		for (SysIpWhitelist sysIpWhitelist : sysIpWhitelists) {
    			iplist.add(sysIpWhitelist.getIpAddress().trim());
    		}
    		String key = "sysIpWhitelists";
    		toolRedis.lSet(key, iplist, TTL_EXP);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
