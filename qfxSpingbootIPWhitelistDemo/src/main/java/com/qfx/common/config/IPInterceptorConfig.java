package com.qfx.common.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.qfx.common.interceptor.IPInterceptor;

//@Configuration
public class IPInterceptorConfig implements WebMvcConfigurer {
	@Value("${ip.white.list}")
    private String[] ipWhiteList;

    @Value("${ip.interceptor.enabled}")
    private boolean ipInterceptorEnabled;

    @Bean
    public IPInterceptor ipInterceptor() {
        return new IPInterceptor(Arrays.asList(ipWhiteList));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (ipInterceptorEnabled) {
            registry.addInterceptor(ipInterceptor()).addPathPatterns("/**");
        }
    }
}
