package com.qfx.common.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.qfx.common.interceptor.IPInterceptorExtTwo;

@Configuration
public class IPInterceptorConfigExtTwo implements WebMvcConfigurer {

    @Value("${ip.interceptor.enabled}")
    private boolean ipInterceptorEnabled;

    @Autowired
    IPInterceptorExtTwo ipInterceptorExtTwo; 
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (ipInterceptorEnabled) {
        	// 设置需要排除的路径,不需要经过拦截验证
        	List<String> excludePathList = new ArrayList<>();
            excludePathList.add("/swagger-ui.html");
            excludePathList.add("/webjars/springfox-swagger-ui/**");
            excludePathList.add("/swagger-resources/**");
            excludePathList.add("/v2/api-docs");
            excludePathList.add("/druid/**");
            excludePathList.add("/ws/**");
        	
            registry.addInterceptor(ipInterceptorExtTwo)
            		.excludePathPatterns(excludePathList)
//            		.excludePathPatterns("/aaa/test", "/bbb/test/**", "/ccc/**")	// 也可以这么写
            		.addPathPatterns("/**");	// 需要拦截验证的路径
        }
    }
}
