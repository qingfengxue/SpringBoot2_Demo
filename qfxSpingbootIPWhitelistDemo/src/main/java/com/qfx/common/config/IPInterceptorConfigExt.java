package com.qfx.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.qfx.common.interceptor.IPInterceptorExt;

//@Configuration
public class IPInterceptorConfigExt implements WebMvcConfigurer {

    @Value("${ip.interceptor.enabled}")
    private boolean ipInterceptorEnabled;

    @Autowired
    IPInterceptorExt ipInterceptorExt; 
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (ipInterceptorEnabled) {
            registry.addInterceptor(ipInterceptorExt).addPathPatterns("/**");
        }
    }
}
