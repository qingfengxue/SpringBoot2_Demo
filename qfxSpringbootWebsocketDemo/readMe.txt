1.springboot的版本是2.4.6
2.添加或修改项目内容后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增websocket支持
	1.pom.xml
		<!-- 4. 引入websocket支持 -->
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-websocket</artifactId>
		</dependency>
  
4.启动
	1.使用内置tomcat,需要打开com.qfx.demo.common.config.WebSocketConfig.java的配置文件信息,然后运行RunApp.java中的main方法
	2.使用外部tomcat,需要关闭com.qfx.demo.common.config.WebSocketConfig.java的配置文件信息,然后复制target目录中的war包到tomcat中,启动tomcat
	
5.ImSer.java中包含心跳检测的机制

6.测试地址
	springboot启动：
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/webSocket.html
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/webSocket2.html
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/im.html	// 如果连接不上,请进入页面修改websocket的请求地址
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/im2.html	// 如果连接不上,请进入页面修改websocket的请求地址
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/message/sendInfo?msg=大家好啊
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/message/sendInfo?msg=你好啊&cid=cid_0002
	tomcat启动：
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/webSocket.html
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/webSocket2.html
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/message/sendInfo?msg=大家好啊
		http://127.0.0.1/qfxSpringbootWebsocketServerDemo/message/sendInfo?msg=你好啊&cid=cid_0002
	