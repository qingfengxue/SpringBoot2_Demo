package com.qfx.demo.common.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.demo.im.service.WebSocketSer;

/**
 * <h5>描述:测试发送消息</h5>
 *  
 */
@RestController
@RequestMapping("message")
public class SendCtl {
	
	/**
	 * <h5>功能:发送信息给正在连接websocket的所有用户</h5>
	 * 
	 * @param msg 消息内容
	 * @return 
	 */
	@RequestMapping("sendAllInfo")
	public String sendAllInfo(String msg) {
		WebSocketSer.sendInfo(msg, null);
		return "success";
	}
	
	/**
	 * <h5>功能:发送信息给正在连接websocket的指定所有用户</h5>
	 * 
	 * @param msg 消息内容
	 * @param cid 用户id
	 * @return 
	 */
	@RequestMapping("sendInfo")
	public String sendInfo(String msg, String cid) {
		WebSocketSer.sendInfo(msg, cid);
		return "success";
	}
}
