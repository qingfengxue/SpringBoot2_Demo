package com.qfx.demo.common.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.qfx.demo.common.util.ToolIP;

@Component
public class ProjectInfo implements ApplicationRunner {
	
	public final Logger log = LoggerFactory.getLogger(ProjectInfo.class);
	
	@Value("${server.port}")
	String port;
	@Value("${server.servlet.context-path}")
	String projectName;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		String localPort = ToolIP.getPort();
		if (null == localPort) {
			localPort = port;
		}
		System.out.println("当前项目访问地址为：" + ToolIP.getLocalIpAddr() + ":" + localPort + projectName);
		log.info("当前项目访问地址为：{}:{}{}", ToolIP.getLocalIpAddr(), localPort, projectName);
	}
}
