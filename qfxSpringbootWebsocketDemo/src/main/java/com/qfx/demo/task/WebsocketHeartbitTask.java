package com.qfx.demo.task;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.qfx.demo.im.service.ImSer;

@Component
public class WebsocketHeartbitTask {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
    // 第一次执行前延时5秒启动,每次任务结束后5秒再次启动
//    @Scheduled(initialDelay = 5000, fixedDelay = 5000)
	private void heartbeatCheck() {
		JSONObject messageJson = new JSONObject();
		messageJson.put("messageType", "heartbeat");
		
		// 放入List中
		List<JSONObject> chatList = new ArrayList<JSONObject>();
		chatList.add(messageJson);
		
		// 发送心跳
		ImSer.sendHeartbeat(JSONObject.toJSONString(chatList));
	}
}
