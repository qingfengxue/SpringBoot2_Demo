package com.qfx.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("demo")
public class DemoCtl {
	
	@RequestMapping("main")
	public String main() {
		
		return "main";
	}
	
	@RequestMapping("user")
	public String user() {
		
		return "user/user/list";
	}
	
	@RequestMapping("test")
	public String test() {
		return "user/user/test";
	}
	
	@RequestMapping(value = "save", method = {RequestMethod.POST})
	@ResponseBody
	public String save(String name,int age, String sex) {
		System.out.println("name =" + name);
		System.out.println("age =" + age);
		System.out.println("sex =" + sex);
		return "ok";
	}
}
