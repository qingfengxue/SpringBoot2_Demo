1.springboot的版本是2.2.1.RELEASE
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.本项目是Thyemleaf的示例
	
启动:
	1.直接运行RunApp.java中的main方法即可
	2.复制target目录中的qfxSpringbootThyemleaf.war到tomcat中,启动tomcat即可

测试地址
	http://127.0.0.1:8080/qfxSpringbootThyemleafDemo/demo/main
	http://127.0.0.1:8080/qfxSpringbootThyemleafDemo/demo/user
	http://127.0.0.1:8080/qfxSpringbootThyemleafDemo/demo/test
