package com.qfx.demo.cache;

import java.util.HashMap;
import java.util.Map;

import com.qfx.demo.vo.SysMenuRole;


/**
 * @功能描述： 存放菜单角色信息缓存数据
 *
 * @作者：zhangpj @创建时间：2017年5月25日
 */
public class MenuRoleCache {
	// 存放公共信息缓存数据
	public static Map<String, SysMenuRole> menuRoleCacheMap = new HashMap<String, SysMenuRole>();

	static{
		System.out.println("初始化菜单角色信息...");
		
		SysMenuRole menuRole = new SysMenuRole();
		menuRole.setMenuName("/user/**");
		menuRole.setRoleNames("\"游客,超级管理员\"");
		
		SysMenuRole menuRole2 = new SysMenuRole();
		menuRole2.setMenuName("/role/**");
		menuRole2.setRoleNames("\"网管,超级管理员\"");
		
		
		setMenuRoleCacheMap(menuRole.getMenuName(), menuRole);
		setMenuRoleCacheMap(menuRole2.getMenuName(), menuRole2);
		
		System.out.println("初始化菜单角色信息完毕!");
	}

	public static SysMenuRole getMenuRoleCacheMap(String key) {
		return menuRoleCacheMap.get(key);
	}

	public static void setMenuRoleCacheMap(String key, SysMenuRole menu) {
		menuRoleCacheMap.put(key, menu);
	}
}
