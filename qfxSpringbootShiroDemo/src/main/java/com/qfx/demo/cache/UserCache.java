package com.qfx.demo.cache;

import java.util.HashMap;
import java.util.Map;

import com.qfx.demo.vo.SysUser;

/**
 * @功能描述： 存放用户缓存数据
 *
 * @作者：zhangpj @创建时间：2017年5月18日
 */
public class UserCache {
	// 存放公共信息缓存数据
	public static Map<String, SysUser> userCacheMap = new HashMap<String, SysUser>();

	static{
		System.out.println("初始化用户信息开始...");
		
		// 默认密码为 111111
		SysUser user = new SysUser();
		user.setUserName("zhangsan");
		user.setPassWord("aff928fbc45d45f9eb46aba180b59c8f");
		user.setSalt("f651bc1d5c94dd65700bd580aee66d24");
		user.setRoleName("超级管理员");
		
		// 默认密码为 111111
		SysUser user2 = new SysUser();
		user2.setUserName("lisi");
		user2.setPassWord("f17a741a0d9b58519b89a38225f2741d");
		user2.setSalt("35ff8852641405670e18a12f489265bb");
		user2.setRoleName("游客");
		
		// 默认密码为 111111
		SysUser user3 = new SysUser();
		user3.setUserName("wangwu");
		user3.setPassWord("e8a0d9ed06ce26b11003aad58cae1da1");
		user3.setSalt("d82e0aea635c078f51e477f85374d2ad");
		user3.setRoleName("网管"); 
		
		setUserCacheMap(user.getUserName(), user);
		setUserCacheMap(user2.getUserName(), user2);
		setUserCacheMap(user3.getUserName(), user3);
		
		System.out.println("初始化用户信息完毕!");
	}

	public static SysUser getUserCacheMap(String key) {
		return userCacheMap.get(key);
	}

	public static void setUserCacheMap(String key, SysUser user) {
		userCacheMap.put(key, user);
	}
}
