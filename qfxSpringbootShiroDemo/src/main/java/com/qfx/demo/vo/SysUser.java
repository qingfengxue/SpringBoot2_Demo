package com.qfx.demo.vo;

public class SysUser {

	private String userName; // 登录用户名
	private String passWord; // 登录密码
	private String userNameReg; // 注册用户名
	private String passWordReg; // 注册密码
	private String encryptPassWord; // 加密密码
	private String salt; // 加密盐值
	
	private String roleName; // 角色名称

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getUserNameReg() {
		return userNameReg;
	}

	public void setUserNameReg(String userNameReg) {
		this.userNameReg = userNameReg;
	}

	public String getPassWordReg() {
		return passWordReg;
	}

	public void setPassWordReg(String passWordReg) {
		this.passWordReg = passWordReg;
	}

	public String getEncryptPassWord() {
		return encryptPassWord;
	}

	public void setEncryptPassWord(String encryptPassWord) {
		this.encryptPassWord = encryptPassWord;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
