package com.qfx.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qfx.demo.shiro.ShiroPermissionSer;

@Controller
@RequestMapping("roleMenu")
public class RoleMenuController extends BaseController{
	@Autowired
	ShiroPermissionSer shiroPermissionSer;
	
	public RoleMenuController() {
		System.out.println("RoleController 构建了~~~");
	}

	
	@RequestMapping("initChain")
	public String initChain() {
		shiroPermissionSer.updatePermission();
		return "main";
	}
}
