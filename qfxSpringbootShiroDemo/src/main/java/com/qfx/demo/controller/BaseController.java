package com.qfx.demo.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;

public class BaseController {
	
	public BaseController() {
		System.out.println("BaseController 构建了~~~");
	}
	
	/**
	 * @功能描述：	获取参数
	 *
	 * @作者：zhangpj		@创建时间：2016年12月11日
	 * @param req
	 * @return
	 */
	public Map<String,Object> getMaps(HttpServletRequest req){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		// 从HttpServletRequest中获取传递过来的数据
	    Map<String, String[]> map = req.getParameterMap();  
        // 遍历 map
        for (Iterator<Map.Entry<String,String[]>> iter = map.entrySet().iterator(); iter.hasNext();) {  
            Map.Entry<String,String[]> element = (Map.Entry<String,String[]>) iter.next();  
            // 获取key值  
            String strKey = element.getKey();  
            // 获取value,默认为数组形式
            String[] value = element.getValue();  
            
            // 存放到指定的map集合中
            if (value.length <= 1) {
            	// 没有或者只有一个value值
            	paramMap.put(strKey, value[0]);
			}else{
				// 有多个value值
				paramMap.put(strKey, value);
			}
        }

        return paramMap;
	}
	
	/**
	 * @功能描述：	跳转到首页面
	 *
	 * @作者：zhangpj		@创建时间：2016年12月11日
	 * @return
	 */
	@RequestMapping("index")
	public String login(){
		System.out.println("跳转到login列表页面");
		return "../index";
	}
	
}
