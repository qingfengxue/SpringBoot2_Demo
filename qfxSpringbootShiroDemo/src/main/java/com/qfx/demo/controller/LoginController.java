package com.qfx.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qfx.demo.service.LoginService;
import com.qfx.demo.vo.SysUser;

@Controller
@RequestMapping("login")
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	/**
	 * @功能描述：	用户注册
	 *
	 * @作者：zhangpj		@创建时间：2017年6月2日
	 * @param request
	 * @param sysUser
	 * @return
	 */
	@RequestMapping("register")
	public String register(HttpServletRequest request,SysUser sysUser){
		// 保存用户注册信息
		loginService.saveUser(request,sysUser);
		
		return "../index";
	}
	
	/**
	 * @功能描述：	用户登录
	 *
	 * @作者：zhangpj		@创建时间：2017年6月2日
	 * @param request
	 * @param sysUser
	 * @return
	 */
	@RequestMapping("login")
	public String login(HttpServletRequest request,SysUser sysUser) {
		String resultPageURL = "../index";
		
		// shiro登录验证,通过返回true,失败返回false
		boolean loginResult = loginService.login(request, sysUser);
		if (loginResult) {
			// 验证通过
			resultPageURL = "redirect:/login/mainPage";
		}
		
		return resultPageURL;
	}
	
	/**
	 * @功能描述：	跳转到主页面
	 *
	 * @作者：zhangpj		@创建时间：2017年6月2日
	 * @return
	 */
	@RequestMapping("mainPage")
	public String toMainPage(){
		return "main";
	}
}
