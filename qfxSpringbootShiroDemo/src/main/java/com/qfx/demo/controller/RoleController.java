package com.qfx.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("role")
public class RoleController extends BaseController{
	
	public RoleController() {
		System.out.println("RoleController 构建了~~~");
	}

	/**
	 * @功能描述：	跳转到list页面
	 *
	 * @作者：zhangpj		@创建时间：2016年12月11日
	 * @return
	 */
	@RequestMapping("list")
	public String list(){
		System.out.println("跳转到user列表页面");
		return "system/role/list";
	}
}
