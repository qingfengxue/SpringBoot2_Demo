package com.qfx.demo.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import com.qfx.demo.cache.UserCache;
import com.qfx.demo.vo.SysUser;

public class UserRealm extends AuthorizingRealm {
	
//	@Autowired
//	private SessionDAO sessionDAO;

	/**
	 * Overriding
	 * @功能描述：	权限验证
	 *
	 * @作者：zhangpj		@创建时间：2017年1月24日
	 * @param arg0
	 * @return
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		// 1.获取用户信息
		//   这里principals.getPrimaryPrincipal()的返回的对象类型与登录验证时
		//   new SimpleAuthenticationInfo(user, pwd, this.getName())中的第一个参数的类型需要保持一致
		SysUser user = (SysUser)principals.getPrimaryPrincipal();
		System.out.println("--------2.用户["+user.getUserName()+"]进行权限验证--------");
		
		// 2.单独定一个集合对象放置角色信息 
		Set<String> roles = new HashSet<String>();
		roles.add(user.getRoleName());
        
        // 3.查到权限数据，返回授权信息(要包括 上边的permissions)  
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo(roles);
        
		return simpleAuthorizationInfo;
	}

	/**
	 * Overriding
	 * @功能描述：	登陆验证
	 *
	 * @作者：zhangpj		@创建时间：2017年1月24日
	 * @param arg0
	 * @return
	 * @throws AuthenticationException
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken token) throws AuthenticationException {
		System.out.println("---------用户登录验证---------");
		// 1.获取登录名称
		String userName = token.getPrincipal().toString();
//		System.out.println("userName:"+userName);
		// 2.根据登录名称获取用户信息(从缓存获取,正式项目从数据库)
		SysUser user = UserCache.getUserCacheMap(userName);
		if (null == user) {
			// 抛出账户不存在的异常
			throw new UnknownAccountException();
		}
		// 3.获取查询到到密码和盐值
		String pwd = user.getPassWord();
		String salt = user.getSalt();

		// 4.交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配
		//   new SimpleAuthenticationInfo(user, pwd, this.getName())中的user是SysUser对象,在其他接收的地方也要转成SysUser对象
		SimpleAuthenticationInfo authcInfo = new SimpleAuthenticationInfo(user, pwd, this.getName());  
        // 设置盐值(salt = userName + salt)
		authcInfo.setCredentialsSalt(ByteSource.Util.bytes(userName + salt));
         
        return authcInfo; 
	}
}
