<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'main.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
  	欢迎用户<b> <shiro:principal property="userName"/> </b>登录系统~,
  	您的sessionID是[<label style="color: red"><%=request.getSession().getId()%></label>]
  	<p><a href="<%=basePath%>user/list">查看user列表信息</a><br/></p>
	<p><a href="<%=basePath%>role/list">查看role列表信息</a><br/></p>
	<p><a href="<%=basePath%>roleMenu/initChain">刷新权限</a><br/></p>
	<a href="<%=basePath%>logout">退出</a><br/>
  </body>
</html>
