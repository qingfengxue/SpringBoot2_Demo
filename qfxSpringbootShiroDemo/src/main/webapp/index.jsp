<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my index page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<link rel="stylesheet" type="text/css"	href="<%=basePath%>css/login/css/demo.css" />
<link rel="stylesheet" type="text/css"	href="<%=basePath%>css/login/css/style3.css" />
<link rel="stylesheet" type="text/css"	href="<%=basePath%>css/login/css/animate-custom.css" />
<script type="text/javascript">
	if ('<%=request.getSession().getAttribute("isLogin")%>' == "true") {
		window.location.href="<%=basePath%>login/mainPage";
	}
	if(${messageBean.result == false}){
		window.location.href='<%=basePath%>#toregister';
		$("#resultMsg").value(${messageBean.message});
	}
</script>

</head>

<body>
	This is my index.jsp page.<label style="color: red" id="resultMsg">${messageBean.message }</label>
	<p>
		<br/>
		<b>已有用户和密码：<br/>
			zhangsan:111111<br/>
			lisi:111111<br/>
			wangwu:111111<br/>
		</b>
	</p>
	<div class="container">
		<div style="text-align:center;clear:both;"></div>
		<section>
			<div id="container_demo">
				<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor" id="tologin"></a>
				<div id="wrapper">
					<div id="login" class="animate form">
						<form action="<%=basePath%>login/login" autocomplete="on" method="post">
							<h1>用户登录</h1>
							<p>
								<label for="userName" class="uname" data-icon="u">用户名</label>
								<input id="userName" name="userName" required="required" type="text" value="zhangsan" placeholder="请输入用户名" />
							</p>
							<p>
								<label for="passWord" class="youpasswd" data-icon="p">密　码</label>
								<input id="passWord" name="passWord" required="required" type="passWord" value="111111" placeholder="请输入密码" />
							</p>
							<p class="login button">
								<input type="reset" value="重　置" />
								<input type="submit" value="登　录" />
							</p>
							<p class="change_link">
								${null == messageBean.message ? '还没有账号?' : messageBean.message }<a href="index.jsp#toregister" class="to_register">去注册</a>
							</p>
						</form>
					</div>

					<div id="register" class="animate form">
						<form action="<%=basePath%>login/register" autocomplete="on" method="post">
							<h1>用户注册</h1>
							<label></label>
							<p>
								<label for="userNameReg" class="uname" data-icon="u">用户名</label>
								<input id="userNameReg" name="userNameReg" required="required" type="text" placeholder="用户名" />
							</p>
							<p>
								<label for="passWordReg" class="youpasswd" data-icon="p">密　码</label>
								<input id="passWordReg" name="passWordReg" required="required" type="passWord" placeholder="请输入密码" />
							</p>
							<p>
								<label for="passWordRegConfirm" class="youpasswd" data-icon="p">确认密码</label>
								<input id="passWordRegConfirm" name="passWordsignupConfirm" required="required" type="passWord" placeholder="请确认密码" />
							</p>
							<p class="signin button">
								<input type="reset" value="重　置" />
								<input type="submit" value="注　册" />
							</p>
							<p class="change_link">
								已经有账号了?<a href="index.jsp#tologin" class="to_register">去登录</a>
							</p>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>

</html>
