1.springboot的版本是2.2.1.RELEASE
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增shiro支持
	1.pom.xml
		<!-- 9.开启shiro依赖 -->
		<dependency>
		    <groupId>org.apache.shiro</groupId>
		    <artifactId>shiro-spring-boot-starter</artifactId>
		    <version>1.4.0</version>
		</dependency>
		
	2.shiro相关类
		com.qfx.demo.shiro.UserRealm.java  -- 自定义realm
		com.qfx.demo.shiro.CustomRolesAuthorizationFilter.java -- 自定义拦截方式
		com.qfx.demo.shiro.ShiroConfig.java -- shiro核心配置文件,取代之前的xml配置文件
4.基础数据
	因为是测试使用,所以未连接数据库,是写死在缓存里面的
		com.qfx.demo.cache.MenuRoleCache.java -- 权限缓存
		com.qfx.demo.cache.RoleCache.java -- 角色缓存
		com.qfx.demo.cache.UserCache.java -- 用户缓存
		
5.更新:
	新增:
		同一个浏览器,如果已经登录,再跳转到登录页面话,会自动跳转到默认已登录页面
		支持动态刷新权限

6.启动:
	1.直接运行RunApp.java中的main方法即可
	2.复制target目录中的qfxSpringbootShiroDemo.war到tomcat中,启动tomcat即可

7.测试地址
		默认首页：http://localhost/qfxSpringbootShiroDemo
	