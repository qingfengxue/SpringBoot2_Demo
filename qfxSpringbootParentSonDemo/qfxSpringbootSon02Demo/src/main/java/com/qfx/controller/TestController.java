package com.qfx.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("son02/test")
public class TestController {

	@RequestMapping("sayHello")
	public String sayHello() {
		return "嗨~你好,我是聚合项目的子项目qfxSpringbootSon_02_Demo.";
	}
}
