package com.qfx;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * <h5>描述添加外部tomcat支持,需要继承SpringBootServletInitializer,并重写configure方法:</h5>
 */
@SpringBootApplication
public class RunApp extends SpringBootServletInitializer{
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(RunApp.class);
	}

	public static void main(String[] args) {
		// 整个程序入口，启动springboot项目，创建内置tomcat服务器，使用tomct加载springmvc注解启动类
		SpringApplication.run(RunApp.class, args);
	}
}
