package com.qfx.common.api;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <h5>描述:qfx-hotel项目的相关公共接口</h5>
 *  
 */
public interface HotelApi {
	
	@RequestMapping("order")
	String order();
	
}
