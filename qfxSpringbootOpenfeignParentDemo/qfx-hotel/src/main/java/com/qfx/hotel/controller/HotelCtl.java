package com.qfx.hotel.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.common.api.HotelApi;

@RestController
@RequestMapping("hotel")
public class HotelCtl implements HotelApi {
	
	@Value("${server.port}")
	String serverPort;

	@Override
	public String order() {
		String str = "通过编号为[" + serverPort + "]的服务端下单成功!";
		System.out.println(str);
		
		return str;
	}
}
