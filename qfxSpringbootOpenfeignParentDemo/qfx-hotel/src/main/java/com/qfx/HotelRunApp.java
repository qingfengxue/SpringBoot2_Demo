package com.qfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelRunApp {
	public static void main(String[] args) {
		SpringApplication.run(HotelRunApp.class, args);
	}
}
