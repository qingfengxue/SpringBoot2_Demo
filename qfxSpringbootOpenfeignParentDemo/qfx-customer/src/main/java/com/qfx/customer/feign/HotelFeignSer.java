package com.qfx.customer.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.qfx.common.api.HotelApi;

/**
 * <h5>描述:openfeign客户端接口</h5>
 *  
 */
@FeignClient(name = "hotel-service", path = "hotel")
public interface HotelFeignSer extends HotelApi{

}
