package com.qfx.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.customer.feign.HotelFeignSer;

@RestController
@RequestMapping("customer")
public class CustomerCtrl {
	
	@Value("${server.port}")
	String serverPort;
	@Autowired
	HotelFeignSer HotelFeignSer;

	@RequestMapping("buy")
	public String buy() {
		String str = "客户使用编号为[" + serverPort + "]的服务端订购了一间海景房!";
		
		str += HotelFeignSer.order();
		System.out.println(str);
		
		return str;
	}
}
