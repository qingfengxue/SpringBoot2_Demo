package com.qfx.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test02")
public class TestController {
	
	@RequestMapping("hello")
	public String hello() {
		return "Hello China!";
	}
	
	@RequestMapping("save")
	public String save() {
		String msg = "新增一名用户!";
		System.out.println(msg);
		
		return msg;
	}
}
