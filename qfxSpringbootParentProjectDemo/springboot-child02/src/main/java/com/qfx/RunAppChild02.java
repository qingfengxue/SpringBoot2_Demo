package com.qfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunAppChild02 {

	public static void main(String[] args) {
		SpringApplication.run(RunAppChild02.class, args);
	}
}
