1. 项目介绍
	这是一个Maven(Springboot)聚合项目的示例,同时也是一个分布式项目的示例
	
2. 目录结构
	qfxSpringbootParentProjectDemo	// 父工程
	│
	├─springboot-child01			// 子工程01
	│
	└─springboot-child02			// 子工程02
		
3. 测试地址：
	springboot-child01
		http://localhost:8080/test01/hello	// 普通请求
		http://localhost:8080/test01/add	// 分布式请求,调用了springboot-child02的请求地址
		
	springboot-child02
		http://localhost:8081/test02/hello
		
	
