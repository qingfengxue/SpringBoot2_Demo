package com.qfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunAppChild01 {

	public static void main(String[] args) {
		SpringApplication.run(RunAppChild01.class, args);
	}
}
