package com.qfx.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("test01")
public class TestController {
	
	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping("hello")
	public String hello() {
		return "Hello world!";
	}
	
	@RequestMapping("add")
	public String add() {
		System.out.println("新增一条信息!");
		// 调用springboot-child02中的请求
        String msg = restTemplate.getForObject("http://localhost:8081/test02/save", String.class);
        
        return "新增成功!" + msg;
	}
}
