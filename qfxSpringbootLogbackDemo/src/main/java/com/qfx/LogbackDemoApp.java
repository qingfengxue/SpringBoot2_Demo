package com.qfx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class LogbackDemoApp {
	
	private final static Logger logger = LoggerFactory.getLogger(LogbackDemoApp.class);

	public static void main(String[] args) {
		SpringApplication.run(LogbackDemoApp.class, args);
		System.out.println("Logback示例服务[LogbackDemoApp]启动成功!");
		
		logger.debug("这是一个debug日志");
        logger.info("这是一个info日志");
        logger.warn("这是一个warn日志");
        logger.error("这是一个error日志");
	}

}
