package com.qfx.main;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qfx.modules.system.dao.SysUserDao;
import com.qfx.modules.system.entity.SysUser;
import com.qfx.modules.system.service.SysUserSer;

@SpringBootTest
class RunAppTests {
	
	@Autowired
	SysUserDao userDao;
	
	@Autowired
	SysUserSer sysUserSer;

//	@Test
	void contextLoads() {
		QueryWrapper<SysUser> qw = new QueryWrapper<SysUser>();
		qw.eq("user_id", "10001");
		List<SysUser> sysUserList = userDao.selectList(qw);

		for (SysUser sysUser : sysUserList) {
			System.out.println(JSONObject.toJSONString(sysUser));
		}
	}
	
//	@Test
	void contextLoadsExt() {
		QueryWrapper<SysUser> qw = new QueryWrapper<SysUser>();
		qw.eq("user_id", "10001");
		
		List<SysUser> sysUserList = sysUserSer.list(qw);
		
		for (SysUser sysUser : sysUserList) {
			System.out.println(JSONObject.toJSONString(sysUser));
		}
	}
	
//	@Test
	void insertTest() {
		SysUser sysUser = new SysUser();
//		sysUser.setUserId("1001017");
		sysUser.setUserName("琳琳");
		sysUser.setPassword("1");
		sysUser.setIslock(1);
		sysUser.setSex("女");
		sysUser.setTrueName("孙琳琳");
		
		int count = userDao.insert(sysUser);
		System.out.println("insert result count = " + count);
	}
	
//	@Test
	void test() {
		SysUser sysUser = userDao.selectOne(new QueryWrapper<SysUser>().eq("user_name", "琳琳"));
		System.out.println(JSONObject.toJSONString(sysUser));
	}
	
	@Test
	void selectPKTest() {
		SysUser sysUser = userDao.selectByPK("10001");
		
		System.out.println(JSONObject.toJSONString(sysUser));
	}

}
