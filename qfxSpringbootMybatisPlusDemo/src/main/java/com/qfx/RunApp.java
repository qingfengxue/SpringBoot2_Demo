package com.qfx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
@MapperScan("com.qfx.modules.*.dao")	// 指定Mapper需要扫描的包,这样就不用每个Mapper上都添加@Mapper注解了
public class RunApp extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(RunApp.class, args);
		Environment environment = context.getBean(Environment.class);
		String port = environment.getProperty("server.port");
		String ctxPath = environment.getProperty("server.servlet.context-path");
		
		port = port == null ? "80" : port;
		ctxPath = ctxPath == null ? "/" : ctxPath;

		System.out.println("系统 已启动！请访问  http://localhost:" + port + ctxPath);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RunApp.class);
	}
}
