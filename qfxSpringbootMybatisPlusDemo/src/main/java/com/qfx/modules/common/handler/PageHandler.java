package com.qfx.modules.common.handler;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * MyBatis-plus分页封装类,简化分页操作,格式化返回数据
 */
@Component
public class PageHandler<T> {
	
	private static String PAGE_NO = "pageNo";
	private static String PAGE_SIZE = "pageSize";
	
	/**
	 * 功能：无条件分页查询
	 *
	 * @param baseMapper 指定的查询接口
	 * @param pageNum 当前页页码
	 * @param pageSize 页面显示条数
	 * 
	 * @return Map<String, Object>
	 */
	public Map<String, Object> pageList(BaseMapper<T> baseMapper, int pageNum, int pageSize) {
        return pageList(baseMapper, Wrappers.emptyWrapper(), pageNum, pageSize);
    }
	
	/**
	 * 功能：翻页查询
	 *
	 * @param baseMapper 指定的查询接口
	 * @param queryWrapper 实体对象封装操作类
	 * @param pageNum 当前页页码
	 * @param pageSize 页面显示条数
	 * 
	 * @return Map<String, Object>
	 */
	public Map<String, Object> pageList(BaseMapper<T> baseMapper, Wrapper<T> queryWrapper, int pageNum ,int pageSize) {
		// 创建分页对象，指定当前页和每页显示的记录数
		Page<T> page = new Page<>(pageNum, pageSize);
		// 执行带条件的分页查询
		page = baseMapper.selectPage(page, queryWrapper);
		
		return buildPageResult(page);
	}
	
	/**
	 * 功能：无条件分页查询
	 *
	 * @param baseMapper 指定的查询接口
	 * @param request 当前页页码
	 * 
	 * @return Map<String, Object>
	 */
	public Map<String, Object> pageList(BaseMapper<T> baseMapper, HttpServletRequest request) {
        return pageList(baseMapper, Wrappers.emptyWrapper(), request);
    }
	
	/**
	 * 功能：翻页查询
	 *
	 * @param baseMapper 指定的查询接口
	 * @param queryWrapper 实体对象封装操作类
	 * @param request 当前页页码
	 * 
	 * @return Map<String, Object>
	 */
	public Map<String, Object> pageList(BaseMapper<T> baseMapper, Wrapper<T> queryWrapper, HttpServletRequest request) {
		// 创建分页对象，获取分页信息
		Page<T> page = getPage(request);
		// 执行带条件的分页查询
		page = baseMapper.selectPage(page, queryWrapper);
		
		return buildPageResult(page);
	}
	
	/**
	 * 功能：根据请求获取分页对象
	 * 该方法从HTTP请求中提取分页参数，并根据这些参数创建一个分页对象
	 * 主要目的是为了简化分页查询的处理过程，将分页相关的逻辑封装到一个方法中
	 *
	 * @param request HTTP请求对象，用于获取分页参数
	 *                   
	 * @return 返回一个分页对象，该对象包含了当前页码和每页大小
	 */
	public Page<T> getPage(HttpServletRequest request) {
		// 从URL中获取page参数
        String pageNo = request.getParameter(PAGE_NO);
        // 从URL中获取limit参数
        String pageSize = request.getParameter(PAGE_SIZE);
        
        // 将参数转换为长整数（注意：这里应该添加异常处理，以防参数不是有效的长整数）
        long current = ObjectUtils.isEmpty(pageNo) ? 1L : Long.parseLong(pageNo); // 默认为第1页
        long size = ObjectUtils.isEmpty(pageSize) ? 10L : Long.parseLong(pageSize); // 默认为每页10条
		Page<T> page = new Page<>(current, size);
		
		return page;
	}

	/**
	 * 功能：构建分页查询结果
	 *
	 * 该方法用于将MyBatis-Plus的Page对象转换为一个包含分页信息和数据的Map对象
	 * 主要用于接口返回，以便前端可以获取到分页查询的相关信息和数据
	 *
	 * @param page MyBatis-Plus的Page对象，包含了分页查询的结果和分页信息
	 *
	 * @return 返回一个Map对象，包含了分页码、总记录数、每页记录数和数据列表
	 */
	public Map<String, Object> buildPageResult(Page<T> page) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("count", page.getTotal());
		map.put("page_count", page.getPages());
		map.put("data", page.getRecords());
		
		return map;
	}
}
