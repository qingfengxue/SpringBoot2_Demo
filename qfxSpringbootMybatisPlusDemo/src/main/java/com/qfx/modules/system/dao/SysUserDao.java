package com.qfx.modules.system.dao;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qfx.modules.system.entity.SysUser;

@Repository
public interface SysUserDao extends BaseMapper<SysUser> {
	
	/**
	 * 自定义分页查询
	 * @param page MyBatis-plus分页插件信息
	 * @return IPage<SysUser>
	 */
	Page<SysUser> selectList(Page<SysUser> page, SysUser sysUser);
	
	SysUser selectByPK(long id);
	SysUser selectByPK(String id);
	SysUser selectByPK(int id);
	
	
}