package com.qfx.modules.system.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qfx.modules.system.entity.SysUser;

public interface SysUserSer extends IService<SysUser> {
	
	/**
	 * 分页查询
	 * @param sysUser
	 * 
	 * @return
	 */
	Map<String, Object> pageExt(SysUser sysUser);
	
	/**
	 * 自定义sql分页查询
	 * @param sysUser
	 * 
	 * @return
	 */
	Map<String, Object> pageExtTwo(SysUser sysUser);
}
