package com.qfx.modules.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qfx.modules.system.entity.SysUser;
import com.qfx.modules.system.service.SysUserSer;

@Controller
@RequestMapping("sysTest")
public class SysUserCtl {
	@Autowired
	HttpServletRequest request;
	@Autowired
	SysUserSer sysUserSer;
	
	/**
	 * <h5>功能:跳转到测试表列表页面</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("list/view")
	public String listView(){
		return "sys/user/list";
	}
	
	/**
	 * <h5>功能:跳转到测试表新增页面</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("add/view")
	public String addView(){
		return "sys/user/add";
	}
	
	/**
	 * <h5>描述:跳转到测试表编辑页面</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("edit/view")
	public String editView(SysUser sysUser){
		request.setAttribute("sysUser", sysUser);
		
		return "sys/user/edit";
	}
	

	/**
	 * <h5>描述:获取测试表列表数据</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("list")
	@ResponseBody
	public String list(SysUser sysUser) {
		List<SysUser> sysUserList = sysUserSer.list(new QueryWrapper<SysUser>());
		
		return JSONObject.toJSONString(sysUserList);
	}
	
	/**
	 * <h5>描述:分页获取测试表列表数据</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("page")
	@ResponseBody
	public String page(SysUser sysUser) {
		// 创建分页对象，指定当前页和每页显示的记录数
        Page<SysUser> page = new Page<>(1, 10);

        // 执行带条件的分页查询
        page = sysUserSer.page(page);

        // 获取并打印查询结果
        List<SysUser> records = page.getRecords();
        long total = page.getTotal();
        long pages = page.getPages();

        System.out.println("总记录数: " + total);
        System.out.println("总页数: " + pages);
        System.out.println("当前页记录: ");
 
        Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("count", total);
		map.put("data", records);
		
        return JSONObject.toJSONString(map);
	}
	
	/**
	 * <h5>描述:分页获取测试表列表数据</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("pageExt")
	@ResponseBody
	public String pageExt(SysUser sysUser) {
		Map<String, Object> mapPage = sysUserSer.pageExt(sysUser);
		
		return JSONObject.toJSONString(mapPage);
	}
	
	/**
	 * <h5>描述:自定义分页获取测试表列表数据</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("pageExtTwo")
	@ResponseBody
	public String pageExtTwo(SysUser sysUser) {
		Map<String, Object> mapPage = sysUserSer.pageExtTwo(sysUser);
		
		return JSONObject.toJSONString(mapPage);
	}
	
	/**
	 * <h5>功能:新增测试表</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("add")
	@ResponseBody
	public String add(SysUser sysUser){
		boolean flag = sysUserSer.save(sysUser);
		
		return JSONObject.toJSONString(flag);
	}
	
	/**
	 * <h5>功能:编辑测试表</h5>
	 * 
	 * @param sysUser
	 * @return 
	 */
	@RequestMapping("edit")
	@ResponseBody
	public String edit(SysUser sysUser){
		boolean flag = sysUserSer.updateById(sysUser);
		
		return JSONObject.toJSONString(flag);
	}
	
	/**
	 * <h5>功能:删除测试表</h5>
	 * 
	 * @param String
	 * @return 
	 */
	@RequestMapping("del")
	@ResponseBody
	public String del(String id){
		boolean flag = sysUserSer.removeById(id);
		
		return JSONObject.toJSONString(flag);
	}
}
