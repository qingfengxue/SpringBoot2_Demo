package com.qfx.modules.system.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

public class SysUser {
  
	// 用户编号
	@TableId(type = IdType.INPUT)
	private Long userId;
	// 用户名称
	
	private String userName;
	// 用户密码
	
	private String password;
	// 是否锁定(否:0,是:1)
	
	private Integer islock;
	// 创建时间
	
	private Date creatTime;
	// 最后登陆时间
	
	private Date lastTime;
	// 真实姓名
	
	private String trueName;
	// 
	
	private String sex;
	// 生日
	
	private String birthday;
	// 电子邮箱
	
	private String email;
	// 余额
	
	private BigDecimal money;
	// 备注
	
	private String remark;
    
	public Long getUserId() {
        return userId;
    }	
	
	public void setUserId(Long userId) {
        this.userId = userId;
    }
	  
	public String getUserName() {
        return userName;
    }	
	public void setUserName(String userName) {
        this.userName = (userName == null) ? null : userName.trim();
    }
	
	  
	public String getPassword() {
        return password;
    }	
	public void setPassword(String password) {
        this.password = (password == null) ? null : password.trim();
    }
	
	  
	public Integer getIslock() {
        return islock;
    }	
	
	public void setIslock(Integer islock) {
        this.islock = islock;
    }
	  
	public Date getCreatTime() {
        return creatTime;
    }	
	
	public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }
	  
	public Date getLastTime() {
        return lastTime;
    }	
	
	public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
	  
	public String getTrueName() {
        return trueName;
    }	
	public void setTrueName(String trueName) {
        this.trueName = (trueName == null) ? null : trueName.trim();
    }
	
	  
	public String getSex() {
        return sex;
    }	
	public void setSex(String sex) {
        this.sex = (sex == null) ? null : sex.trim();
    }
	
	  
	public String getBirthday() {
        return birthday;
    }	
	public void setBirthday(String birthday) {
        this.birthday = (birthday == null) ? null : birthday.trim();
    }
	
	  
	public String getEmail() {
        return email;
    }	
	public void setEmail(String email) {
        this.email = (email == null) ? null : email.trim();
    }
	
	  
	public BigDecimal getMoney() {
        return money;
    }	
	
	public void setMoney(BigDecimal money) {
        this.money = money;
    }
	  
	public String getRemark() {
        return remark;
    }	
	public void setRemark(String remark) {
        this.remark = (remark == null) ? null : remark.trim();
    }
	
	  
}