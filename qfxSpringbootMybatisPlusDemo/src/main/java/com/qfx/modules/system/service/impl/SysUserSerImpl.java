package com.qfx.modules.system.service.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qfx.modules.common.handler.PageHandler;
import com.qfx.modules.system.dao.SysUserDao;
import com.qfx.modules.system.entity.SysUser;
import com.qfx.modules.system.service.SysUserSer;

@Service
public class SysUserSerImpl extends ServiceImpl<SysUserDao, SysUser> implements SysUserSer {
	
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private PageHandler<SysUser> pageHandler;
	@Autowired
	private SysUserDao sysUserDao;

	@Override
	public Map<String, Object> pageExt(SysUser sysUser) {
		// 方式一,使用QueryWrapper
		// 将user_name设置为like条件，其他sysUser对象参数eq条件
//		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
//		if (!ObjectUtils.isEmpty(sysUser.getUserName())) {
//			queryWrapper.like("user_name", sysUser.getUserName());
//			sysUser.setUserName(null);
//		}
//		queryWrapper.setEntity(sysUser);
		
		// 方式二,使用LambdaQueryWrapper(推荐)
		// 将user_name设置为like条件，其他sysUser对象参数eq条件
		LambdaQueryWrapper<SysUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		if (!ObjectUtils.isEmpty(sysUser.getUserName())) {
			lambdaQueryWrapper.like(SysUser :: getUserName, sysUser.getUserName());
			sysUser.setUserName(null);
		}
		lambdaQueryWrapper.setEntity(sysUser);
		
		return pageHandler.pageList(baseMapper, lambdaQueryWrapper, request);
	}

	@Override
	public Map<String, Object> pageExtTwo(SysUser sysUser) {
		Page<SysUser> page = pageHandler.getPage(request);
		page = sysUserDao.selectList(page, sysUser);
		
		return pageHandler.buildPageResult(page);
	}

}
