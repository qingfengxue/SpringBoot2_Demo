/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯
 Source Server Type    : MySQL
 Source Server Version : 80200
 Source Host           : 82.156.116.249:3306
 Source Schema         : qfx_test

 Target Server Type    : MySQL
 Target Server Version : 80200
 File Encoding         : 65001

 Date: 30/12/2024 15:47:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单编号',
  `fid` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '父菜单编号',
  `menu_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '菜单链接',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单图标url',
  `show_order` int NULL DEFAULT NULL COMMENT '显示排序',
  `state` int NULL DEFAULT 1 COMMENT '状态(0:禁用 1:正常)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '首页', '/home', 'icon-home', 1, 1, '2023-05-11 09:34:15', '首页菜单');
INSERT INTO `sys_menu` VALUES ('10', '0', '客户管理', '', 'icon-customer', 4, 1, '2023-05-11 09:34:15', '客户管理菜单');
INSERT INTO `sys_menu` VALUES ('11', '10', '客户列表', '/customer/list', 'icon-list', 1, 1, '2023-05-11 09:34:15', '客户列表菜单');
INSERT INTO `sys_menu` VALUES ('12', '10', '新增客户', '/customer/new', 'icon-new', 2, 1, '2023-05-11 09:34:15', '新增客户菜单');
INSERT INTO `sys_menu` VALUES ('13', '0', '报表管理', '', 'icon-report', 5, 1, '2023-05-11 09:34:15', '报表管理菜单');
INSERT INTO `sys_menu` VALUES ('14', '13', '销售报表', '/report/sales', 'icon-sales', 1, 1, '2023-05-11 09:34:15', '销售报表菜单');
INSERT INTO `sys_menu` VALUES ('15', '13', '库存报表', '/report/inventory', 'icon-inventory', 2, 1, '2023-05-11 09:34:15', '库存报表菜单');
INSERT INTO `sys_menu` VALUES ('16', '0', '设置', '', 'icon-setting', 6, 1, '2023-05-11 09:34:15', '设置菜单');
INSERT INTO `sys_menu` VALUES ('17', '16', '个人信息', '/setting/profile', 'icon-profile', 1, 1, '2023-05-11 09:34:15', '个人信息菜单');
INSERT INTO `sys_menu` VALUES ('18', '16', '修改密码', '/setting/password', 'icon-password', 2, 1, '2023-05-11 09:34:15', '修改密码菜单');
INSERT INTO `sys_menu` VALUES ('19', '16', '系统日志', '/setting/log', 'icon-log', 3, 1, '2023-05-11 09:34:15', '系统日志菜单');
INSERT INTO `sys_menu` VALUES ('2', '0', '系统管理', '', 'icon-setting', 2, 1, '2023-05-11 09:34:15', '系统管理菜单');
INSERT INTO `sys_menu` VALUES ('20', '0', '帮助中心', '/help', 'icon-help', 7, 1, '2023-05-11 09:34:15', '帮助中心菜单');
INSERT INTO `sys_menu` VALUES ('21', '0', '产品管理', '', 'icon-product', 8, 1, '2023-05-11 09:34:34', '产品管理菜单');
INSERT INTO `sys_menu` VALUES ('22', '21', '产品列表', '/product/list', 'icon-list', 1, 1, '2023-05-11 09:34:34', '产品列表菜单');
INSERT INTO `sys_menu` VALUES ('23', '21', '新增产品', '/product/new', 'icon-new', 2, 1, '2023-05-11 09:34:34', '新增产品菜单');
INSERT INTO `sys_menu` VALUES ('24', '21', '产品分类', '/product/category', 'icon-category', 3, 1, '2023-05-11 09:34:34', '产品分类菜单');
INSERT INTO `sys_menu` VALUES ('25', '0', '消息中心', '/message', 'icon-message', 9, 1, '2023-05-11 09:34:34', '消息中心菜单');
INSERT INTO `sys_menu` VALUES ('26', '0', '日历管理', '/calendar', 'icon-calendar', 10, 1, '2023-05-11 09:34:34', '日历管理菜单');
INSERT INTO `sys_menu` VALUES ('27', '0', '邮件管理', '', 'icon-email', 11, 1, '2023-05-11 09:34:34', '邮件管理菜单');
INSERT INTO `sys_menu` VALUES ('28', '27', '收件箱', '/email/inbox', 'icon-inbox', 1, 1, '2023-05-11 09:34:34', '收件箱菜单');
INSERT INTO `sys_menu` VALUES ('29', '27', '发件箱', '/email/outbox', 'icon-outbox', 2, 1, '2023-05-11 09:34:34', '发件箱菜单');
INSERT INTO `sys_menu` VALUES ('3', '2', '用户管理', '/user', 'icon-user', 1, 1, '2023-05-11 09:34:15', '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('30', '27', '草稿箱', '/email/drafts', 'icon-drafts', 3, 1, '2023-05-11 09:34:34', '草稿箱菜单');
INSERT INTO `sys_menu` VALUES ('31', '27', '垃圾箱', '/email/trash', 'icon-trash', 4, 1, '2023-05-11 09:34:34', '垃圾箱菜单');
INSERT INTO `sys_menu` VALUES ('32', '0', '知识库', '/knowledge', 'icon-knowledge', 12, 1, '2023-05-11 09:34:34', '知识库菜单');
INSERT INTO `sys_menu` VALUES ('33', '32', '文章列表', '/knowledge/list', 'icon-list', 1, 1, '2023-05-11 09:34:34', '文章列表菜单');
INSERT INTO `sys_menu` VALUES ('34', '32', '新增文章', '/knowledge/new', 'icon-new', 2, 1, '2023-05-11 09:34:34', '新增文章菜单');
INSERT INTO `sys_menu` VALUES ('35', '32', '分类管理', '/knowledge/category', 'icon-category', 3, 1, '2023-05-11 09:34:34', '分类管理菜单');
INSERT INTO `sys_menu` VALUES ('36', '0', '任务管理', '', 'icon-task', 13, 1, '2023-05-11 09:34:34', '任务管理菜单');
INSERT INTO `sys_menu` VALUES ('37', '36', '任务列表', '/task/list', 'icon-list', 1, 1, '2023-05-11 09:34:34', '任务列表菜单');
INSERT INTO `sys_menu` VALUES ('38', '36', '新增任务', '/task/new', 'icon-new', 2, 1, '2023-05-11 09:34:34', '新增任务菜单');
INSERT INTO `sys_menu` VALUES ('39', '36', '任务分类', '/task/category', 'icon-category', 3, 1, '2023-05-11 09:34:34', '任务分类菜单');
INSERT INTO `sys_menu` VALUES ('4', '2', '角色管理', '/role', 'icon-role', 2, 1, '2023-05-11 09:34:15', '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('40', '0', '退出系统', '/logout', 'icon-logout', 14, 1, '2023-05-11 09:34:34', '退出系统菜单');
INSERT INTO `sys_menu` VALUES ('5', '2', '权限管理', '/permission', 'icon-permission', 3, 1, '2023-05-11 09:34:15', '权限管理菜单');
INSERT INTO `sys_menu` VALUES ('6', '0', '订单管理', '', 'icon-order', 3, 1, '2023-05-11 09:34:15', '订单管理菜单');
INSERT INTO `sys_menu` VALUES ('7', '6', '新建订单', '/order/new', 'icon-new', 1, 1, '2023-05-11 09:34:15', '新建订单菜单');
INSERT INTO `sys_menu` VALUES ('8', '6', '订单列表', '/order/list', 'icon-list', 2, 1, '2023-05-11 09:34:15', '订单列表菜单');
INSERT INTO `sys_menu` VALUES ('9', '6', '订单详情', '/order/detail', 'icon-detail', 3, 1, '2023-05-11 09:34:15', '订单详情菜单');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL COMMENT '用户编号',
  `user_name` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `password` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `islock` bit(1) NULL DEFAULT b'0' COMMENT '是否锁定(否:0,是:1)',
  `creat_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `last_time` datetime NULL DEFAULT NULL COMMENT '最后登陆时间',
  `true_name` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `sex` varchar(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '',
  `birthday` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生日',
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `money` decimal(10, 0) NULL DEFAULT NULL COMMENT '余额',
  `state` int NOT NULL COMMENT '状态:0禁用 1可用',
  `remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`, `state`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'user1', 'password1', b'0', '2023-01-01 12:00:00', '2023-01-01 12:00:00', '真实姓名1', '男', '1990-01-01', 'email1@example.com', 100, 1, '备注1');
INSERT INTO `sys_user` VALUES (2, 'user2', 'password2', b'0', '2023-02-01 12:00:00', '2023-02-01 12:00:00', '真实姓名2', '女', '1991-02-01', 'email2@example.com', 200, 1, '备注2');
INSERT INTO `sys_user` VALUES (3, 'user3', 'password3', b'0', '2023-03-01 12:00:00', '2023-03-01 12:00:00', '真实姓名3', '男', '1992-03-01', 'email3@example.com', 300, 1, '备注3');
INSERT INTO `sys_user` VALUES (4, 'user4', 'password4', b'0', '2023-04-01 12:00:00', '2023-04-01 12:00:00', '真实姓名4', '女', '1993-04-01', 'email4@example.com', 400, 1, '备注4');
INSERT INTO `sys_user` VALUES (5, 'user5', 'password5', b'0', '2023-05-01 12:00:00', '2023-05-01 12:00:00', '真实姓名5', '男', '1994-05-01', 'email5@example.com', 500, 1, '备注5');
INSERT INTO `sys_user` VALUES (6, 'user6', 'password6', b'0', '2023-06-01 12:00:00', '2023-06-01 12:00:00', '真实姓名6', '女', '1995-06-01', 'email6@example.com', 600, 1, '备注6');
INSERT INTO `sys_user` VALUES (7, 'user7', 'password7', b'0', '2023-07-01 12:00:00', '2023-07-01 12:00:00', '真实姓名7', '男', '1996-07-01', 'email7@example.com', 700, 1, '备注7');
INSERT INTO `sys_user` VALUES (8, 'user8', 'password8', b'0', '2023-08-01 12:00:00', '2023-08-01 12:00:00', '真实姓名8', '女', '1997-08-01', 'email8@example.com', 800, 1, '备注8');
INSERT INTO `sys_user` VALUES (9, 'user9', 'password9', b'0', '2023-09-01 12:00:00', '2023-09-01 12:00:00', '真实姓名9', '男', '1998-09-01', 'email9@example.com', 900, 1, '备注9');
INSERT INTO `sys_user` VALUES (10, 'user10', 'password10', b'0', '2023-10-01 12:00:00', '2023-10-01 12:00:00', '真实姓名10', '女', '1999-10-01', 'email10@example.com', 1000, 1, '备注10');
INSERT INTO `sys_user` VALUES (11, 'user11', 'password11', b'0', '2023-11-01 12:00:00', '2023-11-01 12:00:00', '真实姓名11', '男', '2000-11-01', 'email11@example.com', 1100, 1, '备注11');
INSERT INTO `sys_user` VALUES (12, 'user12', 'password12', b'0', '2023-12-01 12:00:00', '2023-12-01 12:00:00', '真实姓名12', '女', '2001-12-01', 'email12@example.com', 1200, 1, '备注12');
INSERT INTO `sys_user` VALUES (13, 'user13', 'password13', b'0', '2024-01-01 12:00:00', '2024-01-01 12:00:00', '真实姓名13', '男', '2002-01-01', 'email13@example.com', 1300, 1, '备注13');
INSERT INTO `sys_user` VALUES (14, 'user14', 'password14', b'0', '2024-02-01 12:00:00', '2024-02-01 12:00:00', '真实姓名14', '女', '2003-02-01', 'email14@example.com', 1400, 1, '备注14');
INSERT INTO `sys_user` VALUES (15, 'user15', 'password15', b'0', '2024-03-01 12:00:00', '2024-03-01 12:00:00', '真实姓名15', '男', '2004-03-01', 'email15@example.com', 1500, 1, '备注15');
INSERT INTO `sys_user` VALUES (16, 'user16', 'password16', b'0', '2024-04-01 12:00:00', '2024-04-01 12:00:00', '真实姓名16', '女', '2005-04-01', 'email16@example.com', 1600, 1, '备注16');
INSERT INTO `sys_user` VALUES (17, 'user17', 'password17', b'0', '2024-05-01 12:00:00', '2024-05-01 12:00:00', '真实姓名17', '男', '2006-05-01', 'email17@example.com', 1700, 1, '备注17');
INSERT INTO `sys_user` VALUES (18, 'user18', 'password18', b'0', '2024-06-01 12:00:00', '2024-06-01 12:00:00', '真实姓名18', '女', '2007-06-01', 'email18@example.com', 1800, 1, '备注18');
INSERT INTO `sys_user` VALUES (19, 'user19', 'password19', b'0', '2024-07-01 12:00:00', '2024-07-01 12:00:00', '真实姓名19', '男', '2008-07-01', 'email19@example.com', 1900, 1, '备注19');
INSERT INTO `sys_user` VALUES (20, 'user20', 'password20', b'0', '2024-08-01 12:00:00', '2024-08-01 12:00:00', '真实姓名20', '女', '2009-08-01', 'email20@example.com', 2000, 1, '备注20');
INSERT INTO `sys_user` VALUES (21, 'user21', 'password21', b'0', '2024-09-01 12:00:00', '2024-09-01 12:00:00', '真实姓名21', '男', '2010-09-01', 'email21@example.com', 2100, 1, '备注21');
INSERT INTO `sys_user` VALUES (22, 'user22', 'password22', b'0', '2024-10-01 12:00:00', '2024-10-01 12:00:00', '真实姓名22', '女', '2011-10-01', 'email22@example.com', 2200, 1, '备注22');
INSERT INTO `sys_user` VALUES (23, 'user23', 'password23', b'0', '2024-11-01 12:00:00', '2024-11-01 12:00:00', '真实姓名23', '男', '2012-11-01', 'email23@example.com', 2300, 1, '备注23');
INSERT INTO `sys_user` VALUES (24, 'user24', 'password24', b'0', '2024-12-01 12:00:00', '2024-12-01 12:00:00', '真实姓名24', '女', '2013-12-01', 'email24@example.com', 2400, 1, '备注24');
INSERT INTO `sys_user` VALUES (25, 'user25', 'password25', b'0', '2025-01-01 12:00:00', '2025-01-01 12:00:00', '真实姓名25', '男', '2014-01-01', 'email25@example.com', 2500, 1, '备注25');
INSERT INTO `sys_user` VALUES (26, 'user26', 'password26', b'0', '2025-02-01 12:00:00', '2025-02-01 12:00:00', '真实姓名26', '女', '2015-02-01', 'email26@example.com', 2600, 1, '备注26');
INSERT INTO `sys_user` VALUES (27, 'user27', 'password27', b'0', '2025-03-01 12:00:00', '2025-03-01 12:00:00', '真实姓名27', '男', '2016-03-01', 'email27@example.com', 2700, 1, '备注27');
INSERT INTO `sys_user` VALUES (28, 'user28', 'password28', b'0', '2025-04-01 12:00:00', '2025-04-01 12:00:00', '真实姓名28', '女', '2017-04-01', 'email28@example.com', 2800, 1, '备注28');
INSERT INTO `sys_user` VALUES (29, 'user29', 'password29', b'0', '2025-05-01 12:00:00', '2025-05-01 12:00:00', '真实姓名29', '男', '2018-05-01', 'email29@example.com', 2900, 1, '备注29');
INSERT INTO `sys_user` VALUES (30, 'user30', 'password30', b'0', '2025-06-01 12:00:00', '2025-06-01 12:00:00', '真实姓名30', '女', '2019-06-01', 'email30@example.com', 3000, 1, '备注30');
INSERT INTO `sys_user` VALUES (31, 'user31', 'password31', b'0', '2025-07-01 12:00:00', '2025-07-01 12:00:00', '真实姓名31', '男', '2020-07-01', 'email31@example.com', 3100, 1, '备注31');
INSERT INTO `sys_user` VALUES (32, 'user32', 'password32', b'0', '2025-08-01 12:00:00', '2025-08-01 12:00:00', '真实姓名32', '女', '2021-08-01', 'email32@example.com', 3200, 1, '备注32');
INSERT INTO `sys_user` VALUES (33, 'user33', 'password33', b'0', '2025-09-01 12:00:00', '2025-09-01 12:00:00', '真实姓名33', '男', '2022-09-01', 'email33@example.com', 3300, 1, '备注33');
INSERT INTO `sys_user` VALUES (34, 'user34', 'password34', b'0', '2025-10-01 12:00:00', '2025-10-01 12:00:00', '真实姓名34', '女', '2023-10-01', 'email34@example.com', 3400, 1, '备注34');
INSERT INTO `sys_user` VALUES (35, 'user35', 'password35', b'0', '2025-11-01 12:00:00', '2025-11-01 12:00:00', '真实姓名35', '男', '2024-11-01', 'email35@example.com', 3500, 1, '备注35');
INSERT INTO `sys_user` VALUES (36, 'user36', 'password36', b'0', '2025-12-01 12:00:00', '2025-12-01 12:00:00', '真实姓名36', '女', '2025-12-01', 'email36@example.com', 3600, 1, '备注36');

SET FOREIGN_KEY_CHECKS = 1;
