1.springboot的版本是2.5.3
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增MyBatis-plus支持
	3.1 pom.xml
		<!-- 7.引入MyBatis-plus连接支持 -->
		<dependency>
		    <groupId>com.baomidou</groupId>
		    <artifactId>mybatis-plus-boot-starter</artifactId>
		    <version>3.4.3.1</version>
		</dependency>
		
	3.2 分页插件(否则page等分页方法不生效)
		3.2.1  从3.5.9版本起,使用分页插件需单独引入 mybatis-plus-jsqlparser依赖,3.5.9以下版本无需单独引入
			<!-- jdk 8+ 引入可选模块 -->
			<dependency>
			    <groupId>com.baomidou</groupId>
			    <artifactId>mybatis-plus-jsqlparser-4.9</artifactId>
			    <version>${mybatis-plus.version}</version>
			</dependency>
			
			<!-- jdk 11+ 引入可选模块 -->
			<dependency>
			    <groupId>com.baomidou</groupId>
			    <artifactId>mybatis-plus-jsqlparser</artifactId>
			    <version>${mybatis-plus.version}</version>
			</dependency>
	
		3.2.2 开启分页插件
			import org.springframework.context.annotation.Bean;
			import org.springframework.context.annotation.Configuration;
			
			import com.baomidou.mybatisplus.annotation.DbType;
			import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
			import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
			
			@Configuration
			public class MybatisPlusConfig {
			    /**
			     * 添加分页插件
			     */
			    @Bean
			    public MybatisPlusInterceptor mybatisPlusInterceptor() {
			        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
			        // 如果配置多个插件, 切记分页最后添加
			        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
			        // 如果有多数据源可以不配具体类型, 否则都建议配上具体的 DbType
			        return interceptor;
			    }
			}

4.基础数据
	连接数据库(mysql8)
	mysql8的数据库驱动采用com.mysql.cj.jdbc.Driver
	spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
		
5.启动:
	1.直接运行RunApp.java中的main方法即可
	2.复制target目录中的qfxSpringbootMybatisPlusDemo.war到tomcat中,启动tomcat即可

6.测试地址
	执行src/test/java资源包下的com.qfx.main.RunAppTests.java即可从控制台看到输出结果
	
7.官方文档
	https://baomidou.com/introduce/
	