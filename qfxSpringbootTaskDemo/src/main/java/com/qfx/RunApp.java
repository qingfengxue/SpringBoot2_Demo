package com.qfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RunApp extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(RunApp.class);
    }

    public static void main(String[] args) {
        // 整个程序入口，启动springboot项目，创建内置tomcat服务器，使用tomct加载springmvc注解启动类
        SpringApplication.run(RunApp.class, args);
    }
}
