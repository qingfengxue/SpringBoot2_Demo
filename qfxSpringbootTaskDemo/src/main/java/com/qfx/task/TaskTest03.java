package com.qfx.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

//@Component
public class TaskTest03 {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	//输出时间格式
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:sss");
    
 // 延时5秒启动,固定间隔执行
    @Scheduled(initialDelay = 5000, fixedDelay = 15000)
    private void sayHello2(){
    	String dateTime = format.format(new Date());
    	log.info("{} 向宇宙发出了一声问候: 你好,世界", dateTime);
    	try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    // 延时5秒启动,固定频率执行
    @Scheduled(initialDelay = 5000, fixedRate = 15000)
    private void sayHello3(){
    	String dateTime = format.format(new Date());
    	log.info("{} 又升了一级!", dateTime);
    	try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
