package com.qfx.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaskTest02 {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	//输出时间格式
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:sss");
    
    @Scheduled(cron = "0/16 * * * * ? ")
    private void sayHello2(){
    	String dateTime = format.format(new Date());
    	log.info("{} 向宇宙发出了一声问候: 你好,世界", dateTime);
    }
}
