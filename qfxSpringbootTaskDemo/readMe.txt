1.这个是基于Springboot2的多线程执行定时任务的示例,升级Springboot版本到2.4.6

2.实现方式一
	请参考com.qfx.config中的文件,两种实现方式,二选一即可,推荐TaskConfig
	
3.实现方式二(Springboot2.1及以上版本支持,推荐)
	在配置文件中application.properties中设定spring.task.scheduling.pool.size=<要设定的线程数>即可
	注意：com.qfx.config目录中的两个文件需要删除,否则不生效