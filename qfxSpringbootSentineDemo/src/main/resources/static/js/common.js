// 获取项目路径
function getProjectName() {
	// 获取路径
	var pathName = window.document.location.pathname;
	// 截取，得到项目名称
	var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
	
	return projectName;
}

// 获取项目地址+项目名
function getRootPath() {
	var pathName = window.location.pathname.substring(1);
	var webName = pathName == '' ? '' : pathName.substring(0, pathName.indexOf('/'));
	if (webName == "") {
		return window.location.protocol + '//' + window.location.host;
	} else {
		return window.location.protocol + '//' + window.location.host + '/' + webName;
	}
}

// 跳转到新的路径
function toUrl(url) {
	window.location.href = getProjectName() + url;
}

// 数字转换(支持小数)
function numberFormat(value) {
    var param = {};
    var k = 10000,
        sizes = ['', '万', '亿', '万亿'],
        i;
        if(value < k){
            param.value =value
            param.unit=''
        }else{
            i = Math.floor(Math.log(value) / Math.log(k)); 
      
            param.value = ((value / Math.pow(k, i))).toFixed(2);
            param.unit = sizes[i];
        }
    return param.value + param.unit;
}
