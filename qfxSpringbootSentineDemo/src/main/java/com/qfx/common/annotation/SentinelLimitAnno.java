package com.qfx.common.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface SentinelLimitAnno {
	
	/**
	 * 频率,可选,默认:5
	 */
    int limitCount() default 5;
    
	/**
	 * 资源路径,必选
	 */
	String resourceName();
}
