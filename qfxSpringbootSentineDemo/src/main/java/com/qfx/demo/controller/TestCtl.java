package com.qfx.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.common.annotation.SentinelLimitAnno;

@RestController
@RequestMapping("test")
public class TestCtl {

	@RequestMapping("test")
	@SentinelLimitAnno(resourceName = "test")
	public String test() {
		return "调用成功了";
	}
}
