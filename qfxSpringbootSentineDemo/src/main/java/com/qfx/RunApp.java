package com.qfx;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * <h5>描述添加外部tomcat支持,需要继承SpringBootServletInitializer,并重写configure方法:</h5>
 * 
 * @author zhangpj	2018年9月18日 
 */
@SpringBootApplication
public class RunApp {

	
	public static void main(String[] args) {
		// 整个程序入口,启动springboot项目,创建内置tomcat服务器,使用tomct加载springmvc注解启动类
		ConfigurableApplicationContext context = SpringApplication.run(RunApp.class, args);
		
		Environment environment = context.getBean(Environment.class);
		String port = environment.getProperty("server.port");
		String ctxPath = environment.getProperty("server.servlet.context-path");
		ctxPath = ctxPath == null ? "/" : ctxPath;

		System.out.println("系统 已启动！请访问 http://localhost:" + port + ctxPath);
	}
}
