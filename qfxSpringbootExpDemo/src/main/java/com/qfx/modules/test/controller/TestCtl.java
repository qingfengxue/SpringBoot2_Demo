package com.qfx.modules.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfx.modules.common.bean.MessageBean;
import com.qfx.modules.test.service.TestService;

@Controller
@RequestMapping("test")
public class TestCtl {
	
	@Autowired
	TestService testService;
	
	@RequestMapping("main/view")
	public String mainView() {
		System.out.println("进来了");
		return "main";
	}
	
	/**
	 * <h5>描述:导出Excel临时文件,并返回文件名称(路径+名称),单Sheet页</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("exp")
	public ResponseEntity<MessageBean> exp() {
		MessageBean mb = testService.exp();
		
		return ResponseEntity.ok(mb);
	}
	
	/**
	 * <h5>描述:导出Excel临时文件,并返回文件名称(路径+名称),多Sheet页</h5>
	 * 
	 * @return 
	 */
	@RequestMapping("expTwo")
	public ResponseEntity<MessageBean> expTwo() {
		MessageBean mb = testService.expTwo();
		
		return ResponseEntity.ok(mb);
	}
	
	/**
	 * <h5>描述:导出Excel临时文件,并返回文件名称(路径+名称)(方式一),单Sheet页</h5>
	 * 直接返回ResponseEntity<?>即可
	 * 
	 * @return 
	 */
	@RequestMapping("expExt")
	public ResponseEntity<?> expExt() {
		testService.expExt();
		return ResponseEntity.ok().build();
	}
	
	/**
	 * <h5>描述:直接导出Excel文件,无临时文件,(方式二),单Sheet页</h5>
	 * 数据流方式导出要使用@ResponseBody且无需返回任何信息
	 * 
	 * @return 
	 */
	@RequestMapping("expExtTwo")
	@ResponseBody
	public void expExtTwo() {
		testService.expExt();
	}
	
	/**
	 * <h5>描述:直接导出Excel文件,无临时文件,(方式二),多Sheet页</h5>
	 * 数据流方式导出要使用@ResponseBody且无需返回任何信息
	 * 
	 * @return 
	 */
	@RequestMapping("expExtThree")
	@ResponseBody
	public void expExtThree() {
		testService.expExtThree();
	}
}
