package com.qfx.modules.test.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.qfx.modules.common.bean.MessageBean;
import com.qfx.modules.common.util.ToolDate;
import com.qfx.modules.common.util.ToolExcelExp;

@Service
public class TestService {
	
	@Autowired
	HttpServletResponse response;
	
	@Value("${temp.file.path}")
	public String tempFilePath;
	
	/**
	 * 导出Excel文件到服务器,并返回文件名称(路径+名称),单Sheet页
	 * @return
	 */
	public MessageBean exp()  {
		String fileName = ToolDate.getNowTimeToSec() + ".xlsx";
		String savePath = tempFilePath + fileName;
		List<String[]> dataList = ToolExcelExp.createData();
		boolean flag = ToolExcelExp.exportBigDataExcel(savePath, dataList);
		if (flag) {
			return new MessageBean("导出文件失败", fileName);
		}
		
		return new MessageBean(1001, "导出文件失败");
    }
	
	/**
	 * 导出Excel文件到服务器,并返回文件名称(路径+名称),多Sheet页
	 * @return
	 */
	public MessageBean expTwo()  {
		String fileName = ToolDate.getNowTimeToSec() + ".xlsx";
		String savePath = tempFilePath + fileName;
		List<String[]> dataList = ToolExcelExp.createData();
		boolean flag = ToolExcelExp.exportBigDataExcelExt(savePath, dataList);
		if (flag) {
			return new MessageBean("导出文件失败", fileName);
		}
		
		return new MessageBean(1001, "导出文件失败");
	}
	
	/**
	 * 直接导出Excel文件,无临时文件,单Sheet页
	 * @return
	 */
	public void expExt()  {
		String fileName = ToolDate.getNowTimeToSec() + ".xlsx";
		List<String[]> dataList = ToolExcelExp.createData();
		ToolExcelExp.downloadBigDataExcel(response, fileName, dataList);
	}
	
	/**
	 * 直接导出Excel文件,无临时文件,多Sheet页
	 * @return
	 */
	public void expExtThree()  {
		String fileName = ToolDate.getNowTimeToSec() + ".xlsx";
		List<String[]> dataList = ToolExcelExp.createData();
		ToolExcelExp.downloadBigDataExcelExt(response, fileName, dataList);
	}
}
