package com.qfx.modules.test.task;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.qfx.modules.common.util.ToolFiles;

@Component
public class FileCleanTask {
	
	// 设定时间阈值为30分钟
	private static final int MINUTES_THRESHOLD = 30;
	
	@Value("${temp.file.path}")
	private String tempFilePath;
	

    /**
     * 延时5秒启动,间隔半小时执行一次,清理指定目录下最后一次修改时间在30分钟之前的文件
     */
    @Scheduled(initialDelay = 1000 * 5, fixedDelay = 1000 * 1800)
    public void cleanupFiles() {
        ToolFiles.cleanFiles(tempFilePath, MINUTES_THRESHOLD);
    }
}
