package com.qfx.modules.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.qfx.modules.common.util.ToolIP;

/**
 * <h5>描述:显示系统基本信息</h5>
 *  
 */
@Component
public class ProjectInfo implements ApplicationListener<ApplicationReadyEvent> {
	
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Value("${server.port}")
	String port;
	@Value("${server.servlet.context-path}")
	String projectName;
	@Value("${spring.profiles.active}")
	String projectActive;
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		String localPort = ToolIP.getPort();
		if (null == localPort) {
			localPort = port;
		}
		String projectUrl = "0.0.0.0";
		if ("prod".equals(projectActive)) {
			projectUrl =  " http://" + ToolIP.getPublicIpAddr() + ":" + localPort + projectName;
		} else {
			projectUrl = " http://" + ToolIP.getLocalIpAddr() + ":" + localPort + projectName;
		}
		log.info("===============当前项目运行的是[{}]环境================", projectActive);
		log.info(projectUrl);
		log.info("=================================================");
	}
}
