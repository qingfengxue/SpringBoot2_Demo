package com.qfx.modules.common.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class ToolExcelExp {
	
	/**
	 * <h5>功能:导出信息到Excel文件(单Sheet页)</h5>
	 * 导出成功后可将文件地址转为网络地址,通过超链接直接下载
	 * 
	 * @param savePath 文件保存物理路径(绝对地址)
	 * @param dataList 要导出的数据集合
	 * @return
	 */
	public static boolean exportBigDataExcel(String savePath, List<String[]> dataList) {
		return exportBigDataExcel(savePath, null, dataList);
	}
	

	/**
	 * <h5>功能:导出信息到Excel文件(单Sheet页)</h5>
	 * 导出成功后可将文件地址转为网络地址,通过超链接直接下载
	 * 
	 * @param savePath 文件保存物理路径(绝对地址)
	 * @param sheetName Sheet页名称
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static boolean exportBigDataExcel(String savePath, String sheetName, List<String[]> dataList) {
		
		long startTime = System.currentTimeMillis();
		boolean flag = false;
		
		sheetName = (null == sheetName || "".equals(sheetName)) ? "Sheet1" : sheetName;
		
		
        // 最重要的就是使用SXSSFWorkbook，表示流的方式进行操作
        // 在内存中保持100行，超过100行将被刷新到磁盘
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        Sheet sh = wb.createSheet(sheetName); // 建立新的sheet对象
        sh.setDefaultColumnWidth(20); // 以字符设置表格宽度

        for (int i = 0, dataListSize = dataList.size(); i < dataListSize; i++) {
        	String[] str = dataList.get(i);
            Row row_value = sh.createRow(i);
            for (int j = 0, strLength = str.length; j < strLength; j++) {
            	Cell cel_value = row_value.createCell(j);
            	cel_value.setCellValue(str[j]);
			}
        }
        FileOutputStream fileOut;
		try {
			File fileDir = new File(savePath.substring(0, savePath.replace("\\", "/").lastIndexOf("/")));
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			fileOut = new FileOutputStream(savePath);
			wb.write(fileOut);
			fileOut.close();
			wb.dispose();
			wb.close();
			flag = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("文件[" + savePath + "]导出完毕,共耗时" + (endTime-startTime) + "毫秒");
		
		return flag;
    }
	
	/**
	 * <h5>功能:导出信息到Excel文件(多Sheet页)</h5>
	 * 导出成功后可将文件地址转为网络地址,通过超链接直接下载
	 * 
	 * @param savePath 文件保存物理路径(绝对地址)
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static boolean exportBigDataExcelExt(String savePath, List<String[]> dataList) {
		return exportBigDataExcelExt(savePath, null, dataList);
	}
	
	/**
	 * <h5>功能:导出信息到Excel文件(多Sheet页)</h5>
	 * 导出成功后可将文件地址转为网络地址,通过超链接直接下载
	 * 
	 * @param savePath 文件保存物理路径(绝对地址)
	 * @param sheetName Sheet页名称
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static boolean exportBigDataExcelExt(String savePath, String sheetName, List<String[]> dataList) {
		long startTime = System.currentTimeMillis();
		boolean flag = false;
		int sheetMaxRows = 20; // 每个Sheet的最大行数
		int sheetIndex = 1;  // Sheet页索引
		int dataListSize = dataList.size(); // 数据总数
		
		sheetName = (null == sheetName || "".equals(sheetName)) ? "Sheet" : sheetName;

		// 最重要的就是使用SXSSFWorkbook，表示流的方式进行操作
        // 在内存中保持100行，超过100行将被刷新到磁盘
		try (SXSSFWorkbook wb = new SXSSFWorkbook(100); // 创建SXSSFWorkbook对象
			 FileOutputStream fileOut = new FileOutputStream(savePath)) { // 创建文件输出流

			Sheet sh = wb.createSheet(sheetName + sheetIndex++);
			sh.setDefaultColumnWidth(20); // 以字符设置表格宽度

			// 添加第一页的表头
			String[] headStr = dataList.get(0); // 提取表头(第一行是表头)
			Row headerRow = sh.createRow(0);
			for (int j = 0; j < headStr.length; j++) {
				Cell cell = headerRow.createCell(j);
				cell.setCellValue(headStr[j]);
			}

			// 添加第一页的数据行
			for (int i = 1; i <= sheetMaxRows && i < dataListSize; i++) {
				String[] str = dataList.get(i);
				Row row_value = sh.createRow(i);
				for (int j = 0; j < str.length; j++) {
					Cell cel_value = row_value.createCell(j);
					cel_value.setCellValue(str[j]);
				}
			}

			// 处理剩余的数据
			for (int i = sheetMaxRows + 1; i < dataListSize; i += sheetMaxRows) {
				sh = wb.createSheet(sheetName + sheetIndex);
				sh.setDefaultColumnWidth(20);

				// 添加表头
				Row headerRowNewSheet = sh.createRow(0);
				for (int j = 0; j < headStr.length; j++) {
					Cell cell = headerRowNewSheet.createCell(j);
					cell.setCellValue(headStr[j]);
				}

				// 添加数据行
				for (int k = 0; k < sheetMaxRows && i + k < dataListSize; k++) {
					String[] str = dataList.get(i + k);
					Row row_value = sh.createRow(k + 1); // 从第2行开始
					for (int j = 0; j < str.length; j++) {
						Cell cel_value = row_value.createCell(j);
						cel_value.setCellValue(str[j]);
					}
				}

				sheetIndex++;
			}

			File fileDir = new File(savePath.substring(0, savePath.replace("\\", "/").lastIndexOf("/")));
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			// 写入Excel文件
			wb.write(fileOut);
			flag = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("文件[" + savePath + "]导出完毕,共耗时" + (endTime-startTime) + "毫秒");

		return flag;
	}
	
	/**
	 * 描述:导出并下载Excel文件,单Sheet页(通过web页面直接请求调用此方法即可)
	 * 	通过数组输出流下载不产生临时文件
	 * 
	 * @param response 
	 * @param fileName 文件名称
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static void downloadBigDataExcel(HttpServletResponse response, String fileName, List<String[]> dataList) {
		downloadBigDataExcel(response, fileName, null, dataList);
	}
	
	/**
	 * 描述:导出并下载Excel文件,单Sheet页(通过web页面直接请求调用此方法即可)
	 * 	通过数组输出流下载不产生临时文件
	 * 
	 * @param response 
	 * @param fileName 文件名称
	 * @param sheetName Sheet页名称
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static void downloadBigDataExcel(HttpServletResponse response, String fileName, String sheetName, List<String[]> dataList) {
		long startTime = System.currentTimeMillis();
		byte[] excelData = null;
		
		sheetName = (null == sheetName || "".equals(sheetName)) ? "Sheet" : sheetName;
		
		try (Workbook wb = new SXSSFWorkbook(1000);
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			Sheet sh = wb.createSheet(sheetName); // 建立新的sheet对象
			// 设置Excel表格列默认宽度
			sh.setDefaultColumnWidth(20);
			// 设置Excel表格列宽:第一个参数为列的索引,第二个参数为列宽,单位为1/256个字符(即宽度这样设定:宽度*256)。
			sh.setColumnWidth(1, 40 * 256);
			sh.setColumnWidth(4, 40 * 256);
			sh.setColumnWidth(5, 40 * 256);
			// 定义设定单元格样式
			CellStyle cellStyle = wb.createCellStyle();
			// 定义为文本
			cellStyle.setWrapText(true);
			// 定义为垂直居中
			cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			
			// 写入数据
			for (int i = 0, dataListSize = dataList.size(); i < dataListSize; i++) {
				String[] str = dataList.get(i);
				Row row_value = sh.createRow(i);
				for (int j = 0, strLength = str.length; j < strLength; j++) {
					Cell cel_value = row_value.createCell(j);
					cel_value.setCellValue(str[j]);
					// 设置单元格样式
					cel_value.setCellStyle(cellStyle);
				}
			}
			
			wb.write(outputStream);
			// 获取生成的 Excel 字节数组
			excelData = outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (excelData != null) {
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			try (OutputStream output = response.getOutputStream()) {
				output.write(excelData);
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("文件[" + fileName + "]导出完毕,共耗时" + (endTime-startTime) + "毫秒");
	}
	
	/**
	 * 描述:导出并下载Excel文件,多Sheet页(通过web页面直接请求调用此方法即可)
	 * 	通过数组输出流下载不产生临时文件
	 * 
	 * @param response 
	 * @param fileName 文件名称
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static void downloadBigDataExcelExt(HttpServletResponse response, String fileName, List<String[]> dataList) {
		downloadBigDataExcelExt(response, fileName, null, dataList);
	}
	
	/**
	 * 描述:导出并下载Excel文件,多Sheet页(通过web页面直接请求调用此方法即可)
	 * 	通过数组输出流下载不产生临时文件
	 * 
	 * @param response 
	 * @param fileName 文件名称
	 * @param sheetName Sheet页名称
	 * @param dataList 要导出的数据集合
	 * 
	 * @return
	 */
	public static void downloadBigDataExcelExt(HttpServletResponse response, String fileName, String sheetName, List<String[]> dataList) {
		long startTime = System.currentTimeMillis();
		byte[] excelData = null;
		int sheetMaxRows = 20; // 每个Sheet的最大行数
		int sheetIndex = 1;
		int dataListSize = dataList.size();
		
		sheetName = (null == sheetName || "".equals(sheetName)) ? "Sheet" : sheetName;
		
		try (Workbook wb = new SXSSFWorkbook(1000);
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			Sheet sh = wb.createSheet(sheetName + sheetIndex++); // 建立新的sheet对象
			// 设置Excel表格列默认宽度
			sh.setDefaultColumnWidth(20);
			// 设置Excel表格列宽:第一个参数为列的索引,第二个参数为列宽,单位为1/256个字符(即宽度这样设定:宽度*256)。
			sh.setColumnWidth(1, 40 * 256);
			sh.setColumnWidth(4, 40 * 256);
			sh.setColumnWidth(5, 40 * 256);
			// 定义设定单元格样式
			CellStyle cellStyle = wb.createCellStyle();
			// 定义为文本
			cellStyle.setWrapText(true);
			// 定义为垂直居中
			cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			
			// 添加第一页的表头
			String[] headStr = dataList.get(0); // 提取表头(第一行是表头)
			Row headerRow = sh.createRow(0);
			for (int j = 0; j < headStr.length; j++) {
				Cell cell = headerRow.createCell(j);
				cell.setCellValue(headStr[j]);
			}
			
			// 添加第一页的数据行
			for (int i = 1; i <= sheetMaxRows && i < dataListSize; i++) {
				String[] str = dataList.get(i);
				Row row_value = sh.createRow(i);
				for (int j = 0; j < str.length; j++) {
					Cell cel_value = row_value.createCell(j);
					cel_value.setCellValue(str[j]);
				}
			}

			// 处理剩余的数据
			for (int i = sheetMaxRows + 1; i < dataListSize; i += sheetMaxRows) {
				sh = wb.createSheet(sheetName + sheetIndex);
				sh.setDefaultColumnWidth(20);

				// 添加表头
				Row headerRowNewSheet = sh.createRow(0);
				for (int j = 0; j < headStr.length; j++) {
					Cell cell = headerRowNewSheet.createCell(j);
					cell.setCellValue(headStr[j]);
				}

				// 添加数据行
				for (int k = 0; k < sheetMaxRows && i + k < dataListSize; k++) {
					String[] str = dataList.get(i + k);
					Row row_value = sh.createRow(k + 1); // 从第2行开始
					for (int j = 0; j < str.length; j++) {
						Cell cel_value = row_value.createCell(j);
						cel_value.setCellValue(str[j]);
					}
				}

				sheetIndex++;
			}
			
			wb.write(outputStream);
			// 获取生成的 Excel 字节数组
			excelData = outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (excelData != null) {
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			try (OutputStream output = response.getOutputStream()) {
				output.write(excelData);
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("文件[" + fileName + "]导出完毕,共耗时" + (endTime-startTime) + "毫秒");
	}
	
	// 生成测试数据
	public static List<String[]> createData (){
		List<String[]> dataList = new ArrayList<String[]>();
		String[] headStr = new String[8];
		headStr[0] = "标题1";
		headStr[1] = "标题2";
		headStr[2] = "标题3";
		headStr[3] = "标题4";
		headStr[4] = "标题5";
		headStr[5] = "标题6";
		headStr[6] = "标题7";
		headStr[7] = "标题8";
		dataList.add(headStr);
		
		for (int i = 1; i <= 77; i++) {
			String[] str = new String[8];
			str[0] = "数据" + i;
			str[1] = "sdfsdfdsfdsfffffffffffff2";
			str[2] = "320480098308409823093";
			str[3] = "sdjlfkjl立刻就饿了接口连接的4";
			str[4] = "6465213的方式多为sdfsdf5";
			str[5] = "水电费水电费水电费水电费收到6";
			str[6] = "而和任何任务而气温气温7";
			str[7] = "ssdljfls了解恶劣螺纹孔8";
			dataList.add(str);
		}
		
		return dataList;
	}
	
	public static void main(String[] args) {
		List<String[]> dataList = createData();
		exportBigDataExcel("e:\\111.xlsx",dataList);
		exportBigDataExcel("e:\\222.xlsx", null,dataList);
		exportBigDataExcelExt("e:\\333.xlsx", dataList);
		exportBigDataExcelExt("e:\\444.xlsx", "数据",dataList);
	}
}
