package com.qfx.modules.common.util;

/**
 * <h5>描述:获取系统各类信息</h5>
 *  
 */
public class ToolOSInfo {

	public static void main(String[] args) {
		System.out.println(getOSName());
	}
	
	/**
	 * <h5>功能:获取系统名称信息</h5>
	 * 
	 * @return 
	 */
	public static String getOSName() {
		return System.getProperty("os.name").toLowerCase();
	}
	
	/**
	 * <h5>功能:验证是否Linux系统</h5>
	 * 
	 * @return 
	 */
	public static boolean isLinux(){
        return getOSName().indexOf("linux")>=0;
    }
	
	/**
	 * <h5>功能:验证是否Windows系统</h5>
	 * 
	 * @return 
	 */
	public static boolean isWindows(){
        return getOSName().indexOf("windows")>=0;
    }
}
