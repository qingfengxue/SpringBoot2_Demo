package com.qfx.modules.common.util;

import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

public class ToolFiles {
    
    /**
     * <h5>功能:清理指定目录中早于指定时间阈值的文件</h5>
     * 
     * @param directoryPath   目录路径
     * @param minutesThreshold 时间阈值(以分钟为单位),当前时间X分钟之前的时间
     */
    public static void cleanFiles(String directoryPath, int minutesThreshold) {
        File directory = new File(directoryPath); // 创建目录对象
        File[] files = directory.listFiles(); // 获取目录中的文件列表

        if (files != null) {
        	// 计算当前时间-minutesThreshold分钟之后的时间
            LocalDateTime thresholdTime = LocalDateTime.now().minus(minutesThreshold, ChronoUnit.MINUTES);

            for (File file : files) {
            	// 获取文件的最后修改时间(以毫秒为单位)
            	long lastModiTime = file.lastModified();
            	// 转换为LocalDateTime对象
                LocalDateTime lastModified = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastModiTime), ZoneId.systemDefault()); 

                // 判断文件的最后修改时间是否早于指定时间
                if (lastModified.isBefore(thresholdTime)) {
                	// 删除文件
                    boolean deleted = file.delete();
                    if (deleted) {
                        System.out.println("已删除文件：" + file.getName()); // 打印已删除的文件名
                    } else {
                        System.out.println("无法删除文件：" + file.getName()); // 打印无法删除的文件名
                    }
                }
            }
        }
        System.out.println(LocalDateTime.now().toString().replace("T", " ") + " 目录[" + directoryPath + "]文件清理完毕!");
    }
    
    public static void main(String[] args) {
//    	cleanFiles("E:\\tempFile\\download", 30);
    }
}
