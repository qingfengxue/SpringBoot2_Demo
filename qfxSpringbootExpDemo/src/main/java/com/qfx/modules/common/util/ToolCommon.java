package com.qfx.modules.common.util;

import java.io.File;



/**
 * @功能描述：公共方法类
 *
 * @作者：zhangpj		@创建时间：2015年3月18日
 */
public class ToolCommon {

	/**
	 * @功能描述：返回项目根路径(全路径,无线程安全问题)
	 *
	 * @return
	 *
	 * @作者：zhangpj		@创建时间：2016年11月25日
	 */
	public static String getRootPath(){
		String projectPath = getClassesPath();
		for (int i = 0; i < 3; i++) {
			projectPath = projectPath.substring(0, projectPath.lastIndexOf("/"));
		}
		projectPath += File.separator;

		return projectPath;
	}
	
	/**
	 * @功能描述：返回项目classes目录(全路径,无线程安全问题)
	 *
	 * @return
	 *
	 * @作者：zhangpj		@创建时间：2016年11月25日
	 */
	public static String getClassesPath(){
		return Thread.currentThread().getContextClassLoader().getResource("").getPath().substring(1);
	}
}
