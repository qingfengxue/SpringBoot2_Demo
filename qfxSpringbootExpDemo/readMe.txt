1.springboot的版本是2.2.1.RELEASE,2023年09月08日升级到2.4.6

2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效

3.本项目是为了测试在使用springboot过程中出现异常时,使用本示例进行调试,找出问题所在并解决问题

4.2021-02-19 新增虚拟文件目录设置,application-*.properties
	# ----------------资源文件配置---------------
	## 自定义外部资源文件目录(如果这里报警告信息,需要在"/resources/META-INF/"目录下创建additional-spring-configuration-metadata.json文件,将自定义属性添加进去即可)
	temp.file.path=E:/tempFile/download/
	
	## 修改默认的静态寻址资源目录(多个使用逗号分隔),需要将默认的也加上否则static、public等这些路径将不能被当作静态资源路径,外部目录设置file即可
	spring.web.resources.static-locations=classpath:/META-INF/resources/,classpath:/resources/,classpath:/static/,classpath:/public/,file:${temp.file.path}
	## 也可以直接自定义外部资源文件目录
	spring.web.resources.static-locations=classpath:/META-INF/resources/,classpath:/resources/,classpath:/static/,classpath:/public/,file:E:/tempFile/download/
5.com.qfx.modules.test.controller的expExt()与expExtTwo()方法效果是一样的		

6.启动:
	1.直接运行RunApp.java中的main方法即可

7.测试地址
	默认首页：http://localhost:80/qfxSpringbootExpDemo/test/main/view
