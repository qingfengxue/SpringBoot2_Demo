1.springboot的版本是2.2.1.RELEASE
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增shiro支持
	1.pom.xml
		<!--9.开启quartz依赖-->
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-quartz</artifactId>
		</dependency>
		
	2.application.properties,可以参考http://wiki.bsdn.org/pages/viewpage.action?pageId=2327098(quartz配置属性详解)
		# quartz任务配置
		## 使用数据库存储定时任务信息(memory表示使用内存存储)
		spring.quartz.job-store-type=jdbc
		## 默认或是自己改名字都行
		spring.quartz.properties.org.quartz.scheduler.instanceName=clusteredScheduler
		## 如果使用集群，instanceId必须唯一，设置成AUTO
		spring.quartz.properties.org.quartz.scheduler.instanceId=AUTO
		## 存储方式使用JobStoreTX，(默认,以spring中配置的数据源为任务仓库的实现类，支持集群 )
		spring.quartz.properties.org.quartz.jobStore.class=org.quartz.impl.jdbcjobstore.JobStoreTX
		## 通过JDBC访问数据库的代理类(默认)
		spring.quartz.properties.org.quartz.jobStore.driverDelegateClass=org.quartz.impl.jdbcjobstore.StdJDBCDelegate
		## 数据库中quartz表的表名前缀
		spring.quartz.properties.org.quartz.jobStore.tablePrefix=QRTZ_
		## 是否使用集群(如果项目只部署到 一台服务器，就不用了，当然设置为true也无所谓)
		spring.quartz.properties.org.quartz.jobStore.isClustered=true
		## Quartz集群的状态更新时间间隔(默认:15000)
		spring.quartz.properties.org.quartz.jobStore.clusterCheckinInterval=2000
		## 使用自己的配置文件
		spring.quartz.properties.org.quartz.jobStore.useProperties=false
		## 要使用的ThreadPool实现的线程池
		spring.quartz.properties.org.quartz.threadPool.class=org.quartz.simpl.SimpleThreadPool
		## 执行任务代码的线程池的大小(默认20)
		spring.quartz.properties.org.quartz.threadPool.threadCount=10
		## 执行任务代码的线程的优先级(默认:5)
		spring.quartz.properties.org.quartz.threadPool.threadPriority=5
		## 加载任务代码的ClassLoader是否从外部继承(默认:true)
		spring.quartz.properties.org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread=true
4.测试任务类
		com.qfx.system.task.TaskTest01 继承实现
		com.qfx.system.task.TaskTest02 继承实现
		com.qfx.system.task.TaskTest03 继承实现
		com.qfx.system.task.TaskTest04 接口实现
		
		QuartzJobBean也是实现的Job接口
  
5.启动:
	1.先创建数据库,执行resources/document/sql/tables_mysql_innodb.sql文件
	2.直接运行RunApp.java中的main方法即可
	3.复制target目录中的qfxSpringbootShiroDemo.war到tomcat中,启动tomcat即可

6.测试地址
	tomcat启动：http://localhost/qfxSpringbootQuartzDemo
	springboot启动：http://localhost
	