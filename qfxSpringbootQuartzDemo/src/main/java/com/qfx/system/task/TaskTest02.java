package com.qfx.system.task;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * <h5>描述:QuartzJobBean也是实现的Job接口</h5>
 *  
 */
public class TaskTest02 extends QuartzJobBean {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info("[{}]执行了一条新的任务", this.getClass().getName());
	}
}
