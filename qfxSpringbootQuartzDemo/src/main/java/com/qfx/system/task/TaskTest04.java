package com.qfx.system.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <h5>描述:推荐使用Job接口实现定时任务</h5>
 *  
 */
public class TaskTest04 implements Job {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("[{}]执行了一条新的任务", this.getClass().getName());
        // 1.获取Trigger
        Trigger trigger = context.getTrigger();
        //2.通过trigger获取job标识
        JobKey jobKey = trigger.getJobKey();
        System.out.println("key:"+"name:"+jobKey.getName()+"\tgroup:"+jobKey.getGroup());getClass();
	}
}
