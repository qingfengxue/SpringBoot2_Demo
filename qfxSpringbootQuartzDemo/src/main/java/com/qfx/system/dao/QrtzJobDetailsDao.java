package com.qfx.system.dao;

import com.qfx.common.dao.BaseDao;
import com.qfx.system.vo.QuartzEntity;

public interface QrtzJobDetailsDao extends BaseDao<QuartzEntity> {
	
}