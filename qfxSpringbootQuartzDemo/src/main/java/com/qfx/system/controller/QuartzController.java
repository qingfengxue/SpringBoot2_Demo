package com.qfx.system.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.qfx.common.bean.MessageBean;
import com.qfx.system.service.QuartzService;
import com.qfx.system.vo.QuartzEntity;

@RestController
@RequestMapping("quartz")
public class QuartzController {
	
	@Autowired
	QuartzService quartzService;
	
	/**
	 * <h5>功能:获取定时任务显示列表</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @return 
	 */
	@RequestMapping("list")
	public String list(){
		Map<String, Object> dataMap = quartzService.list();
		return JSONObject.toJSONString(dataMap);
	}
	
	/**
	 * <h5>功能:验证任务名称是否存在</h5>
	 * 
	 * @author qfx	@date 2019年8月16日
	 * @return 
	 */
	@RequestMapping("validate/jobName")
	public String validateJobName(){
		return JSONObject.toJSONString(quartzService.validateJobName());
	}
	
	/**
	 * <h5>功能:编辑定时任务</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @param quartz
	 * @return 
	 */
	@RequestMapping("edit")
	public String edit(QuartzEntity quartz){
		MessageBean messageBean = quartzService.edit(quartz);
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:删除定时任务</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @param quartz
	 * @return 
	 */
	@RequestMapping("del")
	public String del(QuartzEntity quartz){
		MessageBean messageBean = quartzService.del(quartz);
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:暂停定时任务</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @param quartz
	 * @return 
	 */
	@RequestMapping("pause")
	public String pause(QuartzEntity quartz){
		MessageBean messageBean = quartzService.pause(quartz);
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:重新开启定时任务</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @param quartz
	 * @return 
	 */
	@RequestMapping("resume")
	public String resume(QuartzEntity quartz){
		MessageBean messageBean = quartzService.resume(quartz);
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:暂停所有定时任务</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @param quartz
	 * @return 
	 */
	@RequestMapping("pauseAll")
	public String pauseAll(QuartzEntity quartz){
		MessageBean messageBean = quartzService.pauseAll();
		return JSONObject.toJSONString(messageBean);
	}
	
	/**
	 * <h5>功能:重启开启所有定时任务</h5>
	 * 
	 * @author qfx	@date 2019年8月15日
	 * @param quartz
	 * @return 
	 */
	@RequestMapping("resumeAll")
	public String resumeAll(QuartzEntity quartz){
		MessageBean messageBean = quartzService.resumeAll();
		return JSONObject.toJSONString(messageBean);
	}
}
