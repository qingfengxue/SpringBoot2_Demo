package com.qfx.common.dao;

import java.util.List;
import java.util.Map;

public interface BaseDao<T> {
	
	List<T> selectAll(Map<String, Object> paramMap);
}
