package com.qfx.common.service;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.qfx.common.utils.Page;

public class BaseService {
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public static String CURRENT_PAGE_NAME="page";
	public static String PAGE_SIZE_NAME="limit";
	
	@Autowired
	public HttpServletRequest request;
	@Autowired
	public HttpServletResponse response;

	/**
	 * @功能描述：	获取从request中传递过来的参数信息
	 *
	 * @作者：qfx		@创建时间：2017年8月15日
	 * @param request
	 */
	public Map<String, Object> getMaps(){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> enume = request.getParameterNames();
		while (enume.hasMoreElements()) {
			String key = (String) enume.nextElement();
			String[] values = request.getParameterValues(key);
			paramMap.put(key, values.length == 1 ? request.getParameter(key).trim() : values);
		}

		return paramMap;
	}
	
	/**
	 * @功能描述：	获取分页信息
	 *
	 * @param map
	 * @return 
	 *
	 * @作者：qfx		@创建时间：2018年3月23日
	 */
	public Page getPage(Map<String, Object> map){
		int pageNum=1;
		int pageSize=10;
		if(map.containsKey(CURRENT_PAGE_NAME) && !"".equals(map.get(CURRENT_PAGE_NAME))){
			pageNum=Integer.parseInt((String)map.get(CURRENT_PAGE_NAME));
		}
		if(map.containsKey(PAGE_SIZE_NAME) && !"".equals(map.get(PAGE_SIZE_NAME))){
			pageSize=Integer.parseInt((String)map.get(PAGE_SIZE_NAME));
		}
		Page page=new Page(pageNum,pageSize);
		return page;
	}
}
