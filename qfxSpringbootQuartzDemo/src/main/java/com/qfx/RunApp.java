package com.qfx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * <h5>描述:添加外部tomcat支持,需要继承SpringBootServletInitializer,并重写configure方法</h5>
 * 
 * @author zhangpj	2018年9月19日 
 */

@SpringBootApplication
@MapperScan("com.qfx.*.dao")	// 指定Mapper需要扫描的包,这样就不用每个Mapper上都添加@Mapper注解了
public class RunApp extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(RunApp.class);
	}
	
	public static void main(String[] args) {
		// 整个程序入口,启动springboot项目,创建内置tomcat服务器,使用tomct加载springmvc注解启动类
		SpringApplication.run(RunApp.class, args);
	}
}
