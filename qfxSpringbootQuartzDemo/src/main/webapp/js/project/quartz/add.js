layui.use(['form'], function() {
	var $ = layui.$, 
	form = layui.form;

	//监听提交
	form.on('submit(add-submit)', function(data) {
		var formData = data.field; //获取提交的字段
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引  

		$.ajax({
			type : "post", // 提交方式  
			url : "../../quartz/edit",// 路径  
			data : formData,// 数据，这里使用的是Json格式进行传输  
			dataType: "JSON",
			success : function(result) {// 返回数据根据结果进行相应的处理 
				var alertIndex;
				if (result.result) {
					alertIndex = layer.alert(result.message, {skin: 'layui-layer-molv', closeBtn: 0,icon: 6}, function(){
						layer.close(alertIndex);
						parent.layui.table.reload('demo'); //重载表格
						parent.layer.close(index); //再执行关闭
	    	    	});
				} else {
					alertIndex = layer.alert(result.message, {skin: 'layui-layer-molv', closeBtn: 0,icon: 5}, function(){
						layer.close(alertIndex);
	    	    	});
				}
			},
			error : function(error) {
				alertIndex = layer.alert(error, {skin: 'layui-layer-molv', closeBtn: 0,icon: 5});
            }
		}); 
	});
})

function child(data) {
	$("input[name='oldJobName']").val(data.oldJobName);
	$("input[name='oldJobGroup']").val(data.oldJobGroup);
	$("input[name='jobName']").val(data.jobName);
	$("input[name='jobClassName']").val(data.jobClassName);
	$("input[name='cronExpression']").val(data.cronExpression);
	$("select[name=jobGroup]").val(data.jobGroup);
	$("textarea[name='description']").val(data.description);
}
