layui.use('table', function(){
  var table = layui.table;
  
  //第一个实例
  table.render({
	  id: 'demo',
    elem: '#demo',
//height: 'full-100',	// 高度将始终铺满,full-差值
     url: '../../quartz/list', //数据接口
    cellMinWidth : 95, //全局定义所有常规单元格的最小宽度(默认:60),其优先级低于表头参数中的 minWidth
    page: true, //开启分页
    even: true, //开启隔行背景，不设置该参数即可
    // 开启表格头部工具栏区域
    toolbar: '#toolbarDemo',
    // 配置头部工具栏右侧的图标(layui 2.4.1 新增,默认显示,要控制这里必须要开启toolbar参数),
    // filter: 显示筛选图标;exports: 显示导出图标;print: 显示打印图标,可根据值的顺序显示排版图标,如： ['filter', 'print', 'exports']
    defaultToolbar:['filter'],
    cols: [[ //表头
      {field: 'jobName', title: '任务名称', fixed: 'left'},
      {field: 'jobClassName', title: '执行类'},
      {field: 'cronExpression', title: '执行时间', minWidth:150},
      {field: 'jobGroup', title: '任务分组', width:120, templet: function(d){
    	  var btHtml = "";
		  if (d.jobGroup == 'system') {
			  btHtml = '系统任务';
		  } else {
			  btHtml = '平台任务';
		  }
		  return btHtml;
      }},
      {field: 'description', title: '任务描述'},
      {field: 'triggerState', title: '任务状态', width:120,
    	  templet: function(d){
    		  var btHtml = "";
    		  if (d.triggerState == 'PAUSED') {
    			  btHtml = '<lable style="color:red">暂停</lable>';
    		  } else {
    			  btHtml = '<lable style="color:green">运行中</lable>';
    		  }
    		  return btHtml;
    	  }
      },
      {fixed: 'right', title: '操作', toolbar: '#barDemo'}
    ]],
    done: function(res, curr, count){
//    	$('th').css({'background-color': 'MediumSeaGreen', 'color': '#fff','font-weight':'bold'})
    }
  });
  
  //头工具栏事件
  table.on('toolbar(test)', function(obj){
    var checkStatus = table.checkStatus(obj.config.id);
    switch(obj.event){
      case 'add':
    	add();
    	break;
      case 'pauseAll':
    	layer.confirm('确定要停止当前任务吗?',{skin: 'layui-layer-molv', closeBtn: 0,icon: 5},  function(index){
    		pauseAll();
    	    layer.close(index);
    	});
        break;
      case 'resumeAll':
    	layer.confirm('确定要开始当前任务吗?',{skin: 'layui-layer-molv', closeBtn: 0,icon: 6},  function(index){
    		resumeAll();
    		layer.close(index);
  	    });
        break;
    };
  });
  
  //监听行工具事件
  table.on('tool(test)', function(obj){
    var data = obj.data;
    if(obj.event === 'pause') {
	  layer.confirm('确定要停止当前任务吗?',{skin: 'layui-layer-molv', closeBtn: 0,icon: 5},  function(index){
		pause(data);
	    layer.close(index);
	  });
	} else if(obj.event === 'resume') {
	  layer.confirm('确定要开始当前任务吗?',{skin: 'layui-layer-molv', closeBtn: 0,icon: 6},  function(index){
		resume(data);
		layer.close(index);
	  });
	} else if(obj.event === 'edit') {
	  layer.confirm('确定要编辑当前任务吗?',{skin: 'layui-layer-molv', closeBtn: 0,icon: 0}, function(index){
    	edit(data);
        layer.close(index);
      });
	} else if(obj.event === 'del') {
	  layer.confirm('确定要删除当前任务吗?',{skin: 'layui-layer-molv', closeBtn: 0,icon: 0}, function(index){
    	del(data);
        layer.close(index);
      });
	}
  });
});

// 添加定时任务
function add(){
	layer.open({
		skin: 'layui-layer-molv',
        type: 2,
        title: '添加定时任务',
        content: 'add.html',
        maxmin: true,
        area: ['480px', '450px'],
        btn: ['确定', '取消'],
        yes: function(index, layero){	//确定按钮回调方法
        	//点击确认触发 iframe 内容中的按钮提交
            var submit = layero.find('iframe').contents().find("#add-submit");
            submit.click();
        }
	});
}

//编辑一个定时任务
function edit(data){
	layer.open({
		skin: 'layui-layer-molv',
        type: 2,
        title: '编辑定时任务',
        content: 'add.html',
        maxmin: true,
        area: ['480px', '450px'],
        btn: ['确定', '取消'],
        yes: function(index, layero){
        	//点击确认触发 iframe 内容中的按钮提交
            var submit = layero.find('iframe').contents().find("#add-submit");
            submit.click();
        },success: function(layero, index){	//层弹出后的成功回调方法(当前层DOM,当前层索引)
	        // 获取子页面的iframe
            var iframe = window['layui-layer-iframe' + index];
            // 向子页面的全局函数child传参
            iframe.child(data);
            // 重新渲染select,checkbox同理
            iframe.layui.form.render('select');
	    }
	});
}

//暂停一个定时任务
function pause(data){
	var url = "../../quartz/pause";
	customAjax(url, data);
}

//重新开启一个定时任务
function resume(data){
	var url = "../../quartz/resume";
	customAjax(url, data);
}

//删除一个定时任务
function del(data){
	var url = "../../quartz/del";
	customAjax(url, data);
}

//暂停所有定时任务
function pauseAll(data){
	var url = "../../quartz/pauseAll";
	customAjax(url, data);
}

//重启启动所有定时任务
function resumeAll(data){
	var url = "../../quartz/resumeAll";
	customAjax(url, data);
}

// ajax请求公共方法
function customAjax(url, data){
	$.ajax({
		type : "post", // 提交方式
		url : url,// 路径
		data : data,// 数据，这里使用的是Json格式进行传输
		dataType: "JSON",
		success : function(result) {// 返回数据根据结果进行相应的处理
			var alertIndex;
			if (result.result) {
				// 点击确定后刷新表格数据并关闭提示框
				alertIndex = layer.alert(result.message, {skin: 'layui-layer-molv', closeBtn: 0,icon: 1}, function(){
		    		layui.table.reload('demo'); //重载表格
		    		layer.close(alertIndex);
    	    	});
			} else {
				layer.alert(result.message, {skin: 'layui-layer-molv', closeBtn: 0,icon: 2});
			}
		},
		error : function(error) {
			vlayer.alert(error.status + ':' + error.statusText, {skin: 'layui-layer-molv', closeBtn: 0,icon: 2});
        }
	});
}
