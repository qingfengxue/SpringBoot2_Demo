package com.qfx.modules.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api(tags = "测试类相关信息")
@RestController
@RequestMapping("test")
public class TestCtrl {
	
	@ApiOperation("最简单的问候")
	@GetMapping("hello")
	public String hello () {
		return "你好,世界";
	}
	
	@ApiOperation(value = "接口说明:稍微高级一点的问候", notes = "备注说明:升级版")
	@ApiImplicitParam(name = "name", value = "用户名称")
	@GetMapping("hello/{name}")
	public String hello (@PathVariable String name) {
		return name + "你好";
	}
	
	@ApiOperation(value = "接口说明:高级问候", notes = "备注说明:最终版")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "age", value = "年龄"),
		@ApiImplicitParam(name = "msg", value = "信息", required = false)
	})
	
	@GetMapping("helloExt/{name}/{age}")
	public String hello (@PathVariable String name, @PathVariable int age, String msg) {
		return name + "你好, 你今年" + age + "吗？,你带来的消息是:\"" + msg + "\"吗?";
	}
}
