1.springboot的版本是2.6.4
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增Swagger3支持,进行以下操作
	1.pom.xml
		<!-- 3. 引入Swagger3依赖 -->
		<dependency>
	        <groupId>io.springfox</groupId>
	        <artifactId>springfox-boot-starter</artifactId>
	        <version>3.0.0</version>
	    </dependency>
	    <!-- 4. 引入knife4j依赖,用来增强Swagger3 -->
		<dependency>
		    <groupId>com.github.xiaoymin</groupId>
		    <artifactId>knife4j-spring-boot-starter</artifactId>
		    <!--在引用时请在maven中央仓库搜索3.X最新版本号-->
		    <version>3.0.3</version>
		</dependency>
		
	2.Swagger3配置类
		com.qfx.modules.common.config.SwaggerConfig
		
	3.Springboot配置文件application-*.properties 新增配置以下信息(或者在启动类中添加 @EnableWebMvc 注解),Springboot2.6以下版本可不配置
		# Springboot2.6及以上版本整合Swagger3需要此配置,否则启动会报异常:
		# Failed to start bean 'documentationPluginsBootstrapper'; nested exception is java.lang.NullPointerException
		spring.mvc.pathmatch.matching-strategy=ant-path-matcher
		
启动:
	1.直接运行RunApp.java中的main方法即可
	2.复制target目录中的qfxSpringbootSwaggerDemo.war到tomcat中,启动tomcat即可

测试地址
	默认Swagger文档首页：
		http://localhost/qfxSpringbootSwaggerDemo/swagger-ui/index.html
	默认knife4j文档首页：
		http://localhost/qfxSpringbootSwaggerDemo/doc.html


