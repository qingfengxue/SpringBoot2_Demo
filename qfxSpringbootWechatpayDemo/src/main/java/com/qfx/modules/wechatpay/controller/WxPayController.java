package com.qfx.modules.wechatpay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.WxPayOrderCloseResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;

@RestController
@RequestMapping("wx/pay")
public class WxPayController {
	
	@Autowired
    private WxPayService wxPayService;
	
	@RequestMapping("createOrder")
	public String closeOrder(@RequestBody WxPayUnifiedOrderRequest request) {
		try {
			return JSONObject.toJSONString(wxPayService.createOrder(request));
		} catch (WxPayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("closeOrder")
	public String closeOrder(String outTradeNo) {
		WxPayOrderCloseResult closeOrder = null;
		try {
			closeOrder = wxPayService.closeOrder(outTradeNo);
		} catch (WxPayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return JSONObject.toJSONString(closeOrder);
	}
}
