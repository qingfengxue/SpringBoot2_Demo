# SpringBoot2_Demo

#### 介绍
这是一个springboot2整合其他框架的应用示例集合,可以帮助你快速建立起一个可简单运行的应用示例

#### 软件架构
springboot的版本是2.x.x,具体版本各示例可能会有细微差别

#### 安装教程
1. 直接下载到本地
2. 使用Eclipse或者MyEclipse加载项目
3. 修改jdk等基本配置

#### 示例明细
1. qfxSpringbootAnnotationDemo            自定义注解
2. qfxSpringbootAspectLogDemo             使用AOP统一处理Web请求日志
3. qfxSpringbootDynamicDatasourceDemo     多数据源
4. qfxSpringbootMyBatisDemo				  整合mybatis,并支持多种数据库连接(不是多数据源),即Sql支持多种数据库语法
5. qfxSpringbootMybatisPlusDemo           整合mybatis-plus
6. qfxSpringbootOpenfeignParentDemo       父子项目(聚合工程),使用openFeign继承特性的示例
7. qfxSpringbootParentProjectDemo         聚合项目,分布式项目模块搭建的示例
8. qfxSpringbootParentSonDemo             Maven(Springboot)聚合项目的示例
9. qfxSpringbootQuartzDemo                整合quartz定时任务
11. qfxSpringbootRedisDemo                整合Redis
12. qfxSpringbootShiroDemo                整合shiro
13. qfxSpringbootShiroExtDemo             整合shiro进阶版
14. qfxSpringbootSwaggerDemo              整合Swagger3
15. qfxSpringbootTaskDemo                 多线程执行定时任务
16. qfxSpringbootTaskExtDemo              动态操作定时任务
17. qfxSpringbootTest                     基础Springboot项目
18. qfxSpringbootThyemleafDemo            整合Thyemleaf模板引擎
19. qfxSpringbootValidatedDemo            整合validator进行参数校验
20. qfxSpringbootWebsocketDemo            整合websocket
21 qfxSpringbootWechatpayDemo             整合微信支付
22. qfxSpringbootMyBatisCodeGenDemo       整合MyBatis-plus代码生成器