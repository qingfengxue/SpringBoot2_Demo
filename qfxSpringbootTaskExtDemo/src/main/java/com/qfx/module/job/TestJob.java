package com.qfx.module.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试用的定时任务类，包含几个模拟任务执行的方法
 */
public class TestJob {

    // 日志对象，用于记录日志信息
    private static final Logger log = LoggerFactory.getLogger(TestJob.class);

    /**
     * 一个定时任务执行方法
     */
    public void exec() {
        log.info("执行了一个定时任务: exec()");
    }

    /**
     * 一个定时任务执行方法
     */
    public void execTwo() {
        log.info("执行了一个定时任务: execTwo()");
    }

    /**
     * 测试方法，模拟一个耗时操作
     * 用于测试定时任务的执行效果
     */
    public void test() {
        performTask("test()");
    }

    /**
     * 另一个测试方法，与test方法类似，用于模拟耗时操作
     * 用于测试定时任务的执行效果
     */
    public void sayHello() {
        performTask("sayHello()");
    }

    /**
     * 私有方法，封装耗时操作的公共逻辑
     * 这个方法模拟了一个耗时的任务，并记录任务的执行情况
     *
     * @param taskName 任务名称，用于区分不同的任务
     */
    private void performTask(String taskName) {
        log.info("执行了一个定时任务: {}", taskName);
        try {
            Thread.sleep(1000 * 3);
        } catch (InterruptedException e) {
            log.error("任务 {} 被中断", taskName, e);
            Thread.currentThread().interrupt(); // 恢复中断状态
        }
    }
}
