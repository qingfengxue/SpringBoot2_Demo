package com.qfx.module.common.tool;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

/**
 * 功能：定义时间单位的枚举，用于统一时间单位的表示和转换
 */
public enum ToolPeriodUnit {
    // 定义时间单位枚举常量
    SECOND("second", ChronoUnit.SECONDS), // 秒
    MINUTE("minute", ChronoUnit.MINUTES), // 分钟
    DAY("day", ChronoUnit.DAYS), // 天
    WEEK("week", ChronoUnit.WEEKS), // 星期
    MONTH("month", ChronoUnit.MONTHS), // 月
    YEAR("year", ChronoUnit.YEARS); // 年

    // 成员变量：key用于存储时间单位的字符串表示，value用于存储对应的时间单位
    private final String key;
    private final TemporalUnit value;

    // 构造方法：初始化时间单位的字符串表示和对应的时间单位
    ToolPeriodUnit(String key, ChronoUnit value) {
        this.key = key;
        this.value = value;
    }

    /**
     * 功能：获取时间单位的key
     *
     * @return String 时间单位的key
     */
    public String getKey() {
        return key;
    }

    /**
     * 功能：根据时间单位的key表示获取对应的时间单位value
     *
     * @param key 时间单位的key
     * @return TemporalUnit 时间单位的value
     */
    public static TemporalUnit getValue(String key) {
        TemporalUnit value = null;
        for (ToolPeriodUnit toolPeriodUnit : values()) {
            if (key.equals(toolPeriodUnit.key)) {
                value = toolPeriodUnit.value;
            }
        }
        return value;
    }
}
