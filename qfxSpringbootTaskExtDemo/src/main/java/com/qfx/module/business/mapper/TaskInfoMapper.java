package com.qfx.module.business.mapper;

import com.qfx.module.business.entity.TaskInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 任务调度信息表 Mapper 接口
 */
public interface TaskInfoMapper extends BaseMapper<TaskInfo> {

}
