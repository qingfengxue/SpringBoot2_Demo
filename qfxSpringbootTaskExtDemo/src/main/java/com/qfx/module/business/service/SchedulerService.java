package com.qfx.module.business.service;

import com.qfx.module.business.entity.TaskInfo;

import java.util.List;

/**
 * 功能：定时任务管理接口
 */
public interface SchedulerService {

    /**
     * 功能：获取所有定时任务
     */
    List<TaskInfo> list();

    /**
     * 添加定时任务。
     *
     * @param taskInfo 任务信息对象，包含任务的各种属性
     */
    String add(TaskInfo taskInfo);

    /**
     * 编辑定时任务。
     *
     * @param taskInfo 任务信息对象，包含任务的各种属性
     */
    String edit(TaskInfo taskInfo);

    /**
     * 暂停定时任务。
     *
     * @param taskId 任务ID
     */
    String pause(String taskId);

    /**
     * 恢复定时任务。
     *
     * @param taskId 任务ID
     */
    String resume(String taskId);

    /**
     * 删除定时任务。
     *
     * @param taskId 任务ID
     */
    String delete(String taskId);
}
