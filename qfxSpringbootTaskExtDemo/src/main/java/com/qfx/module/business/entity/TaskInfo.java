package com.qfx.module.business.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 任务调度信息表
 */
@Getter
@Setter
@TableName("task_info")
public class TaskInfo {

    /**
     * 主键
     */
    @TableId(value = "task_id", type = IdType.NONE)
    private String taskId;

    /**
     * 调度类型 0-cron表达式 1-指定时间执行(仅1次) 2-任务结束固定延迟执行(fixedDelay) 3-任务开始固定速率执行(fixedRate)
     */
    private Integer modeType;

    /**
     * 调度类(全路径)
     */
    private String exeClass;

    /**
     * 调度方法(exe_class中的方法)
     */
    private String exeMethod;

    /**
     * 初始延时时间(与初始延时时间单位组合使用)
     */
    private Long initialDelay;

    /**
     * 初始延时时间单位 seconds-秒 minute-分钟 day-天
     */
    private String initialDelayUnit;

    /**
     * 固定间隔(与任务执行时间单位组合使用)
     */
    private Long fixedDelay;

    /**
     * 固定频率(与任务执行时间单位组合使用)
     */
    private Long fixedRate;

    /**
     * 任务执行时间单位 seconds-秒 minute-分钟 day-天 week-星期 month-月 year-年
     */
    private String timeUnit;

    /**
     * cron表达式
     */
    private String cron;

    /**
     * 任务执行的时间(指定任务在特定时间点执行)
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date processTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 状态(0-停止 1-进行中)
     */
    private Integer state;

    /**
     * 说明
     */
    private String remark;
}
