package com.qfx.module.business.controller;

import com.alibaba.fastjson2.JSONObject;
import com.qfx.module.business.entity.TaskInfo;
import com.qfx.module.business.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能：定时任务控制器，提供HTTP接口操作定时任务
 */
@RestController
@RequestMapping("/scheduler")
public class SchedulerController {

    @Autowired
    private SchedulerService schedulerService;

    /**
     * 获取所有定时任务
     */
    @GetMapping("/list")
    public List<TaskInfo> list() {
        return schedulerService.list(); // 添加任务
    }

    /**
     * 添加定时任务
     *
     * @param taskInfo 任务信息对象，包含任务的各种属性
     * @return 操作结果
     */
    @PostMapping("/add")
    public String add(@RequestBody TaskInfo taskInfo) {
        return schedulerService.add(taskInfo);
    }

    /**
     * 编辑定时任务
     *
     * @param taskInfo 任务信息对象，包含任务的各种属性
     * @return 操作结果
     */
    @PostMapping("/edit")
    public String edit(@RequestBody TaskInfo taskInfo) {
        return schedulerService.edit(taskInfo);
    }

    /**
     * 暂停定时任务
     *
     * @param taskId 任务ID
     * @return 操作结果
     */
    @GetMapping("/pause")
    public String pause(@RequestParam String taskId) {
        return schedulerService.pause(taskId); // 停止任务
    }

    /**
     * 恢复定时任务
     *
     * @param taskId 任务ID
     * @return 操作结果
     */
    @GetMapping("/resume")
    public String resume(@RequestParam String taskId) {
        return schedulerService.resume(taskId); // 恢复任务
    }

    /**
     * 删除定时任务
     *
     * @param taskId 任务ID
     * @return 操作结果
     */
    @GetMapping("/delete")
    public String delete(@RequestParam String taskId) {
        return schedulerService.delete(taskId); // 删除任务
    }
}
