package com.qfx.module.business.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.qfx.module.business.entity.TaskInfo;
import com.qfx.module.business.mapper.TaskInfoMapper;
import com.qfx.module.business.service.SchedulerService;
import com.qfx.module.common.tool.ToolPeriodUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

/**
 * 功能：定时任务管理器实现类，负责添加、暂停、恢复、停止和删除定时任务。
 */
@Slf4j
@Service
public class SchedulerServiceImpl implements SchedulerService {

    private final Map<String, ScheduledFuture<?>> scheduledTasks = new ConcurrentHashMap<>(); // 存储定时任务的Map
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler; // 注入TaskScheduler
    @Autowired
    private TaskInfoMapper taskInfoMapper;

    /**
     * 在Bean初始化完成后执行的任务初始化方法
     * 该方法负责加载所有任务信息，并根据任务的状态创建和注册定时任务
     */
    @PostConstruct
    public void init() {
        // 查询所有任务
        List<TaskInfo> taskInfoList = list();

        // 遍历任务并注册
        for (TaskInfo taskInfo : taskInfoList) {
            // 跳过未启用的任务
            if (null == taskInfo.getState() || taskInfo.getState().equals(0)) {
                continue;
            }
            // 创建一个Runnable任务
            Runnable task = createTask(taskInfo);
            // 根据任务信息和任务逻辑创建任务调度信息
            ScheduledFuture<?> scheduledFuture = createSchedule(taskInfo, task);

            // 将任务ID与调度信息关联，以便后续管理
            scheduledTasks.put(taskInfo.getTaskId(), scheduledFuture);
        }
    }

    @Override
    public List<TaskInfo> list() {
        LambdaQueryWrapper<TaskInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(TaskInfo::getUpdateTime);

        return taskInfoMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public String add(TaskInfo taskInfo) {
        // 编辑任务
        return editTask(taskInfo, false);
    }

    @Override
    @Transactional
    public String edit(TaskInfo taskInfo) {
        if (ObjectUtils.isEmpty(taskInfo.getTaskId())) {
            return "任务不存在";
        }

        ScheduledFuture<?> future = scheduledTasks.get(taskInfo.getTaskId());
        if (future != null) {
            // 取消任务调度但不中断正在执行的任务
            boolean flag = future.cancel(false);
            if (flag) {
                // 移除任务
                scheduledTasks.remove(taskInfo.getTaskId());
            }
        }

        // 编辑任务
        return editTask(taskInfo, true);
    }

    @Override
    @Transactional
    public String pause(String taskId) {
        ScheduledFuture<?> future = scheduledTasks.get(taskId);
        if (future != null) {
            // 取消任务调度但不中断正在执行的任务
            boolean flag = future.cancel(false);
            if (flag) {
                // 移除任务
                scheduledTasks.remove(taskId);

                // 更新数据库中任务状态为停止
                TaskInfo taskInfo = new TaskInfo();
                taskInfo.setTaskId(taskId);
                taskInfo.setState(0);
                taskInfoMapper.updateById(taskInfo);

                log.info("任务暂停成功,taskId:{}", taskId);
            }
        }

        return "任务停止成功";
    }

    @Override
    @Transactional
    public String resume(String taskId) {
        // 检查任务是否已运行
        ScheduledFuture<?> future = scheduledTasks.get(taskId);
        if (future != null && !future.isCancelled()) {
            return "任务已运行";
        }

        // 获取任务信息
        TaskInfo taskInfo = taskInfoMapper.selectById(taskId);
        if (taskInfo == null) {
            return "任务不存在";
        }

        // 检查任务信息的合法性
        String result = checkTaskInfo(taskInfo, true);
        if (StringUtils.hasLength(result)) {
            return result;
        }

        // 创建一个Runnable任务
        Runnable task = createTask(taskInfo);
        if (task == null) {
            return "任务恢复失败";
        }
        // 根据任务信息和任务逻辑创建任务调度信息
        ScheduledFuture<?> scheduledFuture = createSchedule(taskInfo, task);
        if (scheduledFuture == null) {
            return "不支持的调度类型";
        }
        scheduledTasks.put(taskInfo.getTaskId(), scheduledFuture);

        // 更新数据库中任务状态为运行
        taskInfo.setState(1);
        int rowCount = taskInfoMapper.updateById(taskInfo);
        if (rowCount == 0) {
            return "任务状态更新失败";
        }
        log.info("任务恢复成功,taskId:{}", taskId);

        return "任务恢复成功";
    }

    @Override
    @Transactional
    public String delete(String taskId) {
        ScheduledFuture<?> future = scheduledTasks.get(taskId);
        if (future != null) {
            // 取消任务调度但不中断正在执行的任务
            boolean flag = future.cancel(false);
            if (flag) {
                // 移除任务
                scheduledTasks.remove(taskId);
                // 删除数据库中任务
                taskInfoMapper.deleteById(taskId);

                log.info("任务删除成功,taskId:{}", taskId);
            }
        }

        return "任务删除成功";
    }

    // ---------------------------------------- private method ----------------------------------------

    /**
     * 编辑任务
     *
     * @param taskInfo 任务信息对象，包含任务的各种属性
     * @param isEdit   是否为编辑操作 true-编辑操作 false-添加操作
     * @return 任务创建结果信息
     */
    private String editTask(TaskInfo taskInfo, boolean isEdit) {
        String msg = isEdit ? "编辑任务信息:" : "新增任务信息：";
        System.out.println(msg + JSONObject.toJSONString(taskInfo));

        String taskId = UUID.randomUUID().toString();
        if (isEdit) {
            taskId = taskInfo.getTaskId();
            // 获取任务信息
            TaskInfo oldTaskInfo = taskInfoMapper.selectById(taskId);
            if (oldTaskInfo == null) {
                return "任务不存在";
            }
        }

        // 检查任务信息的合法性
        String result = checkTaskInfo(taskInfo, isEdit);
        if (StringUtils.hasLength(result)) {
            return result;
        }

        // 创建一个Runnable任务
        Runnable task = createTask(taskInfo);
        if (task == null) {
            return "新的任务创建失败";
        }

        // 根据任务信息和任务逻辑进行任务调度
        ScheduledFuture<?> scheduledFuture = createSchedule(taskInfo, task);
        if (scheduledFuture == null) {
            return "不支持的调度类型";
        }
        scheduledTasks.put(taskId, scheduledFuture);

        // 组合任务信息
        taskInfo.setTaskId(taskId);
        taskInfo.setState(1);
        taskInfo.setUpdateTime(new Date());

        int rowCount;
        if (isEdit) {
            // 更新数据库中任务信息,并清理多余的任务数据
            LambdaUpdateWrapper<TaskInfo> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(TaskInfo::getTaskId, taskId);
            updateWrapper.set(TaskInfo::getModeType, taskInfo.getModeType());
            updateWrapper.set(TaskInfo::getExeClass, taskInfo.getExeClass());
            updateWrapper.set(TaskInfo::getExeMethod, taskInfo.getExeMethod());
            updateWrapper.set(TaskInfo::getInitialDelay, taskInfo.getInitialDelay());
            updateWrapper.set(TaskInfo::getInitialDelayUnit, taskInfo.getInitialDelayUnit());
            updateWrapper.set(TaskInfo::getFixedDelay, taskInfo.getFixedDelay());
            updateWrapper.set(TaskInfo::getFixedRate, taskInfo.getFixedRate());
            updateWrapper.set(TaskInfo::getTimeUnit, taskInfo.getTimeUnit());
            updateWrapper.set(TaskInfo::getCron, taskInfo.getCron());
            updateWrapper.set(TaskInfo::getProcessTime, taskInfo.getProcessTime());
            updateWrapper.set(TaskInfo::getUpdateTime, taskInfo.getUpdateTime());
            updateWrapper.set(TaskInfo::getState, taskInfo.getState());
            updateWrapper.set(TaskInfo::getRemark, taskInfo.getRemark());

            rowCount = taskInfoMapper.update(updateWrapper);
        } else {
            taskInfo.setCreateTime(new Date());
            // 插入一条记录
            rowCount = taskInfoMapper.insert(taskInfo);
        }

        String message = isEdit ? "任务编辑成功" : "任务创建成功";
        if (rowCount == 0) {
            message = isEdit ? "任务编辑失败" : "任务保存失败";
        }
        log.info("{},taskId:{}", message, taskId);

        return message;
    }

    /**
     * 检查任务信息的合法性
     *
     * @param taskInfo 任务信息对象，包含任务的各种属性
     * @param isEdit   是否为编辑操作 true-编辑操作 false-添加操作
     * @return 如果任务信息中的任何属性不符合规范，则返回相应的错误信息；如果一切属性都符合规范，则返回null
     */
    private String checkTaskInfo(TaskInfo taskInfo, boolean isEdit) {
        int modeType = taskInfo.getModeType();

        if (isEdit) {
            if (ObjectUtils.isEmpty(taskInfo.getTaskId())) {
                return "任务ID为空";
            }
        }

        if (ObjectUtils.isEmpty(taskInfo.getExeClass())) {
            return "类名为空";
        }
        if (ObjectUtils.isEmpty(taskInfo.getExeMethod())) {
            return "方法名为空";
        }
        if (modeType < 0 || modeType > 3) {
            return "无效的调度模式";
        }

        // 根据不同的执行模式，检查任务信息的其他属性
        switch (modeType) {
            case 0:
                // 如果是Cron模式，检查Cron表达式是否为空
                if (ObjectUtils.isEmpty(taskInfo.getCron())) {
                    return "Cron 表达式为空";
                }
                break;
            case 1:
                // 如果是定时执行模式，检查执行时间是否为空
                if (ObjectUtils.isEmpty(taskInfo.getProcessTime())) {
                    return "执行时间为空";
                }
                break;
            case 2:
                // 固定间隔执行,检查周期和单位是否为有效值
                if (taskInfo.getFixedDelay() == null || taskInfo.getFixedDelay() <= 0) {
                    return "固定间隔为空或小于等于零";
                }
                if (ObjectUtils.isEmpty(taskInfo.getTimeUnit())) {
                    return "时间单位为空";
                }
                try {
                    // 进一步验证时间单位是否为系统支持的值
                    ToolPeriodUnit.getValue(taskInfo.getTimeUnit());
                } catch (IllegalArgumentException e) {
                    return "无效的时间单位";
                }
                break;
            case 3:
                // 固定频率执行,检查周期和单位是否为有效值
                if (taskInfo.getFixedRate() == null || taskInfo.getFixedRate() <= 0) {
                    return "固定频率为空或小于等于零";
                }
                if (ObjectUtils.isEmpty(taskInfo.getTimeUnit())) {
                    return "时间单位为空";
                }
                try {
                    // 进一步验证时间单位是否为系统支持的值
                    ToolPeriodUnit.getValue(taskInfo.getTimeUnit());
                } catch (IllegalArgumentException e) {
                    return "无效的时间单位";
                }
                break;
        }

        LambdaQueryWrapper<TaskInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TaskInfo::getExeClass, taskInfo.getExeClass());
        queryWrapper.eq(TaskInfo::getExeMethod, taskInfo.getExeMethod());

        Long rowCount = taskInfoMapper.selectCount(queryWrapper);
        if ((!isEdit && rowCount > 0) || (isEdit && rowCount > 1)) {
            return "当前类的调度方法,任务已存在";
        }

        // 如果所有检查都通过，则返回null表示任务信息合法
        return null;
    }

    /**
     * 功能：创建一个Runnable任务
     * 详细描述：该方法根据任务信息（类名和执行方法名）动态加载类和方法，并创建一个Runnable任务,主要用于在运行时灵活地执行不同的任务
     *
     * @param taskInfo 任务信息，包含要执行的类名和方法名
     * @return Runnable 返回一个Runnable任务，该任务在运行时会执行指定类的指定方法
     */
    private Runnable createTask(TaskInfo taskInfo) {
        Runnable task = null;
        try {
            // 获取类对象
            Class<?> clazz = Class.forName(taskInfo.getExeClass());
            // 获取方法对象
            Method method = clazz.getDeclaredMethod(taskInfo.getExeMethod());
            // 创建类的实例
            Object instance = clazz.getDeclaredConstructor().newInstance();

            task = () -> {
                try {
                    // 执行方法
                    method.invoke(instance);
                } catch (Exception e) {
                    log.error("执行任务发生错误: {}", e.getMessage());
                }
            };
        } catch (Exception e) {
            // 处理类加载和方法获取时的异常
            log.error("创建任务发生错误: {}", e.getMessage());
        }

        return task;
    }

    /**
     * 根据任务信息和任务逻辑创建任务调度信息
     *
     * @param taskInfo 包含任务配置信息的 TaskInfo 对象
     * @param task     需要调度的 Runnable 任务
     * @return 调度结果的 ScheduledFuture 对象，如果调度类型不支持则返回 null
     */
    private ScheduledFuture<?> createSchedule(TaskInfo taskInfo, Runnable task) {
        int modeMode = taskInfo.getModeType();
        Long initialDelay = taskInfo.getInitialDelay();
        Date processTime = taskInfo.getProcessTime();

        Duration duration = null;
        if (modeMode == 2) {
            duration = Duration.of(taskInfo.getFixedDelay(), ToolPeriodUnit.getValue(taskInfo.getTimeUnit()));
        } else if (modeMode == 3) {
            duration = Duration.of(taskInfo.getFixedRate(), ToolPeriodUnit.getValue(taskInfo.getTimeUnit()));
        }

        ScheduledFuture<?> future = null;
        try {
            switch (modeMode) {
                case 0:
                    // 使用 cron 表达式
                    future = threadPoolTaskScheduler.schedule(task, new CronTrigger(taskInfo.getCron()));
                    break;
                case 1:
                    // 指定时间执行一次，并且如果是过去的时间，则立即执行，如果是未来时间，则从指定时间开始执行
                    future = threadPoolTaskScheduler.schedule(task, processTime.toInstant());
                    break;
                case 2:
                    // 固定间隔执行
                    if (null == initialDelay) {
                        // 没有指定开始时间，则立即执行
                        future = threadPoolTaskScheduler.scheduleWithFixedDelay(task, duration);
                    } else {
                        // 获取任务执行时间
                        processTime = getProcessTime(initialDelay, taskInfo.getInitialDelayUnit());
                        // 指定开始时间，并且如果是过去的时间，则立即执行，如果是未来时间，则从开始时间开始执行
                        future = threadPoolTaskScheduler.scheduleWithFixedDelay(task, processTime.toInstant(), duration);
                    }
                    break;
                case 3:
                    // 固定频率执行
                    if (null == initialDelay) {
                        // 没有指定开始时间，则立即执行
                        future = threadPoolTaskScheduler.scheduleAtFixedRate(task, duration);
                    } else {
                        // 获取任务执行时间
                        processTime = getProcessTime(initialDelay, taskInfo.getInitialDelayUnit());
                        // 指定开始时间，并且如果是过去的时间，则立即执行，如果是未来时间，则从开始时间开始执行
                        future = threadPoolTaskScheduler.scheduleAtFixedRate(task, processTime.toInstant(), duration);
                    }
                    break;
            }
        } catch (Exception e) {
            log.error("任务调度发生错误: {}", e.getMessage());
        }
        return future;
    }

    /**
     * 根据初始延迟和其单位计算处理时间
     *
     * @param initialDelay     初始延迟的时间值(正数增加，负数减少)
     * @param initialDelayUnit 初始延迟的时间单位 seconds-秒 minute-分钟 day-天
     * @return 计算得到的处理时间
     */
    private Date getProcessTime(long initialDelay, String initialDelayUnit) {
        // 根据不同的时间单位转换初始延迟为秒
        if (ToolPeriodUnit.MINUTE.getKey().equals(initialDelayUnit)) {
            initialDelay = initialDelay * 60;
        } else if (ToolPeriodUnit.DAY.getKey().equals(initialDelayUnit)) {
            initialDelay = initialDelay * 60 * 24;
        }
        // 获取当前日期和时间，并转换为Instant对象
        Instant instant = new Date().toInstant();
        // 根据初始延迟增加时间
        instant = instant.plusSeconds(initialDelay);

        // 返回增加延迟后的时间
        return Date.from(instant);
    }
}
