SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for task_info
-- ----------------------------
DROP TABLE IF EXISTS `task_info`;
CREATE TABLE `task_info`  (
  `task_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `mode_type` int NULL DEFAULT NULL COMMENT '调度类型 \r\n0-cron表达式 \r\n1-指定时间执行(仅1次) \r\n2-任务结束固定延迟执行(fixedDelay)\r\n3-任务开始固定速率执行(fixedRate) ',
  `exe_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '调度类(全路径)',
  `exe_method` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '调度方法(exe_class中的方法)',
  `initial_delay` bigint NULL DEFAULT NULL COMMENT '初始延时时间(与初始延时时间单位组合使用)',
  `initial_delay_unit` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '初始延时时间单位 \r\nseconds-秒 \r\nminute-分钟 \r\nday-天',
  `fixed_delay` bigint NULL DEFAULT NULL COMMENT '固定间隔(与任务执行时间单位组合使用)',
  `fixed_rate` bigint NULL DEFAULT NULL COMMENT '固定频率(与任务执行时间单位组合使用)',
  `time_unit` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务执行时间单位 \r\nseconds-秒 \r\nminute-分钟 \r\nday-天 \r\nweek-星期 \r\nmonth-月 \r\nyear-年',
  `cron` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `process_time` datetime NULL DEFAULT NULL COMMENT '任务执行的时间(指定任务在特定时间点执行)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `state` int NULL DEFAULT NULL COMMENT '状态(0-停止 1-进行中)',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '任务调度信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of task_info
-- ----------------------------
INSERT INTO `task_info` VALUES ('2c419b90-6f44-42c6-80c1-a9b54771b341', 3, 'com.qfx.module.job.TestJob', 'sayHello', 3, 'second', NULL, 12, 'second', NULL, NULL, '2025-01-10 17:38:17', '2025-01-10 17:38:17', 1, NULL);
INSERT INTO `task_info` VALUES ('9199ef94-c72d-4bf2-86b9-573aa35125bd', 1, 'com.qfx.module.job.TestJob', 'execTwo', NULL, NULL, NULL, NULL, NULL, NULL, '2025-01-10 17:38:09', '2025-01-10 17:37:59', '2025-01-10 17:37:59', 1, NULL);
INSERT INTO `task_info` VALUES ('ec821712-ffac-49d9-9a77-f5a0f48a4abf', 2, 'com.qfx.module.job.TestJob', 'test', 3, 'second', 12, NULL, 'second', NULL, NULL, '2025-01-10 17:38:08', '2025-01-10 17:38:08', 1, NULL);
INSERT INTO `task_info` VALUES ('f76924e5-ba07-45c9-987d-cf81055973dc', 0, 'com.qfx.module.job.TestJob', 'exec', NULL, NULL, NULL, NULL, NULL, '0/15 * * * * ?', NULL, '2025-01-10 17:37:51', '2025-01-10 17:37:51', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
