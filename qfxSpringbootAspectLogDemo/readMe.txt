1.这是一个使用AOP统一处理Web请求日志的示例
2.测试地址：
	Springboot启动
		// 普通日志切面
		http://localhost/test/hello?userName=珊珊&age=17
		http://localhost/test/helloTwo/琳琳?age=16
		http://localhost/test/helloTwo/琳琳?&age=16
		http://localhost/test/helloThree?userName=珊珊&age=17
		http://localhost/test/helloFour
		    {
		        "userName":"琳琳",
		        "age":"17"
		    }
		http://localhost/test/save?userName=珊珊&age=17 (无返回值)
		http://localhost/test2/eat?foodName=面包&count=100
		
		// 异常通知
		http://localhost/test2/calc?a=10&b=0
			
		// execution表达式使用
		http://localhost/aspect/test/play