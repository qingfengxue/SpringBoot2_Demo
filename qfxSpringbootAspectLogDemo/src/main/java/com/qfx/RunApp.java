package com.qfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class RunApp {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(RunApp.class, args);
		
		Environment environment = context.getBean(Environment.class);
		String port = environment.getProperty("server.port");
		String ctxPath = environment.getProperty("server.servlet.context-path");
		ctxPath = ctxPath == null ? "/" : ctxPath;

		System.out.println("项目已启动完毕！地址请访问 http://localhost:" + port + ctxPath);
	}
}
