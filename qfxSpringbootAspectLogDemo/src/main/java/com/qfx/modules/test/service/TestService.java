package com.qfx.modules.test.service;

import org.springframework.stereotype.Service;

@Service
public class TestService {
	
	public String hello(String userName, Integer age) {
		return userName + "你好,欢迎" + age + "的你来到java的世界!";
	}
	
	public String eat(String foodName) {
		return "发现了一种名字叫[" + foodName + "]的新食物!";
	}
}
