package com.qfx.modules.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.modules.test.service.TestService;

@RestController
@RequestMapping("test2")
public class TestCtl2 {
	
	@Autowired
	TestService testService;
	
	@RequestMapping("eat")
	public String eat(String foodName) {
		return "发现了一种名字叫[" + foodName + "]的新食物!";
	}
	
	@RequestMapping("calc")
	public double calc(int a, int b) {
		double c = a/b;
		return c;
	}
}
