package com.qfx.modules.test.controller;

import com.qfx.modules.bean.User;
import com.qfx.modules.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("test")
public class TestCtl {
	
	@Autowired
	TestService testService;

	@RequestMapping("sayHello")
	public String sayHello() {
		return "你好呀!";
	}

	@RequestMapping("hello")
	public String hello(String userName, Integer age) {
		return testService.hello(userName, age);
	}
	
	@RequestMapping("helloTwo/{userName}")
	public String helloTwo(@PathVariable("userName") String userName, Integer age) {
		return testService.hello(userName, age);
	}

	@RequestMapping("helloThree")
	public String helloThree(User user) {
		return testService.hello(user.getUserName(), user.getAge());
	}

	@PostMapping("helloFour")
	public String helloFour(@RequestBody User user) {
		return testService.hello(user.getUserName(), user.getAge());
	}

	@RequestMapping("save")
	public void save(String userName, Integer age) {
		System.out.println(userName +"你好，你今年" + age + "岁了吗");
	}
}
