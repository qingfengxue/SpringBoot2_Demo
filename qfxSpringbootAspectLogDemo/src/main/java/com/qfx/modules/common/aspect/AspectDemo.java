package com.qfx.modules.common.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectDemo {
	
	/**
	 * <h5>功能:定义一个切入点,可以是一个规则表达式,也可以是一个注解</h5>
	 * 这里匹配com.qfx.modules.*.service包及子孙包下所有类的所有方法
	 * .*匹配包，但不匹配子包,..*匹配包及子包
	 */
	@Pointcut("execution(* com.qfx.modules.*.service..*(..))")
	public void testLog1() {
	}
	
	/**
	 * <h5>功能:定义一个切入点,可以是一个规则表达式,也可以是一个注解</h5>
	 * 这里匹配com.qfx.modules.*.service包下AspectServie类的所有方法
	*/
	@Pointcut("execution(* com.qfx.modules.*.service.AspectServie.*(..))")
	public void testLog2() {
	}
	
	/**
	 * <h5>功能:定义一个切入点,可以是一个规则表达式,也可以是一个注解</h5>
	 * 这里匹配com.qfx.modules.*.service包下所有类的所有方法
	 */
	@Pointcut("execution(* com.qfx.modules.*.service.*.*(..))")
	public void testLog3() {
	}
	
	// ===========================================================
	@Before("testLog1()")
	public void test1(JoinPoint joinPoint) {
		System.out.println("我被testLog1()执行了。。。。");
	}
	
	@Before("testLog2()")
	public void test2(JoinPoint joinPoint) {
		System.out.println("我被testLog2()执行了。。。。");
	}
	
	@Before("testLog3()")
	public void test3(JoinPoint joinPoint) {
		System.out.println("我被testLog3()执行了。。。。");
	}

}
