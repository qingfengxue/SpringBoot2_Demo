package com.qfx.modules.common.aspect;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Aspect		// 将⼀个java类定义为切⾯类
@Component
public class WebLogAspect {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 记录开始时间
	 */
	private static final ThreadLocal<Long> startTime = new ThreadLocal<>();
	
	/**
	 * <h5>功能:定义一个切入点,只能是常量,可以是一个规则表达式,也可以是一个注解</h5>
	 * 这里匹配com.qfx.modules.*.controller包及子包下所有类的所有方法,表达式说明:
	 * 	.*匹配包，但不匹配子包,..*匹配包及子包
	 * <p>
	 * 包路径格式(从上往下,匹配范围越来越大,匹配精度越来越小):
	 * 	1.配置指定层级中,指定的包: com.qfx.modules.controller
	 * 	2.匹配指定层级中,任意包中指定的包: com.qfx.*.controller
	 * 	3.匹配指定层级中,任意层级中指定的包: com.qfx..controller
	 * 	4.配置任意层级中,指定的包: *..controller
	*/
	@Pointcut("execution(* com.qfx.modules.*.controller..*(..))")
	public void webLog() {
	}
	
//	根据需要在不同位置切入目标方法:
//	@Before	前置通知(在目标方法执行之前执行)
//	@After  后置通知(在目标方法执行之后执行,无论目标方法执行成功还是出现异常,都将执行后置方法)
//	@AfterReturning 返回通知(在目标方法执行成功后才会执行,如果目标方法出现异常,则不执行;可以用来对返回值做一些加工处理)
//	@AfterThrowing	异常通知(只在目标方法出现异常后才会执行,否则不执行;可以获取目标方法出现的异常信息)
//	@Around	环绕通知(包含上面四种通知方法,环绕通知的功能最全面.环绕通知需要携带ProceedingJoinPoint类型的参数,且环绕通知必须有返回值,返回值即为目标方法的返回值)

	/**
	 * <h5>功能:前置通知</h5>
	 * 建议根据业务情况选择是否用启环绕通知，环绕通知比前置通知优先级更高
	 * 
	 * @param joinPoint 被拦截方法信息
	*/
	@Before("webLog()")
	public void doBefore(JoinPoint joinPoint) {
		// 换行
		System.out.println();
		startTime.set(System.currentTimeMillis());
		logger.debug("-------------------- 通知处理开始 --------------------");
		
		// 获取当前线程的 HttpServletRequest,接收到请求,记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if (attributes != null) {
			HttpServletRequest request = attributes.getRequest();
			// 获取真实的客户端IP地址
			String ipAddr = getRemoteIp(request);
			// 获取请求参数并序列化
			String paramsInfo = getParamsInfo(joinPoint);

			// 记录下请求内容
			logger.debug("{} 请求信息:[{}] {}", ipAddr, request.getMethod(), request.getRequestURL().toString()); // 记录请求方法和URL
			logger.debug("{} 调用方法:{}", ipAddr, joinPoint.getSignature()); // 记录调用的方法
			logger.debug("{} 方法参数:{}", ipAddr, paramsInfo); // 记录请求参数
		}
	}

	/**
	 * <h5>功能:返回通知</h5>
	 * 建议根据业务情况选择是否启用环绕通知，环绕通知比返回通知优先级更高
	 * 
	 * @param returnInfo 特殊参数(returning):设定使用通知方法参数接收返回值的变量名
	*/
	@AfterReturning(pointcut = "webLog()", returning = "returnInfo")
	public void doAfterReturning(Object returnInfo) {
		// 获取当前线程的 HttpServletRequest,接收到请求,记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if (attributes != null) {
			HttpServletRequest request = attributes.getRequest();
			// 获取真实的客户端IP地址
			String ipAddr = getRemoteIp(request);

			// 处理完请求，返回内容
			logger.debug("{} 返回信息:{}", ipAddr, JSONObject.toJSONString(returnInfo));
			logger.debug("{} 耗时:{}秒", ipAddr, (System.currentTimeMillis() - startTime.get()) / 1000d);
		}
		logger.debug("-------------------- 通知处理结束 --------------------");
	}
	
	/**
	 * <h5>功能:异常通知(一般不使用这个)</h5>
	 * 通过throwing属性指定目标方法出现的异常信息存储在e变量中,在异常通知方法中就可以从e变量中获取异常信息了
	 * 建议根据业务情况选择是否启用环绕通知，环绕通知比异常通知优先级更高
	 *
	 * @param joinPoint 被拦截方法信息
	 * @param e 特殊参数(throwing):设定使用通知方法参数接收原始方法中抛出的异常对象名
	*/
	@AfterThrowing(pointcut = "webLog()", throwing = "e")
	public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
		// 获取当前线程的 HttpServletRequest,接收到请求,记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if (attributes != null) {
			HttpServletRequest request = attributes.getRequest();
			// 获取真实的客户端IP地址
			String ipAddr = getRemoteIp(request);

			logger.error("{} 调用方法:{} {}", ipAddr, joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
			logger.error("{} 接收参数:{}", ipAddr, Arrays.toString(joinPoint.getArgs()));
			logger.error("{} 异常信息:", ipAddr, e);
		}
		logger.error("-------------------- 通知处理结束 --------------------");
	}

	/**
	 * <h5>功能:环绕通知()</h5>
	 * 建议根据业务情况选择是否启用前置通知或返回通知以及异常通知，环绕通知比前置通知和返回通知优先级更高
	 *
	 * @param joinPoint 被拦截方法信息
	 */
	@Around("webLog()")
	public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
		// 换行
		System.out.println();
		logger.debug("-------------------- 环绕通知处理开始 --------------------");
		long startTime = System.currentTimeMillis();

		// 接收到请求，记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

		Object result = null;
        if (attributes != null) {
			HttpServletRequest request = attributes.getRequest();
			// 获取真实的客户端IP地址
			String ipAddr = getRemoteIp(request);
			// 获取请求参数并序列化
			String paramsInfo = getParamsInfo(joinPoint);

			// 前置通知
			logger.debug("{} 请求信息:[{}] {}", ipAddr, request.getMethod(), request.getRequestURL().toString());
			logger.debug("{} 调用方法: {}", ipAddr, joinPoint.getSignature());
			logger.debug("{} 方法参数: {}", ipAddr, paramsInfo);

			try {
				// 执行目标方法
				result = joinPoint.proceed();
			} catch (Throwable e) {
				logger.error("{} 请求异常:", ipAddr, e);
				throw e;
			} finally {
				// 返回通知
				if (result != null) {
					logger.debug("{} 方法返回值:\r\n{}", ipAddr, result);
				} else {
					logger.debug("{} 方法无返回值", ipAddr);
				}

				// 计算执行时间
				logger.debug("{} 耗时:{}秒", ipAddr, (System.currentTimeMillis() - startTime)/1000d);
				logger.debug("-------------------- 环绕通知处理结束 --------------------");
			}
		}

		return result;
	}

	// ---------------------------------------- private method ----------------------------------------
	/**
	 * <h5>功能:获取真实的客户端IP地址</h5>
	 *
	 * @param request HTTP请求
	 *
	 * @return 客户端IP地址
	 */
	private String getRemoteIp(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * <h5>功能:功能：获取请求参数并序列化</h5>
	 *
	 * @param joinPoint 被拦截方法信息
	 *
	 * @return String
	 */
	private String getParamsInfo(JoinPoint joinPoint) {
		String paramsInfo;
		ObjectMapper objectMapper = new ObjectMapper();
        try {
            paramsInfo = objectMapper.writeValueAsString(joinPoint.getArgs());
        } catch (JsonProcessingException e) {
			paramsInfo = "获取请求参数发生异常：" + e.getMessage();
        }
		return paramsInfo;
    }
}
