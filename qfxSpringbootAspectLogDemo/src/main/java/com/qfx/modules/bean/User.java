package com.qfx.modules.bean;

/**
 * 功能：User
 * 创建者:user 日期:2024/11/22
 */
public class User {

    String userName;
    Integer age;
    Integer sex;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
}
