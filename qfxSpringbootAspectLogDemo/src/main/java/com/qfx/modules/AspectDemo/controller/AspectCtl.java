package com.qfx.modules.AspectDemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.modules.AspectDemo.service.AspectServie;
import com.qfx.modules.AspectDemo.service.userSer.UserSer;

@RestController
@RequestMapping("aspect/test")
public class AspectCtl {
	@Autowired
	UserSer userSer;
	@Autowired
	AspectServie aspectServie;
	
	@RequestMapping("play")
	public String play() {
		System.out.println("开始调用getUser");
		String info = userSer.getUser() + ",";
		System.out.println("开始调用play");
		info += aspectServie.play();
		return info;
	}
}
