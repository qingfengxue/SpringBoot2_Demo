package com.qfx.modules.preview.service;

import com.qfx.modules.common.vo.ResultVO;

/**
 * <h5>功能:文件相关业务</h5>
 * 
 */
public interface PreviewSer {
	
	/**
	 * <h5>功能:获取目录列表</h5>
	 * 
	 * @param 目录名称
	 * 
	 * @return
	 */
	ResultVO getDirectoryList(String directoryName);
	
	/**
	 * <h5>功能:获取文件列表</h5>
	 * 
	 * @param 目录名称
	 * 
	 * @return
	 */
	ResultVO getFileList(String directoryName);
}
