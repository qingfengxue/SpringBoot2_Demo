package com.qfx.modules.preview.vo;

/**
 * <h5>功能:文件信息</h5>
 * 
 */
public class FileVO {
	
	/**
	 * 文件名称
	 */
	private String fileName;
	
	/**
	 * 文件路径
	 */
	private String filePath;
	
	/**
	 * 后缀名
	 */
	private String fileSuffix;
	
	/**
	 * 文件类型 0-目录 1-文件
	 */
	private int fileType;
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileSuffix() {
		return fileSuffix;
	}

	public void setFileSuffix(String fileSuffix) {
		this.fileSuffix = fileSuffix;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
}
