package com.qfx.modules.preview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.modules.common.vo.ResultVO;
import com.qfx.modules.preview.service.PreviewSer;

@RestController
@RequestMapping("/preview")
public class PreviewCtl {
	
	@Autowired
	PreviewSer previewSer;
	
	/**
	 * <h5>功能:获取目录列表</h5>
	 * 
	 * @param directoryName 目录名称
	 * 
	 * @return ResultVO
	 */
	@CrossOrigin
	@GetMapping("/directoryList")
	public ResultVO directoryList(@RequestParam(defaultValue = "") String directoryName) {
		return previewSer.getDirectoryList(directoryName);
	}
	
	/**
	 * <h5>功能:获取文件列表</h5>
	 * 
	 * @param directoryName 目录名称
	 * 
	 * @return ResultVO
	 */
	@CrossOrigin
	@GetMapping("/fileList")
	public ResultVO fileList(@RequestParam(defaultValue = "") String directoryName) {
		return previewSer.getFileList(directoryName);
	}
}
