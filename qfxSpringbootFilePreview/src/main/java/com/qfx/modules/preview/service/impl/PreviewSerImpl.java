package com.qfx.modules.preview.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.qfx.modules.common.tool.ToolFiles;
import com.qfx.modules.common.tool.ToolErrorEnmu;
import com.qfx.modules.common.vo.ResultVO;
import com.qfx.modules.preview.service.PreviewSer;
import com.qfx.modules.preview.vo.FileVO;

@Service
public class PreviewSerImpl implements PreviewSer {
	
	@Value("${temp.dir.root.path}")
	private String dirRootPaht;
	
	@Value("${temp.file.type}")
	private String fileType;

	@Override
	public ResultVO getDirectoryList(String directoryName) {
		String dirPath = dirRootPaht + File.separator + directoryName;
		
		// 1.验证文件或者文件夹是否存在
		boolean flag = ToolFiles.validateFileExist(dirPath);
		if (!flag) {
			return new ResultVO(ToolErrorEnmu.ERROR_CHECK_FILE.getCode(), ToolErrorEnmu.ERROR_CHECK_FILE.getMsg(), dirPath);
		}
		
		// 2.获取指定目录下的所有文件夹名称及路径(单层)
		List<Map<String, String>> directoryList = ToolFiles.getDirectory(dirPath);
		
		
		// 3.将目录信息放在集合中
		List<FileVO> fileVOList = new ArrayList<>();
		FileVO fileVO = null;
		String urlPath = ObjectUtils.isEmpty(directoryName) ? "" : directoryName + File.separator;
		
		for (Map<String, String> fileMap : directoryList) {
			fileVO = new FileVO();
			fileVO.setFileName(fileMap.get("folderName"));
			fileVO.setFilePath(urlPath + fileMap.get("folderName"));
			fileVO.setFileType(0);
			
			fileVOList.add(fileVO);
		}
		
		return new ResultVO(fileVOList);
	}

	@Override
	public ResultVO getFileList(String directoryName) {
		String dirPath = dirRootPaht + File.separator + directoryName;
		
		// 1.验证文件或者文件夹是否存在
		boolean flag = ToolFiles.validateFileExist(dirPath);
		if (!flag) {
			return new ResultVO(ToolErrorEnmu.ERROR_CHECK_FILE.getCode(), ToolErrorEnmu.ERROR_CHECK_FILE.getMsg(), dirPath);
		}
		
		// 2.获取指定目录下的所有文件夹名称及路径(单层)
		
		List<String> fileList = null;
		String urlPath = ObjectUtils.isEmpty(directoryName) ? "" : directoryName + File.separator;
		
		if (ObjectUtils.isEmpty(fileType)) {
			fileList = ToolFiles.getFileNameList(dirPath);
		} else {
			fileList = ToolFiles.getFileNameList(dirPath, fileType.split(","));
		}
		
		// 3.将文件信息放在集合中
		List<FileVO> fileVOList = new ArrayList<>();
		FileVO fileVO = null;
		for (String fileName : fileList) {
			fileVO = new FileVO();
			fileVO.setFileName(fileName);
			fileVO.setFilePath(urlPath + fileName);
			fileVO.setFileSuffix(fileName.substring(fileName.lastIndexOf(".")));
			fileVO.setFileType(1);
			
			fileVOList.add(fileVO);
		}
		
		return new ResultVO(fileVOList);
	}
}
