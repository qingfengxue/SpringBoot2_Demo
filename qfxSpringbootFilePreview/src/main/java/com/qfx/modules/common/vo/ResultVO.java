package com.qfx.modules.common.vo;

/**
 * <h5>功能:返回前端数据对象</h5>
 * 
 */
public class ResultVO {
	/**
	 * 返回编码
	 */
	private int code = 1000;
	
	/**
	 * 提示信息
	 */
	
	private String message = "请求成功!";
	
	/**
	 * 其他信息
	 */
	private Object data = null;
	
	/**
	 * 时间戳
	 */
	private Long time = System.currentTimeMillis();
	
	public ResultVO(int code) {
		this.code = code;
	}

	public ResultVO(String message) {
		super();
		this.message = message;
	}

	public ResultVO(Object data) {
		super();
		this.data = data;
	}

	public ResultVO(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public ResultVO(String message, Object data) {
		this.message = message;
		this.data = data;
	}

	public ResultVO(int code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

}
