package com.qfx.modules.common.tool;

/**
 * <h5>描述:存放各类错误信息状态码</h5>
 *  
 */
public enum ToolErrorEnmu {
	// 常量数据
	/**
	 * 目录或文件不存在
	 */
	ERROR_CHECK_FILE	(1001, "目录或文件不存在");

	// 常量数据对应的参数信息
	// 编码
	private final int code;
	// 提示信息
	private final String msg;

	// 带参的构造函数,参数类型要与常量数据中的数据类型顺序一致
	ToolErrorEnmu(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}


	public static void main(String[] args) throws Exception {
		System.out.println(ToolErrorEnmu.ERROR_CHECK_FILE.getCode());
		System.out.println(ToolErrorEnmu.ERROR_CHECK_FILE.getMsg());
	}
}
