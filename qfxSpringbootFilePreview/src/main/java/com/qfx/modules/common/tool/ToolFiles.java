package com.qfx.modules.common.tool;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.alibaba.fastjson2.JSONObject;

public class ToolFiles {
    
    /**
     * <h5>功能:创建多级目录</h5>
     * 
     * @param folderPath 文件夹地址
     * @return 
     */
    public static boolean mkdirs(String folderPath) {
    	boolean flag = false;
    	File file = new File(folderPath);
    	// 目录不存在则创建目录,支持多级目录
		if (!file.exists()) {
			flag = file.mkdirs();
		}
		return flag;
    }
    
    /**
     * <h5>功能:获取文件名称</h5>
     *  a/b/c.txt --> c.txt<br/>
     *  a.txt     --> a.txt<br/>
     *  a/b/c     --> c<br/>
     *  a/b/c/    --> ""
     * 
     * @param filePath 文件全路径名称
     * @return 
     */
    public static String getFileName(String filePath) {
    	return FilenameUtils.getName(filePath);
    }
    
    /**
     * <h5>功能:获取文件目录全路径</h5>
     * C:\a\b\c.txt --> C:\a\b\<br/>
     * ~/a/b/c.txt  --> ~/a/b/
     * @param filePath 文件全路径
     * @return 
     */
    public static String getFileFullPath(String filePath) {
    	return FilenameUtils.getFullPath(filePath);
    }
    
    /**
     * <h5>功能:获取文件类型</h5>
     * foo.txt   --> "txt"<br/>
 	 * a/b/c.jpg --> "jpg"
     * @param fileName 全路径名称或纯文件名称均可
     * @return 
     */
    public static String getExtension(String fileName) {
    	return FilenameUtils.getExtension(fileName);
    }
    
    /**
     * <h5>功能:验证文件或者文件夹是否存在</h5>
     * 
     * @param filePath 指定的文件或者文件夹绝对路径
     * @return 
     */
    public static boolean validateFileExist(String filePath) {
    	Boolean flag = false;
    	//判断文件或者文件夹是否存在
		File file = new File(filePath);
		if (file.exists()) {
			flag = true;
		}
		return flag;
    }
    
    /**
     * <h5>功能:获取指定目录下的所有文件夹名称及路径(单层)</h5>
     * @param dirPath
     * @return
     */
    public static List<Map<String, String>> getDirectory(String dirPath){
    	List<Map<String, String>> directoryList = new ArrayList<>();
    	
    	// 验证文件夹是否存在
        boolean flag = validateFileExist(dirPath);
        if (flag) {
        	File file = new File(dirPath);
    		File[] listFiles = file.listFiles();
    		
    		Map<String, String> fileMap = null;
    		for (File tempFile : listFiles) {
    			// 验证是否文件夹
    			if (tempFile.isDirectory()) {
    				fileMap = new HashMap<>();
    				fileMap.put("folderName", tempFile.getName());
    				fileMap.put("folderPath", tempFile.getAbsolutePath());
    				
    				directoryList.add(fileMap);
    			}
    		}
        }
    	return directoryList;
    }

    /**
     * <h5>功能:获取指定目录下的所有文件名(单层,仅文件名称)</h5>
     * @param folderPath 指定的文件夹目录(全路径)
     * @return List<String>
     */
    public static List<String> getFileNameList(String folderPath) {
        List<String> fileNamelist = new ArrayList<String>();
        // 验证文件夹是否存在
        boolean flag = validateFileExist(folderPath);
        if (flag) {
        	Collection<File> listFiles = FileUtils.listFiles(new File(folderPath), null, false);
        	for (File file : listFiles) {
        		fileNamelist.add(file.getName());
        	}
		}

        return fileNamelist;
    }

    /**
     * <h5>功能:获取指定目录下的指定类型的所有文件名(单层,仅文件名称)</h5>
     * @param folderPath 指定的文件目录(全路径)
     * @param fileType 文件类型
     * @return List<String>
     */
    public static List<String> getFileNameList(String folderPath, String fileType) {
        return getFileNameList(folderPath, new String[] {fileType});
    }
    
    /**
     * <h5>功能:获取指定目录下的指定类型(可多个)的所有文件名(单层,仅文件名称)</h5>
     * @param folderPath 指定的文件目录(全路径)
     * @param fileType 文件类型,数组
     * @return List<String>
     */
    public static List<String> getFileNameList(String folderPath,String[] fileType) {
    	List<String> fileNamelist = new ArrayList<String>();
    	// 验证文件夹是否存在
        boolean flag = validateFileExist(folderPath);
        if (flag) {
	    	Collection<File> listFiles = FileUtils.listFiles(new File(folderPath), fileType, false);
	    	for (File file : listFiles) {
	    		fileNamelist.add(file.getName());
	    	}
        }
    	
    	return fileNamelist;
    }
    
    public static void main(String[] args) {
    	String filePath = "F:\\myself";
    	
    	List<Map<String, String>> directoryList = getDirectory(filePath);
    	for (Map<String, String> map : directoryList) {
			System.out.println(JSONObject.toJSONString(map));
		}
    	
    	List<String> fileList = getFileNameList(filePath, "mp3");
    	for (String fileName : fileList) {
			System.out.println(fileName);
		}
    	
//    	cleanFiles("E:\\tempFile\\download", 30);
    	String ss ="222.txt";
    	System.out.println(ss.substring(ss.lastIndexOf(".")));
    }
}
