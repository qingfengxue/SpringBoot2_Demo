﻿$(function(){
	initData();
});

function initData(){
	console.log(projectName);
	console.log(projectPath);
	console.log(urlParams.get("directoryName"));
	
	var directoryName = urlParams.get("directoryName");
	directoryName = null == directoryName ? "" : directoryName;
	
	getDirectoryList(directoryName);
	getFileList(directoryName);
}


/* 获取目录信息 */
function getDirectoryList(directoryName){
	$.ajax({
	    method: "get",
	    url: "preview/directoryList?directoryName=" + encodeURIComponent(directoryName),
	    dataType: 'json',
	    success: function (obj) {
	    	if (obj.code == 1000) {
	    		var files = obj.data;
	    		var html = "";
	    		for (var i = 0; i < files.length; i++) {
	    			generateLink(files[i]);
				}
	    		$('#directoryDisplay').append("<br/>")
			} else {
				$("#directoryDisplay").html(obj.message);
			}
	    },
	    error: function(err) {
	    	$("#directoryDisplay").html(err);
	    }
	});
}

/* 获取文件信息 */
function getFileList(directoryName){
	$.ajax({
		method: "get",
		url: "preview/fileList?directoryName=" + encodeURIComponent(directoryName),
		dataType: 'json',
		success: function (obj) {
			if (obj.code == 1000) {
				var files = obj.data;
				var html = "";
				for (var i = 0; i < files.length; i++) {
					generateAudio(files[i]);
				}
			} else {
				$("#fileDisplay").html(obj.message);
			}
		},
		error: function(err) {
			$("#fileDisplay").html(err);
		}
	});
}


//生成链接并添加到页面中
function generateLink(file) {
    $('<a/>', {
        text: file.fileName,
        href: projectPath + "?directoryName=" + encodeURIComponent(file.filePath)
    }).appendTo('#directoryDisplay').after("<br/>");
}

// 生成音频播放器并添加到页面中
function generateAudio(file) {
    $('<div/>').addClass('center-content').append(
        $('<audio/>', {
            controls: true,
            autoplay: true,
            src: file.filePath,
            text: file.fileName
        }),
        file.fileName.replace(file.fileSuffix, "")
    ).appendTo('#fileDisplay');
}