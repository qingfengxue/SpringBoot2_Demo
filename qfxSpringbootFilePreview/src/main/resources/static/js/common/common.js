﻿var projectName;
var rootPath;
var projectPath;
var urlParams;

$(function(){
	getRootPath();
	getProjectName();
	projecPath();
	getParams();
});

/* 获取系统地址及端口号 */
function getRootPath(){
	// 如:http://192.168.1.196:8090
	rootPath = window.location.protocol + '//' + window.location.host;
}

//获取项目路径,最后带"/",如:'/项目名/'
function getProjectName() {
	// 获取路径
	var pathName = window.document.location.pathname;
	// 截取，得到项目名称
	projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 2);
}

/* 获取系统地址+端口号+项目名 */
function projecPath(){
	// 如:http://192.168.1.196:8090/test/
	projectPath = rootPath + projectName;
}

//获取URL中指定名称的参数集合
function getParams() {
  // 获取完整URL
  var url = window.location.href;
  // 创建URL对象
  var urlObj = new URL(url);
  // 获取URL中的查询参数
  urlParams = urlObj.searchParams;
//  // 获取directoryName参数的值
//  return searchParams.get(paramName);
}