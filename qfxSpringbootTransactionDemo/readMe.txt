1.springboot的版本是2.6.10
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.测试事物是否生效
	经测试确认:调用带有@Transactional注解的方法-事物生效
			调用带有@Transactional注解的方法,方法内部调用类中其他方法-事物生效
			调用不带有@Transactional注解的方法,方法内部调用类中其他带有@Transactional注解的方法-事物不生效
5.测试地址
	http://localhost/sys/role/add		// 事物生效,保持了数据一致
	http://localhost/sys/role/addExt	// 事物未生效,表中数据出现了偏差,sys_role_user保存成功,sys_role保存失败
	