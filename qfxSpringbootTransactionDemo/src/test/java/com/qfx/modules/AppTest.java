package com.qfx.modules;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.qfx.RunApp;
import com.qfx.modules.system.entity.SysRole;
import com.qfx.modules.system.service.SysRoleSer;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RunApp.class})
class AppTest {
	
	@Autowired
	SysRoleSer sysRoleSer;

	@Test
	void contextLoads() {
		System.out.println(111);
		SysRole sysRole = new SysRole();
		sysRole.setRoleId("2001004");;
		sysRole.setRoleName("测试用户");
		sysRole.setState(1);
		
		sysRoleSer.addSysRole(sysRole);
		System.out.println(2222);
	}

}
