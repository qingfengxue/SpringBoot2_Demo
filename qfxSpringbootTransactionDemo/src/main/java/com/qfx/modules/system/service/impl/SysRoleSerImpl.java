package com.qfx.modules.system.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qfx.modules.system.dao.SysRoleDao;
import com.qfx.modules.system.dao.SysRoleUserDao;
import com.qfx.modules.system.entity.SysRole;
import com.qfx.modules.system.entity.SysRoleUser;
import com.qfx.modules.system.service.SysRoleSer;

@Service
public class SysRoleSerImpl implements SysRoleSer {
	@Autowired
	SysRoleDao sysRoleMapper;
	@Autowired
	SysRoleUserDao sysRoleUserDao;

	/**
	 * 事物生效,SysRole发生异常致使数据信息回滚
	 */
	@Override
	@Transactional
	public void addSysRole(SysRole sysRole) {
		addSysRoleUser(sysRole.getRoleId());
		int insertCount = sysRoleMapper.insert(sysRole);
		System.out.println(insertCount);
	}
	
	/**
	 * 事物不生效sysRoleUser可以保存成功,SysRole发生异常保存失败
	 */
	@Override
	public void addSysRoleExt(SysRole sysRole) {
		addSysRole(sysRole);
	}
	
	private void addSysRoleUser(String roleId) {
		SysRoleUser sysRoleUser = new SysRoleUser();
		sysRoleUser.setId(UUID.randomUUID().toString());
//		sysRoleUser.setRoleId(roleId);
		sysRoleUser.setRoleId("111");
		sysRoleUser.setUserId("ppp");
		sysRoleUserDao.insert(sysRoleUser);
	}
	
	
}
