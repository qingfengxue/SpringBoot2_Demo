package com.qfx.modules.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.modules.system.entity.SysRole;
import com.qfx.modules.system.service.SysRoleSer;

@RestController
@RequestMapping("sys/role")
public class SysRoleCtl {
	@Autowired
	SysRoleSer sysRoleSer;
	
	@RequestMapping("add")
	public SysRole addSysRole(SysRole sysRole) {
		sysRoleSer.addSysRole(sysRole);
		return sysRole;
	}
	
	@RequestMapping("addExt")
	public SysRole addSysRoleExt(SysRole sysRole) {
		sysRoleSer.addSysRoleExt(sysRole);
		return sysRole;
	}
}
