package com.qfx.modules.system.dao;

import com.qfx.modules.system.entity.SysRoleUser;

public interface SysRoleUserDao {
    int deleteByPrimaryKey(String id);

    int insert(SysRoleUser record);

    int insertSelective(SysRoleUser record);

    SysRoleUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRoleUser record);

    int updateByPrimaryKey(SysRoleUser record);
}