package com.qfx.modules.system.service;

import com.qfx.modules.system.entity.SysRole;

public interface SysRoleSer {
	/**
	 * <h5>功能:新增一个角色信息</h5>
	 * 
	 * @param sysRole
	 */
	void addSysRole(SysRole sysRole);
	
	/**
	 * <h5>功能:新增一个角色信息</h5>
	 * 
	 * @param sysRole
	 */
	void addSysRoleExt(SysRole sysRole);
}
