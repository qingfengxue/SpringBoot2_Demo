package com.qfx.common.annotation.impl;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * <h5>描述:通过@Aspect注解使该类成为切面类</h5>
 *  解析注解：
 *  通过@Pointcut 指定切入点 ，这里指定的切入点为TestAop注解类型，也就是被@TestAop注解修饰的方法，进入该切入点。
 * @Before 前置通知：在某连接点之前执行的通知，但这个通知不能阻止连接点之前的执行流程（除非它抛出一个异常）。
 * @Around 环绕通知：可以实现方法执行前后操作，需要在方法内执行point.proceed(); 并返回结果。
 * @AfterReturning 后置通知：在某连接点正常完成后执行的通知：例如，一个方法没有抛出任何异常，正常返回。
 * @AfterThrowing 异常通知：在方法抛出异常退出时执行的通知。
 * @After 后置通知：在某连接点正常完成后执行的通知：例如，一个方法没有抛出任何异常，正常返回。
 */
@Aspect	//使该类成为切面类
@Component
public class LoginAnnoImpl {

	@Pointcut("@annotation(com.qfx.common.annotation.LoginAnno)")
	private void cut() {
		
	}
	
	/**
	 * <h5>功能:前置通知</h5>
	 */
	@Before("cut()")
	public void before() {
		System.out.println("自定义注解生效了");
	}
}
