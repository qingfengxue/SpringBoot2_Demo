package com.qfx.common.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.common.annotation.LoginAnno;

@RestController
@RequestMapping("login")
public class LoginController {
	
	@RequestMapping("reg")
	public String reg(String userName) {
		return "用户[" + userName +"]注册成功~!";
	}

	@RequestMapping("login")
	@LoginAnno
	public String login(String userName) {
		return "欢迎您:" + userName;
	}
}
