package com.qfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h5>描述:添加外部tomcat支持,需要继承SpringBootServletInitializer,并重写configure方法</h5>
 * 
 * @author zhangpj	2018年9月19日 
 */

@SpringBootApplication
public class RunApp {
	
	public static void main(String[] args) {
		// 整个程序入口,启动springboot项目,创建内置tomcat服务器,使用tomct加载springmvc注解启动类
		SpringApplication.run(RunApp.class, args);
	}
}
