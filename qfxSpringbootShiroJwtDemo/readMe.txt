1.springboot的版本是2.4.0
2.添加或修改项目内容后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.引入shiro与jwt支持
	1.pom.xml
		<!-- 9.开启shiro依赖 -->
		<dependency>
		    <groupId>org.apache.shiro</groupId>
		    <artifactId>shiro-spring-boot-starter</artifactId>
		    <version>1.7.0</version>
		</dependency>
		
		<!-- 10.引入jjwt依赖 -->
		<dependency>
		    <groupId>io.jsonwebtoken</groupId>
		    <artifactId>jjwt</artifactId>
		    <version>0.9.1</version>
		</dependency>
		
	2.shiro相关类
		整合jwt必须文件
			com.qfx.demo.shiro.JwtRealm                 -- 自定义jwt的realm
			com.qfx.demo.shiro.CustomJwtFilter.java     -- 自定义jwt过滤器
			com.qfx.demo.shiro.JwtToken.java            -- 自定义token实体类
			com.qfx.demo.shiro.ShiroConfig.java         -- shiro核心配置文件
		整合jwt可选文件,开启权限验证支持
			com.qfx.demo.shiro.CustomRolesFilter.jva    -- 自定义权限过滤器拦截控制
			
			注意:
				开启权限验证支持,需在ShiroConfig.shiroFilter方法中添加对应的过滤器信息(79-80行),
				并在权限授权的地方启用自定义的权限过滤器(100-107行)
			
4.基础数据
	因为是测试使用,所以未连接数据库,是写死在缓存里面的
		com.qfx.demo.cache.MenuRoleCache.java -- 权限缓存
		com.qfx.demo.cache.RoleCache.java -- 角色缓存
		com.qfx.demo.cache.UserCache.java -- 用户缓存
		
5.支持动态刷新权限(无需重启)
	com.qfx.demo.shiro.ShiroPermissionSer.java
	
	注意：
		如果开启权限验证则需添加权限过滤器(61-79行)

6.启动:
	1.直接运行RunApp.java中的main方法即可

7.测试地址
	登录 http://localhost:8081/qfxSpringbootShiroJwtDemo/login/login?userName=zhangsan&passWord=111111
	获取用户列表 http://localhost:8081/qfxSpringbootShiroJwtDemo/user/list
	动态刷新权限 http://localhost:8081/qfxSpringbootShiroJwtDemo/roleMenu/initChain
	