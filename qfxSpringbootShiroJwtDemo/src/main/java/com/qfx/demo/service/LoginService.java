package com.qfx.demo.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.qfx.demo.cache.UserCache;
import com.qfx.demo.util.ToolToken;
import com.qfx.demo.vo.MessageBean;
import com.qfx.demo.vo.SysUser;

@Service
public class LoginService {

	/**
	 * @功能描述：shiro+jwt登录验证,通过返回true,失败返回false
	 *
	 * @param request
	 * @param sysUser
	 * @return
	 */
	public MessageBean loginJwt(SysUser sysUser, HttpServletResponse response) {
		String message = "登录成功!";
		MessageBean messageBean = new MessageBean();

		System.out.println("用户登录:userName[" + sysUser.getUserName() + "],userPass[" + sysUser.getPassWord() + "]");

		// 获取用户信息
		SysUser user = UserCache.getUserCacheMap(sysUser.getUserName());
		if (ObjectUtils.isEmpty(user)) {
			message = "用户不存在!";
			messageBean.setMessage(message);
			System.out.println("用户[" + sysUser.getUserName() + "]进行登录验证失败,失败原因[" + message + "]");

			return messageBean;
		}

		if (!sysUser.getPassWord().equals(user.getPassWord())) {
			message = "密码错误!";
			messageBean.setMessage(message);
			System.out.println("用户[" + sysUser.getUserName() + "]进行登录验证失败,失败原因[" + message + "]");

			return messageBean;
		}

		// 获取系统时间戳
		long currentTimeMillis = System.currentTimeMillis();

		// 生成Token:userName + 时间戳 + salt
		String tokenStr = ToolToken.createJwtToken(sysUser.getUserName() + currentTimeMillis, sysUser.getUserName());
		if (ObjectUtils.isEmpty(tokenStr)) {
			message = "生成token发生错误!";
			messageBean.setMessage(message);
			System.out.println("用户[" + sysUser.getUserName() + "]进行登录验证失败,失败原因[" + message + "]");
			return messageBean;
		}

		System.out.println("用户[" + sysUser.getUserName() + "]登录认证通过");
		messageBean.setData(tokenStr);
		messageBean.setMessage(message);

		// 将 Token 放入 Response Header 中
		response.addHeader("Authorization", tokenStr);

		return messageBean;
	}
}
