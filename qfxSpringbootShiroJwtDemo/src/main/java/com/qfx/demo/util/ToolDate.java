package com.qfx.demo.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *  时间工具
 * @author
 */
public final class ToolDate {
	
	/**
	 * <h5>功能:字符串转日期</h5>
	 * <p>格式:yyyy-MM-dd</p>
	 * 
	 * @param dateStr
	 * @return Date
	 */
	public static Date stringToDate(String dateStr) {
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		Date date = null; 
		try {
			date = formatter.parse(dateStr);
		} catch (ParseException e) {
			System.err.println("字符串转时间发生错误" + e.getMessage());
			e.printStackTrace();
		}
		
		return date; 
	}
	
	/**
	 * <h5>功能:字符串转日期时间</h5>
	 * <p>格式:yyyy-MM-dd HH:mm:ss</p>
	 * 
	 * @param dateStr
	 * @return Date
	 */
	public static Date stringToDateTime(String dateStr) {
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null; 
	    try {
	    	date = formatter.parse(dateStr);
		} catch (ParseException e) {
			System.err.println("字符串转时间发生错误" + e.getMessage());
			e.printStackTrace();
		}
	    
	    return date; 
	}
	
	/**
	 * <h5>功能:日期转字符串(不补零)</h5>
	 * <p>格式:yyyy-MM-dd</p>
	 *
	 * @param date
	 * @return 返回日期
	 */
	public static String dateToString(Date date) {
		DateFormat formatter = DateFormat.getDateInstance();
		return formatter.format(date);
	}
	
	/**
	 * <h5>功能:日期转字符串(补零)</h5>
	 * <p>格式:yyyy-MM-dd</p>
	 *
	 * @param date
	 * @return 返回日期
	 */
	public static String dateToStringExt(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
	}
	
	/**
	 * <h5>功能:时间转字符串(不补零)</h5>
	 * <p>格式:HH:mm:ss</p>
	 *
	 * @param date
	 * @return 返回时间
	 */
	public static String timeToString(Date date) {
		DateFormat formatter = DateFormat.getTimeInstance();
		return formatter.format(date);
	}
	
	/**
	 * <h5>功能:时间转字符串(补零)</h5>
	 * <p>格式:HH:mm:ss</p>
	 *
	 * @param date
	 * @return 返回时间
	 */
	public static String timeToStringExt(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(date);
	}
	
	/**
	 * <h5>功能:返回日期时间中的日期</h5>
	 * <p>格式:yyyy-MM-dd,不足10的月份或天数前面补零</p>
	 *
	 * @param startDate
	 * @return 返回日期
	 */
	public static String dateToString(String startDate) {
		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyy-M-d H:m:s");
		DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDateTime.parse(startDate, inputFormat).format(outputFormat);
	}
	
	/**
	 * <h5>功能:日期时间转字符串(不补零)</h5>
	 * <p>格式:yyyy-MM-dd HH:mm:ss</p>
	 *
	 * @param date
	 * @return 
	 */
	public static String dateTimeToString(Date date) {
		DateFormat formatter = DateFormat.getDateTimeInstance();
        return formatter.format(date);
	}
	
	/**
	 * <h5>功能:日期时间转字符串(补零)</h5>
	 * <p>格式:yyyy-MM-dd HH:mm:ss</p>
	 *
	 * @param date
	 * @return 
	 */
	public static String dateTimeToStringExt(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
	}
	
	/**
	 * <h5>功能:获得当前日期</h5>
	 * <p>格式:yyyy-MM-dd</p>
	 *
	 * @return String
	 */
	public static String getNowDate() {
		return getNowDateTime("yyyy-MM-dd");
	}
	
	/**
	 * <h5>功能:获得当前时间</h5>
	 * <p>格式:HH:mm:ss</p>
	 *
	 * @return String
	 */
	public static String getNowTime() {
		return getNowDateTime("HH:mm:ss");
	}
	
    /**
     * <h5>功能:获得当前日期时间</h5>
     * <p>格式:yyyy-MM-dd HH:mm:ss</p>
     *
     * @return String
     */
    public static String getNowDateTime() {
        // 精确到秒
        return getNowDateTime("yyyy-MM-dd HH:mm:ss");
    }
    
    /**
     * <h5>功能:获取当前日期时间</h5>
     *
     * @param format 日期格式
     * @return 
     */
    public static String getNowDateTime(String format) {
    	Date nowday = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat(format);// 精确到秒
    	String time = sdf.format(nowday);
    	
    	return time;
    }
    
	/**
	 * <h5>功能:获得当前日期(时间清零)</h5>
	 * <p>格式:yyyy-MM-dd 00:00:00</p>
	 *
	 * @return Date
	 */
	public static Date getNowDateExt() {
		return getNowDateExt(new Date());
	}
	
	/**
	 * <h5>功能:获得当前日期(时间清零)</h5>
	 * <p>格式:yyyy-MM-dd 00:00:00</p>
	 *
	 * @return Date
	 */
	public static Date getNowDateExt(Date date) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		// 将时分秒,毫秒域清零
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);
		
		return cal1.getTime();
	}
    
    /**
     * <h5>功能:获得当前时间</h5>
     * <p>精确到秒,格式:yyyyMMddHHmmss</p>
     *
     * @return String
     */
    public static String getNowTimeToSec() {
    	Date nowday = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");// 精确到毫秒
    	String time = sdf.format(nowday);
    	
    	return time;
    }
    
    /**
     * <h5>功能:获得当前时间</h5>
     * <p>精确到毫秒,格式:yyyyMMddHHmmssSSS</p>
     *
     * @return String
     */
    public static String getNowTimeToMS() {
    	Date nowday = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");// 精确到毫秒
    	String time = sdf.format(nowday);
    	
    	return time;
    }

    /**
     * <h5>功能:获取当前系统时间戳</h5>
     * <p>精确到毫秒</p>
     *
     * @return 
     */
    public static Long getTimeStampToMS() {
        return System.currentTimeMillis();
    }

    /**
     * <h5>功能:获取当前系统时间戳</h5>
     * <p>精确到秒</p>
     *
     * @return 
     */
    public static Long getTimeStampToSec() {
        return getTimeStampToMS() / 1000;
    }
    
    /**
     * <h5>功能:时间字符转成时间戳</h5>
     * <p>精确到毫秒</p>
     *
     * @param timeStr 格式:yyyy-MM-dd HH:mm:ss
     * @return Long
     * @throws ParseException 
     */
    public static Long getTimeStampToMS(String timeStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 精确到秒
        Date date = sdf.parse(timeStr);
        
        return date.getTime();
    }
    
    /**
     * <h5>功能:时间字符转成时间戳</h5>
     * <p>精确到秒</p>
     *
     * @param timeStr 格式:yyyy-MM-dd HH:mm:ss
     * @return Long
     * @throws ParseException 
     */
    public static Long getTimeStampToSec(String timeStr) throws ParseException {
    	Long timeStamp = getTimeStampToMS(timeStr);
    	return timeStamp / 1000;
    }

    /**
     * <h5>功能:Unix时间戳转成时间字符</h5>
     *
     * @param timestamp
     * @return 格式:yyyy-MM-dd HH:mm:ss
     */
    public static String getTimeByTimeStamp(String timestamp) {
        Long timeLong = Long.parseLong(timestamp);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//要转换的时间格式,精确到秒
	    Date date;
		try {
			date = sdf.parse(sdf.format(timeLong * 1000));
			return sdf.format(date);
		} catch (ParseException e) {
			date = new Date(0);
			return sdf.format(date);
		}
    }
    
    /**
     * <h5>功能:Unix时间戳转Date</h5>
     *
     * @param timestamp
     * @return 格式:yyyy-MM-dd HH:mm:ss
     */
    public static Date getDateByTimeStamp(String timestamp) {
    	Long timeLong = Long.parseLong(timestamp);
    	return new Date(timeLong);
    }
    
	/**
	 * <h5>功能:按指定格式格式化日期</h5>
	 * 
	 * @param dt 要格式化的日期
	 * @param format 指定格式,format为null或者""则默认格式"yyyy-MM-dd"
	 * @return 返回格式化后的字符串型日期:dt为null则返回''，返回format格式字符串
	 */
	public static String formatDate(Date dt, String format) {
		String temp;
		if (dt == null)
			return "";
		if ((format == null) || (format.equals(""))) {
			temp = "yyyy-MM-dd";
		} else
			temp = format;
		SimpleDateFormat formatter = new SimpleDateFormat(temp);
		String s = formatter.format(dt);
		return s;
	}

    /**
     * <h5>功能:获取半年(6个月)后的当前时间</h5>
     * <p>格式:yyyy-MM-dd HH:mm:ss</p>
     *
     * @return 
     */
    public static String getHalfYearLaterTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 精确到秒

        Calendar calendar = Calendar.getInstance();
        int currMonth = calendar.get(Calendar.MONTH) + 1;

        if (currMonth >= 1 && currMonth <= 6) {
            calendar.add(Calendar.MONTH, 6);
        } else {
            calendar.add(Calendar.YEAR, 1);
            calendar.set(Calendar.MONTH, currMonth - 6 - 1);
        }

        return sdf.format(calendar.getTime());
    }
    
	/**
	 * <h5>功能:获取两个时间相差的秒数</h5>
	 * <p>格式:：yyyy-MM-dd HH:mm:ss</p>
	 *
	 * @param str1
	 * @param str2
	 * @return 
	 */
	public static int getDistanceSecond(String str1, String str2) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date one;
		Date two;
		long diff;
		long min = 0;
		try {
			one = df.parse(str1);
			two = df.parse(str2);
			long time1 = one.getTime();
			long time2 = two.getTime();
			diff = time1 - time2;
			min = diff / 1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return (int) Math.abs(min);
	}

	/**
	 * @功能描述：当前日期和指定日期的相差天数
	 * 指定时间大于当前日期时间返回正数;指定时间等于当前日期时间返回0;指定时间小于当前时间返回负数
	 * 
	 * @作者：zhangpj		@创建时间：2015年1月16日
	 * @param startDate	"yyyy-MM-dd"和"yyyy-MM-dd HH:mm:ss" 格式均可
	 * @return
	 *
	 * @修改描述：	
	 *
	 * @修改者：			@修改时间：
	 */
	public static long getDateIntevalDays(String startDate) {
		//获取当前日期
		Timestamp time = new Timestamp(System.currentTimeMillis());
		String nowTime = formatDate(time, null);
		try {
			// 指定日期
			SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
			
			Date nowDate = myFormatter.parse(nowTime);
			Date theDate = myFormatter.parse(startDate);
			// 两个时间之间的天数
			long days=(theDate.getTime() - nowDate.getTime())/(24*60*60*1000);
			return days;
		} catch (Exception ee) {
			return 0l;
		}
	}

	/**
	 * @功能描述：	计算两个日期之间的间隔天数
	 * endDate大于startDate返回正数;endDate等于startDate返回0;endDate小于startDate返回负数;
	 *
	 * @作者：zhangpj		@创建时间：2015年1月16日
	 * @param startDate
	 * @param endDate
	 * @return 间隔的天数
	 *
	 * @修改描述：	
	 *
	 * @修改者：			@修改时间：
	 */
	public static long getDateIntevalDays(Date startDate, Date endDate) {
		long days = 0;
		long st = startDate.getTime();
		long en = endDate.getTime();
		long times = en - st;
		days = times / (24 * 3600 * 1000);
		return days;
	}
	
	/**
	 * @功能描述：	计算两个日期之间的间隔天数
	 * endDate大于startDate返回正数;endDate等于startDate返回0;endDate小于startDate返回负数;
	 * 
	 * @作者：zhangpj		@创建时间：2015年1月16日
	 * @param beginDateStr
	 * @param endDateStr
	 * @return 间隔的天数
	 *
	 * @修改描述：	
	 *
	 * @修改者：			@修改时间：
	 */
	public static long getDateIntevalDays(String beginDateStr,String endDateStr)
    {
        long day=0;
        SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");    
        Date beginDate;
        Date endDate;
        try
        {
		    beginDate = format.parse(beginDateStr);
		    endDate= format.parse(endDateStr);    
		    day=(endDate.getTime()-beginDate.getTime())/(24*60*60*1000);     
        } catch (ParseException e)
        {
        	e.printStackTrace();
        }   
        return day;
	}
	
    /** 
     * 两个时间相差距离多少天多少小时多少分多少秒 
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00 
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00 
     * @return long[] 返回值为：{天, 时, 分, 秒} 
     */  
	/**
	 * @功能描述：	计算两个日期之间的间隔时间(天、小时、分、秒)
	 *
	 * @作者：zhangpj		@创建时间：2022年07月08日
	 * @param starttime
	 * @param endtime
	 * @return
	 *
	 */
    public static long[] getDistanceTimes(String starttime, String endtime) {  
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        Date one;  
        Date two;  
        long day = 0;  
        long hour = 0;  
        long min = 0;  
        long sec = 0;  
        try {  
            one = df.parse(starttime);  
            two = df.parse(endtime);  
            long time1 = one.getTime();  
            long time2 = two.getTime();  
            long diff ;  
            if(time1<time2) {  
                diff = time2 - time1;  
            } else {  
                diff = time1 - time2;  
            }  
            day = diff / (24 * 60 * 60 * 1000);  
            hour = (diff / (60 * 60 * 1000) - day * 24);  
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);  
            sec = (diff/1000-day*24*60*60-hour*60*60-min*60);  
        } catch (ParseException e) {  
            e.printStackTrace();  
        }  
        long[] times = {day, hour, min, sec};  
        return times;  
    }  

	/**
	 * <h5>功能:获得当前日期是周几</h5>
	 * 
	 * @return 周一(1)、周二(2)...周日(7)
	 */
	public static int getDayOfWeek() {
        return getDayOfWeek(new Date());
	}
	
	/**
	 * <h5>功能:获得指定时间是周几</h5>
	 *
	 * @param date
	 * @return 周一(1)、周二(2)...周日(7)
	 */
	public static int getDayOfWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int day = cal.get(Calendar.DAY_OF_WEEK) - 1;
		
		return day == 0 ? 7 : day;
	}
	
	/**
	 * <h5>功能:获得指定时间是周几</h5>
	 * <p>格式:yyyy-MM-dd [HH:mm:ss]</p>
	 * 
	 * @param dateStr
	 * @return 周一(1)、周二(2)...周日(7)
	 */
	public static int getDayOfWeek(String dateStr) {
		Date date = stringToDate(dateStr);
		
		return getDayOfWeek(date);
	}
	
    /**
     * <h5>功能:增加年</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     *
     * @param date
     * @param amount
     * @return 
     */
    public static Date addYears(Date date, int amount){
        return addDateTime(date, amount, Calendar.YEAR);
    }
    
    /**
     * <h5>功能:增加月</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     *
     * @param date
     * @param amount
     * @return 
     */
    public static Date addMonths(Date date, int amount){
    	return addDateTime(date, amount, Calendar.MONTH);
    }
    
    /**
     * <h5>功能:当前日期增加天</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     * 
     * @param amount
     * @return 
     */
    public static String addDays(int amount) {
    	return dateToString(addDays(new Date(), amount));
    }
    
    /**
     * <h5>功能:增加天</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     *
     * @param date
     * @param amount
     * @return 
     */
    public static Date addDays(Date date, int amount){
    	return addDateTime(date, amount, Calendar.DATE);
    }
    
    /**
     * <h5>功能:增加小时</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     *
     * @param date
     * @param amount
     * @return 
     */
    public static Date addHours(Date date, int amount){
    	return addDateTime(date, amount, Calendar.HOUR);
    }
    
    /**
     * <h5>功能:增加分钟</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     *
     * @param date
     * @param amount
     * @return 
     */
    public static Date addMinute(Date date, int amount){
    	return addDateTime(date, amount, Calendar.MINUTE);
    }
    
    /**
     * <h5>功能:增加秒</h5>
     * <p>当前时间往后用正数,点前时间往前用负数</p>
     *
     * @param date
     * @param amount
     * @return 
     */
    public static Date addSeconds(Date date, int amount){
    	return addDateTime(date, amount, Calendar.SECOND);
    }
	
	/**
	 * <h5>功能:日期计算</h5>
	 *
	 * @param date 基准日期
	 * @param amount 增加的数量
	 * @param field 增加的单位:年(Calendar.YEAR)、月(Calendar.MONTH)、 日(Calendar.DATE)、时(Calendar.HOUR)、分(Calendar.MINUTE)、秒(Calendar.SECOND)、其他
	 * @return 增加以后的日期
	 */
	public static Date addDateTime(Date date, int amount, int field) {
        Calendar calendar = Calendar.getInstance();
 
        calendar.setTime(date);
        calendar.add(field, amount);
 
        return calendar.getTime();
    }
	
	/**
	 * <h5>功能:判断是否是闰年</h5>
	 * 
	 * @param year 年份
	 * @return boolean true是闰年
	 */
	public static boolean isLeapYear(int year) {
		boolean leapyear = false;
		if ((year % 4 == 0 && (year % 100) != 0) || (year % 400 == 0)) {
			leapyear = true;
		}
		return leapyear;
	}
	
	/**
	 * <h5>功能:判断指定月份的最大天数</h5>
	 * 
	 * @param year
	 * @param month
	 * @return 
	 */
	public static int getMonthDays(int year, int month) {
		int days = 0;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			days = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			days = 30;
			break;
		case 2:
			if (isLeapYear(year)) {
				days = 29;
			} else {
				days = 28;
			}
			break;
		}
		return days;
	}
	
	/**
	 * <h5>功能:取星期值</h5>
	 * 
	 * @param dt
	 * @return 星期天为1,...星期六为7
	 */
	public static int getDayofWeek(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * <h5>功能:取星期值</h5>
	 * 
	 * @param dt
	 * @return 星期天 ,...星期六
	 */
	public static String getDayofWeekStr(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int week = cal.get(Calendar.DAY_OF_WEEK);
		String rslt;
		switch (week) {
		case 1:
			rslt = "星期天";
			break;
		case 2:
			rslt = "星期一";
			break;
		case 3:
			rslt = "星期二";
			break;
		case 4:
			rslt = "星期三";
			break;
		case 5:
			rslt = "星期四";
			break;
		case 6:
			rslt = "星期五";
			break;
		case 7:
			rslt = "星期六";
			break;
		default:
			rslt = "星期六";
			break;
		}
		return rslt;
	}
	
	/**
	 * <h5>功能:获取当前周的周一在月份中的日期</h5>
	 * 
	 * @return 
	 */
	public static String getCurrentWeekEndDate() {
		String weekEndDateString = "";
		try {
			Calendar cl = Calendar.getInstance(Locale.CHINA);
			int dayOfWeek = cl.get(Calendar.DAY_OF_WEEK);
			int daySpace = (7 - dayOfWeek);
			if (daySpace == 6) {
				daySpace = 0;
			} else {
				daySpace += 1;
			}
			cl.add(Calendar.DAY_OF_MONTH, daySpace);
			int year = cl.get(Calendar.YEAR);
			int month = cl.get(Calendar.MONTH) + 1;
			int day = cl.get(Calendar.DAY_OF_MONTH);
			weekEndDateString = year + "-" + (month < 10 ? "0" + month : month)
					+ "-" + (day < 10 ? "0" + day : day) + " " + "23:59:59";
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return weekEndDateString;
	}

	/**
	 * <h5>功能:获取当前周的周一在月份中的日期</h5>
	 * 
	 * @param format
	 * @return 
	 */
	public static String getCurrentWeekEndDate(String format) {
		String weekEndDateString = "";
		try {
			Calendar cl = Calendar.getInstance(Locale.CHINA);
			int dayOfWeek = cl.get(Calendar.DAY_OF_WEEK);
			int daySpace = (7 - dayOfWeek);
			if (daySpace == 6) {
				daySpace = 0;
			} else {
				daySpace += 1;
			}
			cl.add(Calendar.DAY_OF_MONTH, daySpace);
			int year = cl.get(Calendar.YEAR);
			int month = cl.get(Calendar.MONTH) + 1;
			int day = cl.get(Calendar.DAY_OF_MONTH);
			weekEndDateString = year + "-" + (month < 10 ? "0" + month : month)
					+ "-" + (day < 10 ? "0" + day : day) + " " + "23:59:59";
			weekEndDateString = formatDate(new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").parse(weekEndDateString), format);
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return weekEndDateString;
	}

	/**
	 * <h5>功能:获取当前周的周末在月份中的日期</h5>
	 * 
	 * @return 
	 */
	public static String getCurrentWeekStartDate() {
		String weekStartDateString = "";
		try {
			Calendar cl = Calendar.getInstance(Locale.CHINA);
			int dayOfWeek = cl.get(Calendar.DAY_OF_WEEK);
			int daySpace = dayOfWeek - 2;
			if (daySpace == -1) {
				daySpace = -6;
			} else {
				daySpace = daySpace - 2 * daySpace;
			}
			cl.add(Calendar.DAY_OF_MONTH, daySpace);
			int year = cl.get(Calendar.YEAR);
			int month = cl.get(Calendar.MONTH) + 1;
			int day = cl.get(Calendar.DAY_OF_MONTH);
			weekStartDateString = year + "-"
					+ (month < 10 ? "0" + month : month) + "-"
					+ (day < 10 ? "0" + day : day) + " " + "00:00:00";
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return weekStartDateString;
	}

	/**
	 * <h5>功能:获取当前周的周末在月份中的日期</h5>
	 * 
	 * @param format
	 * @return 
	 */
	public static String getCurrentWeekStartDate(String format) {
		String weekStartDateString = "";
		try {
			Calendar cl = Calendar.getInstance(Locale.CHINA);
			int dayOfWeek = cl.get(Calendar.DAY_OF_WEEK);
			int daySpace = dayOfWeek - 2;
			if (daySpace == -1) {
				daySpace = -6;
			} else {
				daySpace = daySpace - 2 * daySpace;
			}
			cl.add(Calendar.DAY_OF_MONTH, daySpace);
			int year = cl.get(Calendar.YEAR);
			int month = cl.get(Calendar.MONTH) + 1;
			int day = cl.get(Calendar.DAY_OF_MONTH);
			weekStartDateString = year + "-"
					+ (month < 10 ? "0" + month : month) + "-"
					+ (day < 10 ? "0" + day : day) + " " + "00:00:00";
			weekStartDateString = formatDate(new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").parse(weekStartDateString), format);
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return weekStartDateString;
	}

	/**
	 * <h5>功能:当月开始的时间</h5>
	 * 
	 * @return 
	 */
	public static String getCurrentMonthStartDate() {
		String monthStartDateString = "";
		try {
			Calendar cl = Calendar.getInstance(Locale.CHINA);
			int year = cl.get(Calendar.YEAR);
			int month = cl.get(Calendar.MONTH) + 1;
			monthStartDateString = year + "-"
					+ (month < 10 ? "0" + month : month) + "-" + "01 00:00:00";
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return monthStartDateString;
	}

	/**
	 * <h5>功能:当月开始的时间</h5>
	 * 
	 * @param format
	 * @return 
	 */
	public static String getCurrentMonthStartDate(String format) {
		String monthStartDateString = "";
		try {
			Calendar cl = Calendar.getInstance(Locale.CHINA);
			int year = cl.get(Calendar.YEAR);
			int month = cl.get(Calendar.MONTH) + 1;
			monthStartDateString = year + "-"
					+ (month < 10 ? "0" + month : month) + "-" + "01 00:00:00";
			monthStartDateString = formatDate(new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").parse(monthStartDateString), format);
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return monthStartDateString;
	}

	/**
	 * <h5>功能:当月结束的时间</h5>
	 * 
	 * @return 
	 */
	public static String getCurrentMonthEndDate() {
		String monthEndDateString = "";
		Calendar cl = Calendar.getInstance(Locale.CHINA);
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH) + 1;
		monthEndDateString = year
				+ "-"
				+ (month < 10 ? "0" + month : month)
				+ "-"
				+ (getMonthDays(year, month) < 10 ? "0"
						+ getMonthDays(year, month) : getMonthDays(year, month))
				+ " 23:59:59";
		return monthEndDateString;
	}

	/**
	 * <h5>功能:当月结束的时间</h5>
	 * 
	 * @param format
	 * @return 
	 */
	public static String getCurrentMonthEndDate(String format) {
		String monthEndDateString = "";
		Calendar cl = Calendar.getInstance(Locale.CHINA);
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH) + 1;
		monthEndDateString = year
				+ "-"
				+ (month < 10 ? "0" + month : month)
				+ "-"
				+ (getMonthDays(year, month) < 10 ? "0"
						+ getMonthDays(year, month) : getMonthDays(year, month))
				+ " 23:59:59";
		try {
			monthEndDateString = formatDate(new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").parse(monthEndDateString), format);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return monthEndDateString;
	}
	
	/**
	 * @功能描述：	获取指定时间段的日期
	 *
	 * @param dBegin
	 * @param dEnd
	 * @return 
	 *
	 * @作者：zhangpj		@创建时间：2018年3月29日
	 */
	public static List<String> findDates(Date dBegin, Date dEnd) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<String> lDate = new ArrayList<String>();
		lDate.add(sdf.format(dBegin));
		Calendar calBegin = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calBegin.setTime(dBegin);
		Calendar calEnd = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calEnd.setTime(dEnd);
		// 测试此日期是否在指定日期之后
		while (dEnd.after(calBegin.getTime())) {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
			calBegin.add(Calendar.DAY_OF_MONTH, 1);
			lDate.add(sdf.format(calBegin.getTime()));
		}
		return lDate;
	}
	
	/**
	 * <h5>功能:获取当前月第一天</h5>
	 * <p>格式:：yyyy-MM-dd</p>
	 * 
	 * @return
	*/
	public static Date getFirstDayOfMonth() {
		LocalDate onenext = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
		return stringToDate(onenext.toString());
	}
	
	/**
	 * <h5>功能:获取下个月第一天</h5>
	 * <p>格式:：yyyy-MM-dd</p>
	 * 
	 * @return
	 */
	public static Date getFirstDayOfNextMonth() {
		LocalDate onenext = LocalDate.now().with(TemporalAdjusters.firstDayOfNextMonth());
		return stringToDate(onenext.toString());
	}
	
	/**
	 * <h5>功能:获取上个月第一天</h5>
	 * <p>格式:：yyyy-MM-dd</p>
	 * 
	 * @return
	 */
	public static Date getFirstDayOfPrevMonth() {
		LocalDate onenext = LocalDate.now().minus(1, ChronoUnit.MONTHS).with(TemporalAdjusters.firstDayOfMonth());
		return stringToDate(onenext.toString());
	}
	
	/**
	 * @功能描述：	获取指定时间段内的日期
	 *
	 * @param startDate
	 * @param endDate
	 * @return 日期列表从小到大
	 *
	 * @作者：zhangpj		@创建时间：2018年3月29日
	 */
	public static List<String> findDates(String startDate, String endDate) {
		List<String> findDates = new ArrayList<String>();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dBegin = sdf.parse(startDate);
			Date dEnd = sdf.parse(endDate);
			findDates = findDates(dBegin, dEnd);
		} catch (ParseException e) {
			System.out.println("获取指定时间段的的日期发生异常");
			e.printStackTrace();
		}
		return findDates;
	}
	
    public static void main(String[] args) {
//		System.out.println("getNowTime()\t" + getNowDateTime());
//		System.out.println("getNowTime()\t" + getNowDateTime("yyyy-MM-dd"));
//		System.out.println("getNowTime()\t" + getNowDateTime("HH:mm:ss"));
//		System.out.println("getNowTime()\t" + getNowDate());
//		System.out.println("getNowTimeExt()\t" + getNowTimeExt());
//		System.out.println("getNowTimeToSec()\t" + getNowTimeToSec());
//		System.out.println("getNowTimeToMS()\t" + getNowTimeToMS());
//		System.out.println("getTimeStampToMS()\t" + getTimeStampToMS());
//		System.out.println("getTimeStampToSec()\t" + getTimeStampToSec());
//		System.out.println("getNowTime(String format)\t" + getNowTime("yyyymmddhhmmssSSS").length());
//		System.out.println("getHalfYearLaterTime()\t" + getHalfYearLaterTime());
//		System.out.println("20160922092954983132800093661014".length());
//		System.out.println(getDistanceSecond("2018-04-04 10:10:49", "2018-04-04 10:10:53"));
//
//		System.out.println(getTimeByTimeStamp("1592448070"));
//		System.out.println(getTimeByTimeStamp("1592449070"));
//		System.out.println(getDistanceSecond(getTimeByTimeStamp("1592448070"), getTimeByTimeStamp("1592449070")));
//
//		System.out.println(getDayOfWeek());
//		System.out.println(getDayOfWeek("2018-02-03"));
//		System.out.println(getDayOfWeek("2018-02-04 12：:1:01"));
//
//		System.out.println(dateTimeToString(addDays(new Date(), 3)));
//		System.out.println(dateToString(addDays(new Date(), 1)));
//
//		System.out.println(getTimeByTimeStamp("1592530913"));
//		System.out.println(addDays(-1));
		
    	// -----------------------------------
//    	Date date = new Date();
//    	System.out.println(dateToString(date));
//    	System.out.println(dateToStringExt(date));
//    	System.out.println(timeToString(date));
//    	System.out.println(timeToStringExt(date));
//    	System.out.println(dateTimeToString(date));
//    	System.out.println(dateTimeToStringExt(date));
//		System.out.println(dateTimeToString(getNowDateExt()));
		Date d = getFirstDayOfNextMonth();
		System.out.println(dateToString(d));
	}
}
