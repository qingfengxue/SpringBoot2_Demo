package com.qfx.demo.handler;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.qfx.demo.vo.MessageBean;

/**
 * <h5>描述:全局异常处理类</h5>
 * 优先执行此异常处理类
 *  
 */
@ControllerAdvice
public class MyExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(MyExceptionHandler.class);
		
	/**
	 * <h5>功能:Shiro权限验证未通过异常</h5>
	 * @param request
	 * @param e
	 * @return 错误信息
	 */
	@ExceptionHandler(value =AuthorizationException.class)
    public ResponseEntity<?> authorizationExceptionHandler(HttpServletRequest request, AuthorizationException e){
		String requestUri =  request.getRequestURI();
		
		// 最后的e会在控制台输出异常的完整信息,如果不想显示完整的错误信息可以将e去掉
//		LOG.error("请求[{}]发生[{}]异常", requestUri, e.getMessage(), e);
		LOG.error("请求[{}]发生[{}]异常", requestUri, e.getMessage());
		
		// 返回错误信息,交给其他异常处理类处理
//		return e.getMessage();
		MessageBean messageBean = new MessageBean();
		messageBean.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		messageBean.setMessage("权限验证未通过[" + e.getMessage() + "]");
		
		return ResponseEntity.ok().body(messageBean);
    }

	/**
	 * <h5>功能:全局异常处理方式二</h5>
	 * 返回视图用String或者ModelAndView,返回纯数据用ResponseEntity<?>,这里返回一个数据
	 * @param request
	 * @param e
	 * @return 错误信息
	 */
	@ExceptionHandler(value =Exception.class)
    public ResponseEntity<?> exceptionHandler(HttpServletRequest request, Exception e){
		Map<String, Object> paramsMap = getMaps(request);
		Map<String, Object> headersMap = getHeaders(request);
	
		String requestUri = request.getRequestURI();
	
		LOG.error("请求[{}]发生[{}]异常", requestUri, e.getMessage());
		LOG.error("参数[{}]", paramsMap);
		LOG.error("header[{}]", headersMap);
	
		MessageBean messageBean = new MessageBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(messageBean);
    }
	
	// =================== private method ===================
	
	/**
	 * <h5>功能:获取从request中传递过来的参数信息</h5>
	 * 
	 * @return Map<String, Object>
	 */
	private Map<String, Object> getMaps(HttpServletRequest request){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> enume = request.getParameterNames();
		while (enume.hasMoreElements()) {
			String key = (String) enume.nextElement();
			String[] values = request.getParameterValues(key);
			paramMap.put(key, values.length == 1 ? request.getParameter(key).trim() : values);
		}
		
		return paramMap;
	}
	
	/**
	 * <h5>功能: 获取从request中传递过来的header信息</h5>
	 * 
	 * @return Map<String, Object>
	 */
	private Map<String, Object> getHeaders(HttpServletRequest request) {
		Map<String, Object> headerMap = new HashMap<String, Object>();
		Enumeration<?> er = request.getHeaderNames();//获取请求头的所有name值
		String headerName;
		while(er.hasMoreElements()){
			headerName = er.nextElement().toString();
			headerMap.put(headerName, request.getHeader(headerName));
		}
		
		return headerMap;
	}
}