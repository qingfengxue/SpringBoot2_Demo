package com.qfx.demo.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.qfx.demo.vo.MessageBean;

public class CustomJwtFilter extends BasicHttpAuthenticationFilter {

	/**
	 * 该方法用于判断当前用户是否有权访问指定的 URL 或资源。如果返回 true，则表示允许访问；否则，表示禁止访问
	 * 1. 返回true,shiro直接通过验证
     * 2. 返回false,shiro会根据onAccessDenied的方法的返回值决定是否允许访问url
	 */
	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		System.out.println("--------------------- 第一步、验证是否携带token信息 ---------------------");
		response.setContentType("application/json;charset=utf-8");
		MessageBean messageBean = new MessageBean();
		
		try {
			// 1.从HTTP头中获取Authorization头的值
	        String token = getAuthzHeader(request);
	        
			// 2.验证请求头是否携带"Authorization"信息
			if(StringUtils.hasLength(token) && ! "null".equals(token.toLowerCase())) {
				// 3.执行基本身份验证登录逻辑
				boolean executeLogin = executeLogin(request, response);
				return executeLogin;
			} else {
				messageBean.setCode(HttpServletResponse.SC_UNAUTHORIZED);
				messageBean.setMessage("您还没有登录,请进行登录!");
				
                response.getWriter().print(JSONObject.toJSONString(messageBean));
			}
		} catch (Exception e) {
			try {
				messageBean.setCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				messageBean.setMessage(e.getMessage());
				response.getWriter().print(JSONObject.toJSONString(messageBean));
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * 执行基本身份验证登录逻辑
	 */
	@Override
	protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
		// 从HTTP头中获取Authorization头的值
		String tokenStr = getAuthzHeader(request);
		
		JwtToken jwtToken = new JwtToken(tokenStr);
        // 提交给realm进行登录校验,将会调用JwtRealm中的doGetAuthenticationInfo方法进行登录验证
        // 如果校验失败会抛出异常,而异常会在我们的全局异常捕获类ExceptionController中捕获
        getSubject(request, response).login(jwtToken);
        
        // 如果没有抛出异常则代表登入成功，返回true
        return true;
	}

//	/**
//	 * 添加跨域支持,也可以@CrossOrigin注解等方式来实现,或者采用配置类的方式来实现
//	 */
//	@Override
//	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
//		HttpServletRequest httpRequest = (HttpServletRequest) request;
//	    HttpServletResponse httpResponse = (HttpServletResponse) response;
//
//	    // 获取请求头部信息
//	    String origin = httpRequest.getHeader("Origin");
//	    String headers = httpRequest.getHeader("Access-Control-Request-Headers");
//	    String methods = httpRequest.getHeader("Access-Control-Request-Method");
//
//	    // 判断是否为预检请求（Preflight Request）
//	    if ("OPTIONS".equalsIgnoreCase(httpRequest.getMethod())) {
//	        // 允许跨域请求
//	        httpResponse.setHeader("Access-Control-Allow-Origin", origin);
//	        httpResponse.setHeader("Access-Control-Allow-Methods", methods);
//	        httpResponse.setHeader("Access-Control-Allow-Headers", headers);
//
//	        return true; // 直接返回，不进行身份验证
//	    }
//
//	    // 设置响应头部信息
//	    httpResponse.setHeader("Access-Control-Allow-Origin", origin);
//	    httpResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//	    httpResponse.setHeader("Access-Control-Allow-Headers", methods);
//
//	    // 处理身份验证
//	    return super.preHandle(httpRequest, httpResponse);
//	}
}
