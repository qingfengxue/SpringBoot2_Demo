package com.qfx.demo.shiro;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;

import com.alibaba.fastjson.JSONObject;
import com.qfx.demo.vo.MessageBean;
import com.qfx.demo.vo.SysUser;

/**
 * @功能描述：拦截控制,用于替换shiro默认的roles拦截规则,改"并且(and)"为"或者 (or)"
 *
 * @作者：zhangpj		@创建时间：2017年5月27日
 */
public class CustomRolesFilter extends AuthorizationFilter{

	private final Logger logger = LogManager.getLogger(getClass()); 
	
	/**
	 * Overriding
	 * @功能描述：设置同一个URL配置多个角色为"或者"的关系,默认为"并且",
	 * 			如:/user/** = Role["admin,user"],默认必须满足"admin","user"条件,
	 * 			      改为"或者"之后只需要满足一个条件即可(Ini.Section中有此url,会走此方法)
	 *
	 * @param request
	 * @param response
	 * @param obj 当前请求url所需要的权限信息,从ShiroConfig.shiroFilter方法的filterChainDefinitionMap中获取
	 * @return
	 * @throws Exception
	 */
	@Override
	protected boolean isAccessAllowed(ServletRequest request,ServletResponse response, Object obj) throws Exception {
		System.out.println("--------------------- 第三步、权限验证 ---------------------");
		// 获取请求地址
		HttpServletRequest hsq = (HttpServletRequest) request;
		String requestUrl = hsq.getServletPath();
		
		System.out.println("-------- 请求[" + requestUrl + "]权限验证开始 --------");
      
		Subject subject = getSubject(request, response);
		// 验证是否登录
		if (null == subject.getPrincipals()) {
			return false;
		}
        // 获取用户信息,这里返回的对象类型与登录验证时
        // new SimpleAuthenticationInfo(user, pwd, this.getName())中的第一个参数的类型需要保持一致
        SysUser user = (SysUser)subject.getPrincipals().getPrimaryPrincipal();
        
        System.out.println("--------1.开启用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤--------");
        
        // 获取角色信息
        String[] rolesArray = (String[]) obj;
        if (rolesArray == null || rolesArray.length == 0) { //没有角色限制，有权限访问
        	System.out.println("--------3.用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤结束--------");
        	logger.info("用户["+user.getUserName()+"]访问["+requestUrl+"]无角色限制,权限验证通过!");
            return true;
        }
        // 验证是否有当前请求地址的权限(循环验证)
        for (int i = 0; i < rolesArray.length; i++) {
        	// 可直接调用JwtRealm.doGetAuthorizationInfo方法进行权限验证(建议使用,通用性更好)
        	boolean hasRole = subject.hasRole(rolesArray[i]);
            if (hasRole) { //若当前用户是rolesArray中的任何一个，则有权限访问  
            	System.out.println("--------3.用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤结束--------");
            	logger.info("用户["+user.getUserName()+"]访问["+requestUrl+"]权限验证通过!");
                return true;    
            }
            // 如果user中有权限信息,可以直接从这里与rolesArray进行比对(不建议使用,user必须要有角色信息方可使用)
            // 略...
        }
        System.out.println("--------3.用户["+user.getUserName()+"]访问["+requestUrl+"]的角色过滤结束--------");
        logger.info("用户["+user.getUserName()+"]访问["+requestUrl+"]权限验证失败,禁止访问!");
        
        System.out.println("--------------------- 请求[" + requestUrl + "]权限验证结束 ---------------------");
        
		return false;
	}
	
	/**
	 * isAccessAllowed验证失败后处理方法
	 * 这里返回403错误信息,而不是返回403错误页面
	 */
	@Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {
		System.out.println("--------------------- 第五步、权限验证失败处理 ---------------------");
		MessageBean messageBean = new MessageBean();
		messageBean.setCode(HttpServletResponse.SC_FORBIDDEN);
		messageBean.setMessage("权限验证失败,禁止访问!");
		
        HttpServletResponse httpResponse = WebUtils.toHttp(response);
        httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        httpResponse.setContentType("application/json;charset=UTF-8");
        httpResponse.getWriter().write(JSONObject.toJSONString(messageBean));
        
        return false;
    }
}
