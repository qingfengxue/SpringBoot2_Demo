package com.qfx.demo.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qfx.demo.cache.UserCache;
import com.qfx.demo.vo.SysUser;

@Controller
@RequestMapping("role")
public class RoleController extends BaseController{
	
	public RoleController() {
		System.out.println("RoleController 构建了~~~");
	}

	/**
	 * @功能描述：	跳转到list页面
	 *
	 * @作者：zhangpj		@创建时间：2016年12月11日
	 * @return
	 */
	@RequestMapping("list")
	public Map<String, SysUser> list(){
		Map<String, SysUser> userCacheMap = UserCache.userCacheMap;
		System.out.println("返回role信息");
		return userCacheMap;
	}
}
