package com.qfx.demo.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.demo.service.LoginService;
import com.qfx.demo.vo.MessageBean;
import com.qfx.demo.vo.SysUser;

@RestController
@RequestMapping("/login")
public class loginController {
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping("/login")
    public ResponseEntity<?> login(SysUser sysUser, HttpServletResponse response) throws AuthenticationException {
        // shiro登录验证
        MessageBean messageBean = loginService.loginJwt(sysUser, response);
        return ResponseEntity.ok(messageBean);
    }
	
	@RequestMapping("/test")
	@ResponseBody
    public String test() {
        return "你好";
    }
	
	/**
	 * 使用注解添加权限验证
	 */
	@RequestMapping("/test02")
	@ResponseBody
	@RequiresRoles("网管")
	public String test02() {
		return "你也好";
	}
}
