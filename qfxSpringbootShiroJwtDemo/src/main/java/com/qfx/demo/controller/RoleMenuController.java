package com.qfx.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.demo.shiro.ShiroPermissionSer;

@RestController
@RequestMapping("roleMenu")
public class RoleMenuController extends BaseController{
	@Autowired
	ShiroPermissionSer shiroPermissionSer;
	
	public RoleMenuController() {
		System.out.println("RoleController 构建了~~~");
	}

	
	/**
	 * 更新权限
	 */
	@RequestMapping("initChain")
	public String initChain() {
		boolean flag = shiroPermissionSer.updatePermission();
		return "权限更新" + (flag ? "成功!" : "失败,请重试!");
	}
}
