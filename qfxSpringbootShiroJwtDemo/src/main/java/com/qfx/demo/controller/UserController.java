package com.qfx.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.demo.cache.UserCache;
import com.qfx.demo.vo.SysUser;

@RestController
@RequestMapping("user")
public class UserController extends BaseController{
	
	public UserController() {
		System.out.println("UserController 构建了~~~");
	}

	/**
	 * @功能描述：	跳转到list页面
	 *
	 * @作者：zhangpj		@创建时间：2016年12月11日
	 * @return
	 */
	@RequestMapping("list")
	public Map<String, SysUser> list(){
		return UserCache.userCacheMap;
	}
	
	/**
	 * @功能描述：	获取参数并跳转到list页面
	 *
	 * @作者：zhangpj		@创建时间：2016年12月11日
	 * @param req
	 * @return
	 */
	@RequestMapping("save")
	public String save(HttpServletRequest req){
		// 获取参数
		Map<String, Object> maps = this.getMaps(req);
		String userName = String.valueOf(maps.get("userName"));
		int userAge = Integer.valueOf(String.valueOf(maps.get("userAge")));
		System.out.println(userName+"	"+userAge);
		
		req.setAttribute("userName", userName);
		req.setAttribute("userAge", userAge);

		return "system/user/list";
	}
}
