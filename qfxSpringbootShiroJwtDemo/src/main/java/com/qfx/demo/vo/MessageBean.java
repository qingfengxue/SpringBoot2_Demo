package com.qfx.demo.vo;

import java.util.Date;

/**
 * @功能描述： JSON模型 用户后台向前台返回的JSON对象
 */
public class MessageBean {
	private int code = 200;	// 返回编码
	private String message = "";	// 提示信息
	private Object data = null;		// 其他信息
	private Long time = new Date().getTime();
	
	public MessageBean() {
		super();
	}

	public MessageBean(String message) {
		super();
		this.message = message;
	}

	public MessageBean(Object data) {
		super();
		this.data = data;
	}

	public MessageBean(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public MessageBean(String message, Object data) {
		super();
		this.message = message;
		this.data = data;
	}

	public MessageBean(int code, String message, Object data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}
}
