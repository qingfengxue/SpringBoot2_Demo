package com.qfx.demo.cache;

import java.util.HashMap;
import java.util.Map;

import com.qfx.demo.vo.SysUser;

/**
 * @功能描述： 存放用户缓存数据
 *
 * @作者：zhangpj @创建时间：2017年5月18日
 */
public class UserCache {
	// 存放公共信息缓存数据
	public static Map<String, SysUser> userCacheMap = new HashMap<String, SysUser>();

	static{
		System.out.println("初始化用户信息开始...");
		
		// 默认密码为 111111
		SysUser user = new SysUser();
		user.setUserName("zhangsan");
		user.setPassWord("111111");
		user.setRoleName("超级管理员");
		
		// 默认密码为 111111
		SysUser user2 = new SysUser();
		user2.setUserName("lisi");
		user2.setPassWord("111111");
		user2.setRoleName("游客");
		
		// 默认密码为 111111
		SysUser user3 = new SysUser();
		user3.setUserName("wangwu");
		user3.setPassWord("111111");
		user3.setRoleName("网管"); 
		
		// 默认密码为 111111
		SysUser user4 = new SysUser();
		user4.setUserName("linlin");
		user4.setPassWord("111111");
		user4.setRoleName("游客"); 
		
		setUserCacheMap(user.getUserName(), user);
		setUserCacheMap(user2.getUserName(), user2);
		setUserCacheMap(user3.getUserName(), user3);
		setUserCacheMap(user4.getUserName(), user4);
		
		System.out.println("初始化用户信息完毕!");
	}

	public static SysUser getUserCacheMap(String key) {
		return userCacheMap.get(key);
	}

	public static void setUserCacheMap(String key, SysUser user) {
		userCacheMap.put(key, user);
	}
}
