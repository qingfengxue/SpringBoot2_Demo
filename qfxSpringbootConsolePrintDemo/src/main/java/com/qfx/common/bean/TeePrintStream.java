package com.qfx.common.bean;

import java.io.OutputStream;
import java.io.PrintStream;

import com.qfx.websocket.service.WebSocketSer;

/**
 * 继承自 `PrintStream`，可以同时将输出内容发送到两个不同的输出流。
 */
public class TeePrintStream extends PrintStream {
	private PrintStream secondStream; // 第二个输出流

	/**
	 * 构造函数，用于创建 `TeePrintStream` 对象。
	 *
	 * @param mainStream   主要的输出流
	 * @param secondStream 第二个输出流
	 */
	public TeePrintStream(OutputStream mainStream, OutputStream secondStream) {
		super(mainStream); // 调用父类 PrintStream 的构造函数，传入主要的输出流
		this.secondStream = new PrintStream(secondStream); // 创建第二个输出流
	}

	/**
	 * 将字节数组输出到主要和第二个输出流。
	 *
	 * @param buf 输出的字节数组
	 * @param off 数组的起始偏移量
	 * @param len 要写入的字节数
	 */
	@Override
	public void write(byte[] buf, int off, int len) {
		super.write(buf, off, len); // 将字节数组输出到主要输出流
		secondStream.write(buf, off, len); // 将字节数组输出到第二个输出流
		// 检测换行符
		String str = new String(buf, off, len);
		if ("\r\n".equals(str) || "\r".equals(str)) {
			return;
		}
		WebSocketSer.sendInfo(str, null);
	}

	/**
	 * 刷新主要和第二个输出流。
	 */
	@Override
	public void flush() {
		super.flush(); // 刷新主要输出流
		secondStream.flush(); // 刷新第二个输出流
	}

	/**
	 * 关闭第二个输出流。
	 */
	@Override
	public void close() {
		// super.close(); // 务必不要关闭,否则后续正常的标准输出流都无法生效
		secondStream.close(); // 关闭第二个输出流
	}
}
