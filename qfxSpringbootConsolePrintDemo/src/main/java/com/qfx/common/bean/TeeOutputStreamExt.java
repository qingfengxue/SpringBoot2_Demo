package com.qfx.common.bean;

import java.io.IOException;
import java.io.OutputStream;

import com.qfx.websocket.service.WebSocketSer;

public class TeeOutputStreamExt extends OutputStream {

	private OutputStream primaryStream;
	private OutputStream secondaryStream;

	public TeeOutputStreamExt(OutputStream primaryStream, OutputStream... secondaryStreams) {
		this.primaryStream = primaryStream;
		if (secondaryStreams.length == 0) {
			throw new IllegalArgumentException("请至少提供一个输出流");
		}
		this.secondaryStream = new CompositeOutputStream(secondaryStreams);
	}

	@Override
	public void write(int b) throws IOException {
		primaryStream.write(b);
		secondaryStream.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		primaryStream.write(b, off, len);
		secondaryStream.write(b, off, len);

		// 检测换行符
		String str = new String(b, off, len);
		if ("\r\n".equals(str)) {
			return;
		}
		WebSocketSer.sendInfo(new String(b, off, len).replace("\r\n", ""), null);
	}

	@Override
	public void flush() throws IOException {
		primaryStream.flush();
		secondaryStream.flush();
	}

	@Override
	public void close() throws IOException {
//        primaryStream.close();
		secondaryStream.close();
	}
}
