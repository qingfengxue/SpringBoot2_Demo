package com.qfx.demo.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.qfx.common.bean.TeePrintStream;
import com.qfx.demo.service.TestSer;

@Service
public class TestSerImpl implements TestSer {
	
	@Override
	@Async
	public void start(String cid) {
		consoleOutput(cid);
	}
	
	// -------------------------- private method ----------------------------
	/**
	 * 控制台打印及输出
	 */
	private void consoleOutput(String cid) {
		// 保存原来的标准输出流
		PrintStream originalOut = new PrintStream(System.out);
		String output = "";
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				TeePrintStream teePrintStream = new TeePrintStream(System.out, baos)) {
			System.setOut(teePrintStream);

			infoPrint();
			// 获取捕获到的控制台输出
			output = baos.toString();
		} catch (Exception e) {
			e.getMessage();
		}

		// 恢复原始的标准输出流
		System.setOut(originalOut);

		System.out.println();
		System.out.println("-------------获取抓取到的控制台信息---------------");
		System.out.println(output);

	}

	/**
	 * 信息打印
	 */
	private void infoPrint() {
		try {
			for (int i = 1; i <= 16; i++) {
				System.out.println(i + ": 这条消息会同时在控制台中显示，并在字节数组输出流中存在");
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
