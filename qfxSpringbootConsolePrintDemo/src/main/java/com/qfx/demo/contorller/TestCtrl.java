package com.qfx.demo.contorller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.demo.service.TestSer;
import com.qfx.websocket.service.WebSocketSer;

@RestController
@RequestMapping("test")
public class TestCtrl {
	@Autowired
	TestSer testSer;

	@RequestMapping("start")
	public String start(String cid) {
		testSer.start(cid);
		return "OK";
	}
	
	@RequestMapping("consolePrint")
	public String consolePrint(String msg) {
		WebSocketSer.sendInfo(msg, null);
		return "OK";
	}
}
