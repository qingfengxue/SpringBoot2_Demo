package com.qfx.websocket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
 
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * <h5>描述:WebSocket服务端</h5>
 *  WebSocket是类似客户端服务端的形式(采用ws协议),
 *  所以 WebSocketServer其实就相当于一个ws协议的 Controller,
 *  可以在里面实现 @OnOpen、@onClose、@onMessage等方法
 */
@ServerEndpoint("/websocket/{cid}")
@Component
public class WebSocketSer {
    private static final Logger LOG = LoggerFactory.getLogger(WebSocketSer.class);
    
    // 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
 
    // concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<WebSocketSer> webSocketSet = new CopyOnWriteArraySet<WebSocketSer>();
 
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
 
    // 接收cid
    private String cid = "";
 
    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("cid") String cid) {
        this.session = session;
        webSocketSet.add(this);     // 加入set中
        addOnlineCount();           // 在线数加1
        LOG.info("客户端: " + cid + " 连接成功, 当前在线人数为：" + getOnlineCount());
        this.cid = cid;
        try {
            sendMessage("连接成功");
        } catch (IOException e) {
            LOG.error("发送消息异常：", e);
        }
    }
 
    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);  // 从set中删除
        subOnlineCount();           // 在线数减1
        //LOG.info("有一个连接关闭，当前在线人数为：" + getOnlineCount());
    }
 
    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        LOG.info("收到来自客户端 " + cid + " 的信息: " + message);
        // 群发消息
        for (WebSocketSer item : webSocketSet) {
            try {
                item.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
 
    /**
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        LOG.error("发生错误");
        error.printStackTrace();
    }
 
    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }
 
    /**
     * 群发自定义消息
     */
    public static void sendInfo(String message, @PathParam("cid") String cid) {
        //LOG.info("推送消息到客户端：" + cid + "，内容: [{}]", message);
        for (WebSocketSer item : webSocketSet) {
            try {
                // 这里可以设定只推送给这个cid的，为null则全部推送
                if (cid == null) {
                    item.sendMessage(message);
                } else if (item.cid.equals(cid)) {
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }
 
    public static synchronized int getOnlineCount() {
        return onlineCount;
    }
 
    public static synchronized void addOnlineCount() {
        WebSocketSer.onlineCount++;
    }
 
    public static synchronized void subOnlineCount() {
        WebSocketSer.onlineCount--;
    }
}
