# 1.项目介绍
	1.1 本示例采用的SpringBoot版本是2.5.3
	1.2 这是一个SpringBoot+jasypt,用来验证配置文件参数加密的的示例
# 2.启动服务
	添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
# 3.配置参数加密信息
## 3.1 pom.xml引入依赖
	<!-- 9.引入配置文件信息加密功能依赖包 -->
	<dependency>
	   <groupId>com.github.ulisesbocchio</groupId>
	   <artifactId>jasypt-spring-boot-starter</artifactId>
	   <version>3.0.5</version>
	</dependency>
## 3.2 application.properties添加参数
	## 设定jasypt加密的盐值(加密/解密使用,生产环境建议从命令行进行设置)
	jasypt.encryptor.password=qfxkey
	## 设定jasypt加密算法(默认PBEWithHMACSHA512AndAES_256,可不设定),还有PBEWithMD5AndDES与PBEWithMD5AndTripleDES加密算法
	jasypt.encryptor.algorithm=PBEWithHMACSHA512AndAES_256
	## 设定jasypt加密类(默认org.jasypt.iv.RandomIvGenerator,可不设定),对应org.jasypt.iv.NoIvGenerator与org.jasypt.iv.NoIvGenerator
	jasypt.encryptor.iv-generator-classname=AES256TextEncryptor
	
	## 设定加密内容,以默认格式ENC()进行包裹，加密信息从com.qfx.EncryptorTest.getPass()方法中生成即可
	spring.datasource.username=ENC(jlDqfJaB/LJHLtowv2JPT82ZKEzDnYV72dPoZ7HtdDNMPOdYhoD5WIObj2QPtXU3)
	spring.datasource.password=ENC(BjnC5rl5qimDnKmyrhfFi2EmfyMhKnFd3Wvst2rP7+C2T63+CTuVcsLRWlTHs7Ec)
## 3.3 扩展：加密算法与加密类关系
	加密算法                                                                                          加密类 
	PBEWithHMACSHA512AndAES_256(3.x版本默认)    org.jasypt.iv.RandomIvGenerator
	PBEWithMD5AndDES(3.x以下版本默认)            org.jasypt.iv.NoIvGenerator
	PBEWithMD5AndTripleDES                    org.jasypt.iv.NoIvGenerator
## 3.4 获取参数
	// 使用@Value("${xxx}")正常获取即可
	@Value("${spring.datasource.password}")
	String password;

# 4.测试
	运行com.qfx.TestApp.test()方法
	