package com.qfx.modules.util;

import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jasypt.util.text.StrongTextEncryptor;
import org.jasypt.util.text.TextEncryptor;

public class JasyptEncryptorUtils {

	/**
	 * 加密密码
	 */
	private static final String PASSWORD = "xxx";

	public static void main(String[] args) {
		// 需加密信息
		String name = "孙悟空";
		System.out.println("\r\n----------- PBEWithHMACSHA512AndAES_256 (3.x版本默认默认算法) -----------");
		encodeAES(name);
		
		System.out.println("\r\n----------- PBEWithMD5AndDES (3.x以下版本默认算法) -----------");
		encodeBasic(name);
		
		System.out.println("\r\n----------- PBEWithMD5AndTripleDES 算法 -----------");
		encodeStrong(name);
	}
	
	// -------------------- BasicTextEncryptor --------------------
	/**
	 * Basic加密
	 * 
	 * @param plaintext
	 * @return
	 */
	public static String encodeBasic(String plaintext) {
		BasicTextEncryptor encryptor = new BasicTextEncryptor();
		encryptor.setPassword(PASSWORD);
		
		return encode(encryptor, plaintext);
	}

	/**
	 * Basic解密
	 * 
	 * @param ciphertext
	 * @return
	 */
	public static String decodeBasic(String ciphertext) {
		BasicTextEncryptor encryptor = new BasicTextEncryptor();
		encryptor.setPassword(PASSWORD);

		return decode(encryptor, ciphertext);
	}
	
	// -------------------- AES256TextEncryptor --------------------
	/**
	 * AES加密(默认)
	 * 
	 * @param plaintext
	 * @return
	 */
	public static String encodeAES(String plaintext) {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword(PASSWORD);
		
		return encode(encryptor, plaintext);
	}
	
	/**
	 * AES解密
	 * 
	 * @param ciphertext
	 * @return
	 */
	public static String decodeAES(String ciphertext) {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword(PASSWORD);
		
		return decode(encryptor, ciphertext);
	}
	
	// -------------------- StrongTextEncryptor --------------------
	/**
	 * StrongText加密
	 * 
	 * @param plaintext
	 * @return
	 */
	public static String encodeStrong(String plaintext) {
		StrongTextEncryptor encryptor = new StrongTextEncryptor();
		encryptor.setPassword(PASSWORD);
		
		return encode(encryptor, plaintext);
	}
	
	/**
	 * StrongText解密
	 * 
	 * @param ciphertext
	 * @return
	 */
	public static String decodeStrong(String ciphertext) {
		StrongTextEncryptor encryptor = new StrongTextEncryptor();
		encryptor.setPassword(PASSWORD);
		
		return decode(encryptor, ciphertext);
	}
	
	// -------------------- private method --------------------
	/**
	 * 明文加密
	 * 
	 * @param encryptor 文本加解密对象
	 * @param plaintext 需加密的明文
	 * @return
	 */
	private static String encode(TextEncryptor encryptor, String plaintext) {
		System.out.println("明文字符串：" + plaintext);
		String ciphertext = encryptor.encrypt(plaintext);
		System.out.println("加密后字符串：" + ciphertext);
		return ciphertext;
	}
	
	/**
	 * 解密
	 * 
	 * @param encryptor 文本加解密对象
	 * @param ciphertext 需解密的密文
	 * @return
	 */
	private static String decode(TextEncryptor encryptor, String ciphertext) {
		System.out.println("加密字符串：" + ciphertext);
		ciphertext = "ENC(" + ciphertext + ")";
		if (PropertyValueEncryptionUtils.isEncryptedValue(ciphertext)) {
			String plaintext = PropertyValueEncryptionUtils.decrypt(ciphertext, encryptor);
			System.out.println("解密后的字符串：" + plaintext);
			return plaintext;
		}
		System.out.println("解密失败");
		return "";
	}
}
