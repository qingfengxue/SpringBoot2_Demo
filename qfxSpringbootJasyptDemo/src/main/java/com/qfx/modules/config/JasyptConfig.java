package com.qfx.modules.config;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * 功能:配置类,用于初始化Jasypt字符串加密器
 */
@Configuration
public class JasyptConfig {
	
	/**
	 * 加密密码,默认null
	 */
	private final static String PASSWORD = "qfxkey";

	/**
     * 功能:创建并配置一个Jasypt字符串加密器 Bean,覆盖默认的 StringEncryptor,默认算法:PBEWithHMACSHA256AndAES_256
	 * Bean 'jasyptConfig' of type [com.linghu.base.common.config.JasyptConfig$$SpringCGLIB$$0] is not eligible for getting processed by all BeanPostProcessors
	 * (for example: not eligible for auto-proxying). Is this bean getting eagerly injected into a currently created BeanPostProcessor
	 * [projectingArgumentResolverBeanPostProcessor]? Check the corresponding BeanPostProcessor declaration and its dependencies.
	 * Bean 'jasyptStringEncryptor' of type [org.jasypt.encryption.pbe.PooledPBEStringEncryptor] is not eligible for getting processed by all BeanPostProcessors
	 * (for example: not eligible for auto-proxying). Is this bean getting eagerly injected into a currently created BeanPostProcessor
	 * [projectingArgumentResolverBeanPostProcessor]? Check the corresponding BeanPostProcessor declaration and its dependencies.
	 * 如果报以上错误信息,请添加@DependsOn("projectingArgumentResolverBeanPostProcessor"),确保在projectingArgumentResolverBeanPostProcessor之后初始化
	 *
     * @return 配置好的字符串加密器实例
     */
//	@DependsOn("projectingArgumentResolverBeanPostProcessor")
	@Bean("jasyptStringEncryptor")
    public StringEncryptor jasyptStringEncryptor() {
        // 返回配置好的加密器实例
        return jasyptStringEncryptor(1, PASSWORD);
    }
	
	/**
	 * 功能:创建并配置一个Jasypt字符串加密器 Bean,覆盖默认的 StringEncryptor
	 *
	 * @param type 加密算法 
	 * 			1-PBEWithHMACSHA256AndAES_256(默认算法)
	 * 			2-PBEWithMD5AndDES加密算法
	 * 			3-PBEWithMD5AndTripleDES
	 * @param password 加密密码
	 * 
	 * @return 配置好的字符串加密器实例
	 */
	public StringEncryptor jasyptStringEncryptor(Integer type, String password) {
		// 创建一个基于 PooledPBEStringEncryptor 的加密器实例
		PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
		// 创建一个简单的字符串 PBE 配置对象
		SimpleStringPBEConfig config = new SimpleStringPBEConfig();
		
		switch (type) {
		case 2:
			// PBEWithMD5AndDES加密算法(3.x以下版本默认)
			createBasicConfig(config, password);
			break;
		case 3:
			// PBEWithMD5AndTripleDES加密算法
	        createStrongConfig(config, password);
			break;
		default:
			// PBEWithHMACSHA256AndAES_256加密算法(3.x版本默认)
	        createAESConfig(config, password);
			break;
		}
		
		// 将配置应用到加密器
		encryptor.setConfig(config);
		
		// 返回配置好的加密器实例
		return encryptor;
	}
	
	// ---------------------------------------- private method ----------------------------------------
	/**
	 * 功能:生成默认的PBEWithHMACSHA256AndAES_256加密算法配置信息
	 * 3.x版本默认
	 */
	private void createAESConfig(SimpleStringPBEConfig config, String password) {
		// ---------- 这些可以使用默认的 ----------
		// 设置加密提供者名称,默认null
		// config.setProviderName("qfx");
		// 设置加密后的字符串输出类型为 base64,默认base64
		// config.setStringOutputType("base64");
		// 设置密钥获取迭代次数,增加安全性,默认1000
		// config.setKeyObtentionIterations("1000");
		// 设置盐生成器类名,使用随机盐生成器,默认org.jasypt.salt.RandomSaltGenerator
		// config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
		
		// ---------- 以下是必须设置的 ----------
		// 设置加密器池的大小,必须大于0
		config.setPoolSize("10");
        // 设置加密密码,默认null
        config.setPassword(password);
        // 设置加密算法,使用更强的加密算法,默认PBEWithHMACSHA256AndAES_256
        config.setAlgorithm("PBEWithHMACSHA256AndAES_256");
        // 设置初始化向量生成器类名，使用随机初始化向量生成器
        config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
	}
	
	/**
	 * 功能:生成PBEWithMD5AndDES加密算法配置信息
	 * 3.x以下版本默认
	 */
	private void createBasicConfig(SimpleStringPBEConfig config, String password) {
		// ---------- 这些可以使用默认的 ----------
		// 设置加密提供者名称,默认null
		// config.setProviderName("qfx");
		// 设置加密后的字符串输出类型为 base64,默认base64
		// config.setStringOutputType("base64");
		// 设置密钥获取迭代次数,增加安全性,默认1000
		// config.setKeyObtentionIterations("2000");
		// 设置盐生成器类名,使用随机盐生成器,默认org.jasypt.salt.RandomSaltGenerator
		// config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        
		// ---------- 以下是必须设置的 ----------
		// 设置加密器池的大小,必须大于0
		config.setPoolSize("10");
        // 设置加密密码,默认null
        config.setPassword(password);
        // 设置加密算法,使用更强的加密算法,默认PBEWithHMACSHA256AndAES_256
        config.setAlgorithm("PBEWithMD5AndDES");
        // 设置初始化向量生成器类名，使用随机初始化向量生成器
        config.setIvGeneratorClassName("org.jasypt.iv.NoIvGenerator");
	}
	
	/**
	 * 功能:生成PBEWithMD5AndTripleDES加密算法配置信息
	 */
	private void createStrongConfig(SimpleStringPBEConfig config, String password) {
		// ---------- 这些可以使用默认的 ----------
		// 设置加密提供者名称,默认null
		// config.setProviderName("qfx");
		// 设置加密后的字符串输出类型为 base64,默认base64
		// config.setStringOutputType("base64");
		// 设置密钥获取迭代次数,增加安全性,默认1000
		// config.setKeyObtentionIterations("2000");
		// 设置盐生成器类名,使用随机盐生成器,默认org.jasypt.salt.RandomSaltGenerator
		// config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");

		// ---------- 以下是必须设置的 ----------
		// 设置加密器池的大小,必须大于0
		config.setPoolSize("10");
		// 设置加密密码,默认null
		config.setPassword(password);
		// 设置加密算法,使用更强的加密算法,默认PBEWithHMACSHA256AndAES_256
		config.setAlgorithm("PBEWithMD5AndTripleDES");
		// 设置初始化向量生成器类名，使用随机初始化向量生成器
		config.setIvGeneratorClassName("org.jasypt.iv.NoIvGenerator");
	}
}
