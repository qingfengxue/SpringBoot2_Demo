package com.qfx;

import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.qfx.modules.config.JasyptConfig;

@SpringBootTest
public class TestApp {
	
	@Value("${my.param.name}")
	private String myParamName;
	
	/**
	 * 启动即可,测试配置文件中的加密信息是否能正确获取
	 */
	@Test
	public void test() {
		System.out.println("my.param.name = " + myParamName);
	}
	
	/**
	 * 自定义加密解密测试
	 */
//	@Test
	public void encrypt() {
		String str = "孙悟空";
		System.out.println("原始信息:" + str);
		// 加密
		String encryptStr = encrypt(str);
		System.out.println("加密后信息:" + encryptStr);
		// 解密
		System.out.println("解密前信息:" + encryptStr);
		String decryptStr = decrypt(encryptStr);
		System.out.println("解密后信息:" + decryptStr);
	}
	
	
	// ---------------------------------------- private mothod ----------------------------------------
	/**
     * 加密字符串
     *
     * @param plainText 明文字符串
     * @return 加密后的字符串
     */
    private String encrypt(String plainText) {
    	JasyptConfig jasyptConfig = new JasyptConfig();
    	StringEncryptor stringEncryptor = jasyptConfig.jasyptStringEncryptor(1, "qfxkey");
        return stringEncryptor.encrypt(plainText);
    }

    /**
     * 解密字符串
     *
     * @param encryptedText 加密后的字符串
     * @return 明文字符串
     */
    private String decrypt(String encryptedText) {
    	JasyptConfig jasyptConfig = new JasyptConfig();
    	StringEncryptor stringEncryptor = jasyptConfig.jasyptStringEncryptor(1, "qfxkey");
        return stringEncryptor.decrypt(encryptedText);
    }
}
