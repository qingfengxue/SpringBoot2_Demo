package com.qfx.modules.system.dao;

import com.qfx.modules.common.dao.BaseDao;
import com.qfx.modules.system.entity.SysMenu;

public interface SysMenuMapper extends BaseDao<SysMenu>{

}