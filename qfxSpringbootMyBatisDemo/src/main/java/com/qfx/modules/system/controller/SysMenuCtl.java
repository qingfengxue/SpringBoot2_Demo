package com.qfx.modules.system.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.modules.system.service.SysMenuSer;

@RestController
@RequestMapping("sys/menu")
public class SysMenuCtl {
	@Autowired
	SysMenuSer sysMenuSer;
	
	/**
	 * 分页查询菜单信息
	 * @return
	 */
	@RequestMapping("list")
	public Map<String,Object> list(){
		return sysMenuSer.selectAll();
	}
	
	/**
	 * 分页查询菜单信息
	 * @return
	 */
	@RequestMapping("listExt")
	public Map<String,Object> listExt(){
		return sysMenuSer.selectAllExt();
	}
	
	/**
	 * 分页查询菜单信息
	 * @return
	 */
	@RequestMapping("listExtTwo")
	public Map<String,Object> listExtTwo(){
		return sysMenuSer.selectAllExtTwo();
	}
}
