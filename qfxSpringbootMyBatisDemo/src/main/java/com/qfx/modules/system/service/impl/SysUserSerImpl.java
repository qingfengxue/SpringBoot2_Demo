package com.qfx.modules.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qfx.modules.system.dao.SysUserMapper;
import com.qfx.modules.system.entity.SysUser;
import com.qfx.modules.system.service.SysUserSer;

@Service
public class SysUserSerImpl implements SysUserSer {
	
	@Autowired
	SysUserMapper sysUserMapper;

	@Override
	public List<SysUser> selectAll() {
		return sysUserMapper.selectAll();
	}
	
	@Override
	public SysUser info() {
		return sysUserMapper.info();
	}

}
