package com.qfx.modules.system.dao;

import java.util.List;
import java.util.Map;

import com.qfx.modules.system.entity.SysUser;

public interface SysUserMapper {
    SysUser selectByPrimaryKey(String userId);
    
    List<SysUser> selectAll();
    
    Map<String, Object> sayHello();
    
    SysUser info();
}