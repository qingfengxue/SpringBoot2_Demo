package com.qfx.modules.system.service;

import java.util.Map;

public interface SysMenuSer {
	Map<String,Object> selectAll();
	Map<String,Object> selectAllExt();
	Map<String,Object> selectAllExtTwo();
}
