package com.qfx.modules.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qfx.modules.common.service.BaseService;
import com.qfx.modules.common.util.ToolCommon;
import com.qfx.modules.system.dao.SysMenuMapper;
import com.qfx.modules.system.entity.SysMenu;
import com.qfx.modules.system.service.SysMenuSer;

@Service
public class SysMenuSerImpl extends BaseService<SysMenu> implements SysMenuSer {

	@Autowired
	SysMenuMapper sysMenuMapper;
	
	@Autowired
	ToolCommon<SysMenu> toolCommon;

	/**
	 * 分页查询菜单信息(传统方式)
	 */
	@Override
	public Map<String, Object> selectAll() {
		Map<String, Object> paramMap = getMaps();
		Page<SysMenu> page = getPage(paramMap);
		PageHelper.startPage(page.getPageNum(), page.getPageSize());

		// 根据条件获取菜单信息
		List<SysMenu> sysMenuList = sysMenuMapper.selectAll(paramMap);
		PageInfo<SysMenu> pageInfo = new PageInfo<SysMenu>(sysMenuList);
		// 获得总条数
		long total = pageInfo.getTotal();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("count", total);
		map.put("data", pageInfo.getList());

		return map;
	}

	/**
	 * 分页查询菜单信息(抽取为公共方法)
	 */
	@Override
	public Map<String, Object> selectAllExt() {
		return selectAll(sysMenuMapper);
	}
	
	
	/**
	 * 分页查询菜单信息(抽取为公共方法)
	 */
	@Override
	public Map<String, Object> selectAllExtTwo() {
		Map<String, Object> paramMap = getMaps();
		Page<SysMenu> page = getPage(paramMap);
		
		Map<String, Object> dataMap = null;
		// 方式一
//		ToolCommon<SysMenu> tc = new ToolCommon<>();
//		dataMap = tc.selectAll(sysMenuMapper, page.getPageNum(), page.getPageSize());
		
		// 方式二
		dataMap = toolCommon.selectAll(sysMenuMapper, page.getPageNum(), page.getPageSize());
		
		return dataMap;
	}
}
