package com.qfx.modules.system.service;

import java.util.List;

import com.qfx.modules.system.entity.SysUser;

public interface SysUserSer {
	List<SysUser> selectAll();
	
	SysUser info();
}
