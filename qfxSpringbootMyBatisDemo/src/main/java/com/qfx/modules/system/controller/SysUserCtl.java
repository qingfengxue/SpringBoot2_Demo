package com.qfx.modules.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfx.modules.system.entity.SysUser;
import com.qfx.modules.system.service.impl.SysUserSerImpl;

@RestController
@RequestMapping("test")
public class SysUserCtl {
	@Autowired
	SysUserSerImpl sysUserSer;
	
	@RequestMapping("list")
	public List<SysUser> list(){
		return sysUserSer.selectAll();
	}
	
	@RequestMapping("info")
	public SysUser info(){
		return sysUserSer.info();
	}

}
