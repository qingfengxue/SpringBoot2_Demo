package com.qfx.modules.common.util;

import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jasypt.util.text.StrongTextEncryptor;
import org.jasypt.util.text.TextEncryptor;

public class JasyptEncryptorUtils {

	private static final String salt = "qfxkey";

	public static void main(String[] args) {
		String name = "F6zx3qcY4yhl9t";
		// 3.x以下版本默认算法
		System.out.println("----------- PBEWithMD5AndDES 算法 -----------");
		encodeBasic(name);
		// 3.x版本默认默认算法
		System.out.println("\r\n----------- PBEWithHMACSHA512AndAES_256 算法 -----------");
		encodeAES(name);
		
		System.out.println("\r\n----------- PBEWithMD5AndTripleDES 算法 -----------");
		encodeStrong(name);
	}
	
	// -------------------- BasicTextEncryptor --------------------
	/**
	 * Basic加密
	 * 
	 * @param plaintext
	 * @return
	 */
	public static String encodeBasic(String plaintext) {
		BasicTextEncryptor encryptor = new BasicTextEncryptor();
		encryptor.setPassword(salt);
		
		return encode(encryptor, plaintext);
	}

	/**
	 * Basic解密
	 * 
	 * @param ciphertext
	 * @return
	 */
	public static String decodeBasic(String ciphertext) {
		BasicTextEncryptor encryptor = new BasicTextEncryptor();
		encryptor.setPassword(salt);

		return decode(encryptor, ciphertext);
	}
	
	// -------------------- AES256TextEncryptor --------------------
	/**
	 * AES加密(默认)
	 * 
	 * @param plaintext
	 * @return
	 */
	public static String encodeAES(String plaintext) {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword(salt);
		
		return encode(encryptor, plaintext);
	}
	
	/**
	 * AES解密
	 * 
	 * @param ciphertext
	 * @return
	 */
	public static String decodeAES(String ciphertext) {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword(salt);
		
		return decode(encryptor, ciphertext);
	}
	
	// -------------------- StrongTextEncryptor --------------------
	/**
	 * StrongText加密
	 * 
	 * @param plaintext
	 * @return
	 */
	public static String encodeStrong(String plaintext) {
		StrongTextEncryptor encryptor = new StrongTextEncryptor();
		encryptor.setPassword(salt);
		
		return encode(encryptor, plaintext);
	}
	
	/**
	 * StrongText解密
	 * 
	 * @param ciphertext
	 * @return
	 */
	public static String decodeStrong(String ciphertext) {
		StrongTextEncryptor encryptor = new StrongTextEncryptor();
		encryptor.setPassword(salt);
		
		return decode(encryptor, ciphertext);
	}
	
	// -------------------- private method --------------------
	/**
	 * 明文加密
	 * 
	 * @param encryptor 文本加解密对象
	 * @param plaintext 需加密的明文
	 * @return
	 */
	private static String encode(TextEncryptor encryptor, String plaintext) {
		System.out.println("明文字符串：" + plaintext);
		String ciphertext = encryptor.encrypt(plaintext);
		System.out.println("加密后字符串：" + ciphertext);
		return ciphertext;
	}
	
	/**
	 * 解密
	 * 
	 * @param encryptor 文本加解密对象
	 * @param ciphertext 需解密的密文
	 * @return
	 */
	private static String decode(TextEncryptor encryptor, String ciphertext) {
		System.out.println("加密字符串：" + ciphertext);
		ciphertext = "ENC(" + ciphertext + ")";
		if (PropertyValueEncryptionUtils.isEncryptedValue(ciphertext)) {
			String plaintext = PropertyValueEncryptionUtils.decrypt(ciphertext, encryptor);
			System.out.println("解密后的字符串：" + plaintext);
			return plaintext;
		}
		System.out.println("解密失败");
		return "";
	}

}
