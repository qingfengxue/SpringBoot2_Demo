package com.qfx.modules.common.service;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qfx.modules.common.dao.BaseDao;

public class BaseService<T> {
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	protected static String CURRENT_PAGE_NAME = "page";
	protected static String PAGE_SIZE_NAME = "limit";

	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected HttpServletResponse response;

	/**
	 * <h5>功能:获取从request中传递过来的参数信息</h5>
	 * 
	 * @return Map<String, Object>
	 */
	protected Map<String, Object> getMaps() {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> enume = request.getParameterNames();
		while (enume.hasMoreElements()) {
			String key = (String) enume.nextElement();
			String[] values = request.getParameterValues(key);
			paramMap.put(key, values.length == 1 ? request.getParameter(key).trim() : values);
		}

		return paramMap;
	}

	/**
	 * <h5>功能: 获取从request中传递过来的header信息</h5>
	 * 
	 * @return Map<String, Object>
	 */
	protected Map<String, Object> getHeaders() {
		Map<String, Object> headerMap = new HashMap<String, Object>();
		Enumeration<?> er = request.getHeaderNames();// 获取请求头的所有name值
		String headerName;
		while (er.hasMoreElements()) {
			headerName = er.nextElement().toString();
			headerMap.put(headerName, request.getHeader(headerName));
		}

		return headerMap;
	}

	/**
	 * @功能描述：获取分页信息
	 *
	 * @param map
	 * @return
	 *
	 * @作者：zhangpj @创建时间：2018年3月23日
	 */
	protected Page<T> getPage(Map<String, Object> map) {
		int pageNum = 1;
		int pageSize = 10;
		if (map.containsKey(CURRENT_PAGE_NAME) && !"".equals(map.get(CURRENT_PAGE_NAME))) {
			pageNum = Integer.parseInt((String) map.get(CURRENT_PAGE_NAME));
		}
		if (map.containsKey(PAGE_SIZE_NAME) && !"".equals(map.get(PAGE_SIZE_NAME))) {
			pageSize = Integer.parseInt((String) map.get(PAGE_SIZE_NAME));
		}
		Page<T> page = new Page<>(pageNum, pageSize);
		return page;
	}

	/**
	 * <h5>功能:异常处理</h5>
	 * 
	 * @param e
	 * @param log
	 */
	protected void excHandling(Exception e) {
		e.printStackTrace();
		// 手工回滚事务
		TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
	}

	/**
	 * <h5>功能:获取String[]类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息或转换失败返回null
	 */
	protected String[] getStringArryParam(Map<String, Object> paramMap, String key) {
		Object ojb = paramMap.get(key);
		return null == ojb ? null : (String[]) ojb;
	}

	/**
	 * <h5>功能:获取String类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息返回null
	 */
	protected String getStringParam(Map<String, Object> paramMap, String key) {
		Object ojb = paramMap.get(key);
		return null == ojb ? null : String.valueOf(ojb);
	}

	/**
	 * <h5>功能:获取int类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息返回0
	 */
	protected int getIntParam(Map<String, Object> paramMap, String key) {
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0 : Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * <h5>功能:获取double类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息返回0.0
	 */
	protected double getDoublePara(Map<String, Object> paramMap, String key) {
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0.0d : Double.parseDouble(value);
		} catch (Exception e) {
			return 0.0d;
		}
	}

	/**
	 * <h5>功能:获取float类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息返回0.0
	 */
	protected float getFloatParam(Map<String, Object> paramMap, String key) {
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0f : Float.parseFloat(value);
		} catch (Exception e) {
			return 0.0f;
		}
	}

	/**
	 * <h5>功能:获取long类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息返回0
	 */
	protected long getLongParam(Map<String, Object> paramMap, String key) {
		String value = getStringParam(paramMap, key);
		try {
			return null == value ? 0L : Long.parseLong(value);
		} catch (Exception e) {
			return 0L;
		}
	}

	/**
	 * <h5>功能:获取boolean类型参数</h5>
	 * 
	 * @author zhangpj @date 2018年11月16日
	 * @param paramMap 参数信息
	 * @param key      参数名
	 * @return 没有参数信息或转换失败返回false
	 */
	protected boolean getBooleanParam(Map<String, Object> paramMap, String key) {
		String value = getStringParam(paramMap, key);
		return null == value ? false : Boolean.parseBoolean(value);
	}
	
	/**
	 * 分页查询菜单信息(这里抽取为公共方法)
	 * @param baseDao
	 * @return
	 */
	protected Map<String, Object> selectAll(BaseDao<T> baseDao) {
        Map<String, Object> paramMap = getMaps();
        Page<T> page = getPage(paramMap);
        PageHelper.startPage(page.getPageNum(), page.getPageSize());

        List<T> list = baseDao.selectAll(paramMap);
        PageInfo<T> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();

        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("count", total);
        map.put("data", pageInfo.getList());

        return map;
    }
}
