package com.qfx.modules.common.dao;

import java.util.List;
import java.util.Map;

public interface BaseDao<T> {
	/**
	 * <h5>功能:根据条件获取全部信息</h5>
	 * 
	 * @param paramMap
	 * @return List<T>
	 */
	List<T> selectAll(Map<String, Object> paramMap);
	
	/**
	 * <h5>功能:根据条件获取信息数量</h5>
	 * 
	 * @param paramMap
	 * @return int
	 */
	int selectCount(Map<String, Object> paramMap);
	
	/**
	 * <h5>功能:根据ID获取信息</h5>
	 * 
	 * @param t_id
	 * @return T
	 */
	T selectByPrimaryKey(String t_id);
	
	/**
	 * <h5>功能:根据ID获取信息</h5>
	 * 
	 * @param t_id
	 * @return T
	 */
	T selectByPrimaryKey(int t_id);
	
	/**
	 * <h5>功能:新增信息</h5>
	 * 
	 * @param t
	 * @return int
	 */
	int insert(T t);
	
	/**
	 * <h5>功能:批量新增信息</h5>
	 * 
	 * @param list
	 * @return int
	 */
	int batchInsert(List<T> list);
	
    /**
     * <h5>功能:批量新增信息</h5>
     * 
     * @param list
     * @return int
     */
    int batchInsertExt(List<Map<String, Object>> list);
    
    /**
     * <h5>功能:根据ID更新信息</h5>
     * 
     * @param t
     * @return int
     */
    int updateByPrimaryKeySelective(T t);
    
    /**
     * <h5>功能:根据ID删除信息</h5>
     * 
     * @param t_id
     * @return int
     */
    int deleteByPrimaryKey(String t_id);
    
    /**
     * <h5>功能:根据ID删除信息</h5>
     * 
     * @param t_id
     * @return int
     */
    int deleteByPrimaryKey(int t_id);
    
    /**
     * <h5>功能:根据ID批量删除信息</h5>
     * 
     * @param ids
     * @return int
     */
    int batchDeleteByPrimaryKey(String[] ids);
}
