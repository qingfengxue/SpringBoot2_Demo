package com.qfx.modules.common.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qfx.modules.common.dao.BaseDao;

@Component
public class ToolCommon<T> {
	/**
	 * 分页查询菜单信息(这里抽取为公共方法)
	 * @param baseDao
	 * @return
	 */
	public Map<String, Object> selectAll(BaseDao<T> baseDao, int pageNum ,int pageSize ) {
        PageHelper.startPage(pageNum, pageSize);

        List<T> list = baseDao.selectAll(null);
        PageInfo<T> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();

        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("count", total);
        map.put("data", pageInfo.getList());

        return map;
    }
}
