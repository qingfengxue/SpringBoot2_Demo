package com.qfx;

import org.jasypt.util.text.AES256TextEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//@SpringBootTest
public class EncryptorTest {

	@Test
	public void getPass() {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword("qfxkey");
		System.out.println("明文字符串：" + "F6zx3qcY4yhl9t");
		String ciphertext = encryptor.encrypt("F6zx3qcY4yhl9t");
		System.out.println("加密后字符串：" + ciphertext);
	}
}
