package com.qfx;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alibaba.fastjson.JSONObject;
import com.qfx.modules.system.dao.SysUserMapper;
import com.qfx.modules.system.entity.SysMenu;
import com.qfx.modules.system.entity.SysUser;
import com.qfx.modules.system.service.SysMenuSer;

@SpringBootTest
class RunAppTests {
	
	@Autowired
	SysUserMapper sysUserMapper;
	
	@Test
	void test01() {
		List<SysUser> sysUserList = sysUserMapper.selectAll();
		for (SysUser sysUser : sysUserList) {
			System.out.println(JSONObject.toJSONString(sysUser.getRemark()));
		}
		System.out.println("-------------------------------------------------");
		Map<String, Object> map = sysUserMapper.sayHello();
		System.out.println(JSONObject.toJSONString(map));
	}
	
	@Autowired
	SysMenuSer sysMenuSer;
	
	@SuppressWarnings("unchecked")
	@Test
	void test02() {
		Map<String,Object> map = sysMenuSer.selectAllExt();
		List<SysMenu> list = (List<SysMenu>) map.get("data");
		System.out.println("获取信息开始");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(JSONObject.toJSONString(list.get(i)));
		}
		System.out.println("获取信息结束");
	}

}
