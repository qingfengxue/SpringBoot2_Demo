/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 80018
Source Host           : 127.0.0.1:3306
Source Database       : qfxshirotest

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2019-11-11 14:23:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_function_app`
-- ----------------------------
DROP TABLE IF EXISTS `sys_function_app`;
CREATE TABLE `sys_function_app` (
  `fun_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '功能ID',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '功能路径',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '功能描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`fun_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='App功能菜单表';

-- ----------------------------
-- Records of sys_function_app
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单编号',
  `fid` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父菜单编号',
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单链接',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标url',
  `show_order` int(11) DEFAULT NULL COMMENT '显示排序',
  `state` int(11) DEFAULT '1' COMMENT '状态(0:禁用 1:正常)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` varchar(36) NOT NULL COMMENT '角色ID',
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '角色描述',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态(0:禁用 1:正常)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('2001001', 'APP用户_VIP', null, '1', null, null);
INSERT INTO `sys_role` VALUES ('2001002', 'APP用户_普通', null, '1', null, null);

-- ----------------------------
-- Table structure for `sys_role_func_app`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_func_app`;
CREATE TABLE `sys_role_func_app` (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '记录标识ID',
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `fun_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '功能ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色-App功能菜单权限表';

-- ----------------------------
-- Records of sys_role_func_app
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '记录标识ID',
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `menu_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '记录标识ID',
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色用户表';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('0291f898ca7945518bf62778efe4b749', '2001001', '1001004', null, null);
INSERT INTO `sys_role_user` VALUES ('057fcc8f0d80422481f9bd6690ae0e17', '2001002', '1001009', null, null);
INSERT INTO `sys_role_user` VALUES ('0d95785d71a241728d41adcbee65b059', '2001002', '1001007', null, null);
INSERT INTO `sys_role_user` VALUES ('1e814dc4c9754faf87f08faac8c7540f', '2001001', '1001009', null, null);
INSERT INTO `sys_role_user` VALUES ('1f4e48a8939e41f08e663d25b1525e4f', '2001001', '1001003', null, null);
INSERT INTO `sys_role_user` VALUES ('271fde3ef4144293bee0308bf0b6ce51', '2001002', '1001011', null, null);
INSERT INTO `sys_role_user` VALUES ('3ea6d6f33ddc47aeacd68d8c2de958ad', '2001001', '1001010', null, null);
INSERT INTO `sys_role_user` VALUES ('4ea9ce0ee51c4f14b651420e187c3cb1', '2001001', '1001002', null, null);
INSERT INTO `sys_role_user` VALUES ('53c91f7fd11746279c2666610c92095a', '2001002', '1001003', null, null);
INSERT INTO `sys_role_user` VALUES ('6ad6df59d5c247afab74dffde1e4d8af', '2001001', '1001001', null, null);
INSERT INTO `sys_role_user` VALUES ('9257a737ca264f0f91943c8132b39a63', '2001002', '1001006', null, null);
INSERT INTO `sys_role_user` VALUES ('94389a3536a3473687dc8a6c4484b03c', '2001001', '1001008', null, null);
INSERT INTO `sys_role_user` VALUES ('c7a0a47c3503436d97bcd8d9ac8fd019', '2001001', '1001006', null, null);
INSERT INTO `sys_role_user` VALUES ('d99f5739afb145cb9f6876bf7249050c', '2001001', '1001007', null, null);
INSERT INTO `sys_role_user` VALUES ('dbfd698f79a444448f11bfdaa9d4facf', '2001001', '1001005', null, null);
INSERT INTO `sys_role_user` VALUES ('dea5c54975c245fba271db68831b8678', '2001001', '1001011', null, null);

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ID',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户密码',
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户盐值',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '状态(0:禁用 1:正常 2:锁定)',
  `sex` int(11) NOT NULL DEFAULT '2' COMMENT '性别(0:女 1:男 2:未知)',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '昵称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '手机号码',
  `avatar_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '头像路径',
  `background_img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '背景图路径',
  `email` varchar(255) DEFAULT NULL COMMENT '电子邮件',
  `personal_profile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '个人简介',
  `register_source` int(11) DEFAULT NULL COMMENT '注册来源(0:系统 1:App)',
  `cipher` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '暗码(自定义规则密码)',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1001001', '13280009366', '72716214e751677c86bb52868ca8e26e', '9a5991e38eb7a6ed9b42659f40f0fda5', '1', '2', null, '13280009366', null, null, null, null, '1', 'znEzk69961j8cR8e5u6YJ1kNelDN3uYJZf0th49II0Bb0aLs0GfSqfZYPp0bsMV877Mf0V96UJi05y8HBpwLEwt62v8bCHhP4A3iirn1Q9w61LyWZShjzd1gB64w8K', null, '2019-11-07 15:58:00', null);
INSERT INTO `sys_user` VALUES ('1001002', '13280009367', '83064a974c9b5747aafdcd2bba0bec67', 'eed02a6630b2ac4ff66e6a9216d35059', '1', '2', null, '13280009367', null, null, null, null, '1', 'jqb4Ub63dC73VS6rus3x9w9CSG0Of20f300z7d8ivF2Uu734Ra1j66qD13KCsz3TD', null, '2019-11-07 15:58:08', null);
INSERT INTO `sys_user` VALUES ('1001003', '13280009368', '5c0d3ae50ce5b4b5367f9dc4e6250c51', 'dd507da7c6d3ce657fd2caafb581b96a', '1', '2', null, '13280009368', null, null, null, null, '1', '3BXaqI1NA58INFOi6EHowJ3JaN5I9I2sIP0QLsbq0qrc6d0gAhi480avUA2o7Mkx3cvod01bgI2V33K21ODA', null, '2019-11-07 15:58:13', null);
INSERT INTO `sys_user` VALUES ('1001004', '13280009369', '35756167c89eead70da9a7d676bc260d', 'e7cf3d5b8d6a1e0df3e9d7ef1e200369', '1', '2', null, '13280009369', null, null, null, null, '1', '6iUwXQ7IJJqOR59BshsI6FG9Nj3Ga8Hi9TWw8T0dpMCY0joL1b0hlA7w8kciqo2izTki3lQENB1rmO03aU032SXbR0', null, '2019-11-07 15:58:17', null);
INSERT INTO `sys_user` VALUES ('1001005', '13280009370', 'c2e13045250a8d1aefd1cbd01f8224ac', 'cf23480676805410ee191a33551e874e', '1', '2', null, '13280009370', null, null, null, null, '1', 'ba5I3d1Zc604FxC9B7KhIRBy3JsS5kQ9tm6oEr0zh7Kd5087xefX0kKMzXF8eLSw3y2jUi8hS3OAO5Jr1nlrHoD3wX7Qpcc', null, '2019-11-07 15:58:21', null);
INSERT INTO `sys_user` VALUES ('1001006', '13280009371', '043081114f523721297e30aacf532e73', 'a02ace7d494085099a8fe46adf5973a6', '1', '2', null, '13280009371', null, null, null, null, '1', 'cBwfn32KQ27Y15R7Uo35j9Cz04S0yt06l8Y42eZ30A1DS551GZVXc', null, '2019-11-07 15:58:25', null);
INSERT INTO `sys_user` VALUES ('1001007', '13280009372', '8d649612d1b28be29b293829995a1d5d', 'e030c22d00b554f2b55d7ca09ce8636b', '1', '2', null, '13280009372', null, null, null, null, '1', '9cjW4v7YR2DeeGb26I7xF3N09ae0zN0lf0H684v2tn3OQ1VUgzhzg8sLWGG1M', null, '2019-11-07 15:58:29', null);
INSERT INTO `sys_user` VALUES ('1001008', '13280009373', '60e85c014aceced1572d7c9917844006', 'f90c83dd9311ad8413e5f4a14b1ea38f', '1', '2', null, '13280009373', null, null, null, null, '1', 'o5L0e37C9MaoGnYi3tTIic5ku57JeiYXadsV3CcSNtnMwg98EULbBlia0lL7MwkqRM0EYVhzUyN20Bp1KMwiWT8gOrYeXvOf2SgCNBHdyC3IsQz5aZnL1zafaSqXnY9g1wG7QF', null, '2019-11-07 15:58:34', null);
INSERT INTO `sys_user` VALUES ('1001009', '13280009374', 'b8b9d14f4a793aac3cb3af4e582c5334', 'd3bad0e43647c7c81f41c11c26591626', '1', '2', null, '13280009374', null, null, null, null, '1', 'UhS3wQ9Krv1Wvj4b7P3G9d0s0r018o2p3U1NXLXKNhQ77vwk8rfS', null, '2019-11-07 15:58:40', null);
INSERT INTO `sys_user` VALUES ('1001010', '13280009375', '06e7293ec4083fc47aacdf6902b413a2', '41252d9d34dfcffe31b61aadd235fe28', '1', '2', null, '13280009375', null, null, null, null, '1', '4XB2DO5059OV7Jbj3WoA9A2M0ENI0A4R0PnV8bBJ2tGD3zDG1e0m5i1l8DGIPV', null, '2019-11-07 15:58:44', null);
INSERT INTO `sys_user` VALUES ('1001011', '13280009376', '71ffb4d14911d39b66c9dc99f7ac25af', '62d2397ac9eb284fc47f2fadf54a8e90', '1', '2', null, '13280009376', null, null, null, null, '1', 'FpwRZ57I0ayXy6F4p7LVE3UXZ98zh0zno0JC90d1f8gwP24Bw3qCG1Dl6f3YF6aA5mPEz', null, '2019-11-07 15:58:49', null);
