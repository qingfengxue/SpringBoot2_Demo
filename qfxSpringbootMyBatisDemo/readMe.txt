1.项目介绍
	1.本示例采用的SpringBoot版本是2.5.3
	2.这是一个SpringBoot+MyBatis的示例,支持多种数据库连接(不是多数据源),即Sql支持多种数据库语法
	3.分页查询方法抽取成了个一个公共的方法
2.添加或修改项目内容后后,pom.xml先clear再install,然后再启动,否则配置可能不生效
3.新增MyBatis支持,并引入MySQL与PostgreSql连接支持
	3.1 pom.xml
		<!-- 5.引入MySQL连接支持 -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>

		<!-- Springboot3.x需要使用mysql-connector-j -->
        <!-- <dependency> -->
            <!-- <groupId>com.mysql</groupId> -->
            <!-- <artifactId>mysql-connector-j</artifactId> -->
            <!-- <version>9.1.0</version> -->
        <!-- </dependency> -->
		
		<!-- 6.引入PostgreSql连接支持 -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
		</dependency>
		
		<!-- 7.引入MyBatis支持,一定要使用starter,不然无法自动配置和注入 -->
		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>1.3.2</version>
		</dependency>
		
		<!-- 8.开启分页插件pagehelper依赖包 -->
      	<dependency>
			<groupId>com.github.pagehelper</groupId>
			<artifactId>pagehelper-spring-boot-starter</artifactId>
			<version>1.2.7</version>
		</dependency>
4.添加支持多种数据库连接(不是多数据源)的配置,即Sql支持多种数据库语法
	com.qfx.modules.common.config.DatabaseConfig
	
5.新增配置参数加密
	5.1 pom.xml
		<!-- 9.引入配置文件信息加密功能依赖包 -->
		<dependency>
		   <groupId>com.github.ulisesbocchio</groupId>
		   <artifactId>jasypt-spring-boot-starter</artifactId>
		   <version>3.0.5</version>
		</dependency>
	5.2 application.properties添加参数
		## 设定jasypt加密的盐值(加密/解密使用,生产环境建议从命令行进行设置)
		jasypt.encryptor.password=qfxkey
		## 设定jasypt加密算法(默认PBEWithHMACSHA512AndAES_256,可不设定),还有PBEWithMD5AndDES与PBEWithMD5AndTripleDES加密算法
		jasypt.encryptor.algorithm=PBEWithHMACSHA512AndAES_256
		## 设定jasypt加密类(默认org.jasypt.iv.RandomIvGenerator,可不设定),对应org.jasypt.iv.NoIvGenerator与org.jasypt.iv.NoIvGenerator
		jasypt.encryptor.iv-generator-classname=AES256TextEncryptor
		
		## 设定加密内容,以默认格式ENC()进行包裹，加密信息从com.qfx.EncryptorTest.getPass()方法中生成即可
		spring.datasource.username=ENC(jlDqfJaB/LJHLtowv2JPT82ZKEzDnYV72dPoZ7HtdDNMPOdYhoD5WIObj2QPtXU3)
		spring.datasource.password=ENC(BjnC5rl5qimDnKmyrhfFi2EmfyMhKnFd3Wvst2rP7+C2T63+CTuVcsLRWlTHs7Ec)
	5.3 加密算法与加密类关系
		加密算法                                                                                          加密类 
		PBEWithHMACSHA512AndAES_256(3.x版本默认)    org.jasypt.iv.RandomIvGenerator
		PBEWithMD5AndDES(3.x以下版本默认)            org.jasypt.iv.NoIvGenerator
		PBEWithMD5AndTripleDES                    org.jasypt.iv.NoIvGenerator
	5.4  获取
		// 使用@Value("${xxx}")正常获取即可
		@Value("${spring.datasource.password}")
		String password;

5.基础数据
	连接数据库(mysql8)
	MySql8的数据库驱动采用com.mysql.cj.jdbc.Driver
	spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
	
	连接数据库(PostgreSql)
	PostgreSql的数据库驱动采用org.postgresql.Driver
	spring.datasource.driver-class-name=org.postgresql.Driver
		
6.启动:
	1.直接运行RunApp.java中的main方法即可

7.测试
	7.1 执行src/test/java资源包下的com.qfx.main.RunAppTests.java即可从控制台看到输出结果
	7.2 启动后访问 
		普通请求:http://127.0.0.1:8081/test/list
		分页请求:http://127.0.0.1:8081/sys/menu/list?page=1&limit=2
		公共分页请求方式一:http://127.0.0.1:8081/sys/menu/listExt?page=2&limit=2
		公共分页请求方式二:http://127.0.0.1:8081/sys/menu/listExtTwo?page=2&limit=3
	